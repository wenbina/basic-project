package com.basic.project.spring.ioc.service;

import com.basic.project.spring.ioc.annotation.Bean;
import com.basic.project.spring.ioc.annotation.Di;

@Bean
public class TestService {

    @Di
    private UserService userService;

    public void diTest() {
        userService.add();
    }
}
