package com.basic.project.spring.ioc.test;

import com.basic.project.spring.ioc.bean.ApplicationContext;
import com.basic.project.spring.ioc.bean.ApplicationContextImpl;
import com.basic.project.spring.ioc.service.TestService;
import com.basic.project.spring.ioc.service.UserService;

public class TestUser {

    public static void main(String[] args) {
        ApplicationContext context = new ApplicationContextImpl("com.basic.project.spring");
        TestService testService = (TestService) context.getBean(TestService.class);
        System.out.println(testService);
        testService.diTest();
    }
}
