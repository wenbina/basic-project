package com.basic.project.spring.reflect;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class ReflectStudy {

    public static void main(String[] args) throws Exception {
        ReflectStudy reflect = new ReflectStudy();
        // 获取class对象
        Class clazz = reflect.getClassMethod();
        // 实例化
        Car car = (Car)clazz.getDeclaredConstructor().newInstance();
        System.out.println(car);

        // 获取构造方法
        reflect.getConstructorMethod(clazz);

        // 获取属性
        reflect.getFieldsMethod(clazz);

        // 获取方法
        reflect.getMethodMethod(clazz);
    }

    // 获取class对象方法
    private Class getClassMethod() throws ClassNotFoundException {
        // 类名.class
        Class clazz = Car.class;

        // 对象.getClass
        Class clazz1 = new Car().getClass();

        // class.forName("全路径")
        Class clazz2 = Class.forName("com.basic.project.spring.reflect.Car");

        return clazz2;
    }

    // 获取构造方法
    private void getConstructorMethod(Class clazz) throws Exception {
        // 获取所有构造：getConstructors：只获取public；getDeclaredConstructors：获取private
        Constructor[] constructors = clazz.getConstructors();
        for (Constructor constructor : constructors) {
            System.out.println("方法名称：" + constructor.getName() + " 参数个数：" + constructor.getParameterCount());
        }
        // 指定有参构造创建对象
        // public
        /*Constructor constructor = clazz.getConstructor(String.class, int.class, String.class);
        Car car1 = (Car) constructor.newInstance("奔驰", 1, "黑色");
        System.out.println(car1);*/
        // private
        Constructor declaredConstructor = clazz.getDeclaredConstructor(String.class, int.class, String.class);
        // 设置允许访问私有方法
        declaredConstructor.setAccessible(true);
        Car car2 = (Car) declaredConstructor.newInstance("奔驰", 1, "黑色");
        System.out.println(car2);
    }

    // 获取属性方法
    private void getFieldsMethod(Class clazz) throws Exception {
        // 获取所有public属性
        Car car = (Car)clazz.getDeclaredConstructor().newInstance();
        clazz.getFields();
        // 获取所有private属性
        Field[] declaredFields = clazz.getDeclaredFields();
        for (Field declaredField : declaredFields) {
            if ("name".equals(declaredField.getName())) {
                // 设置允许访问
                declaredField.setAccessible(true);
                // 设置值
                declaredField.set(car, "宝马");
                System.out.println(car);
            }
            System.out.println(declaredField.getName());
        }
    }

    // 获取方法
    private void getMethodMethod(Class clazz) throws Exception {
        Car car = new Car("奔驰", 1, "黑色");
        // public
        Method[] methods = clazz.getMethods();
        for (Method method : methods) {
            System.out.println(method.getName());
            // 执行方法
            if ("toString".equals(method.getName())) {
                String invoke = (String)method.invoke(car);
                System.out.println(invoke);
            }
        }

        // private
        Method[] declaredMethods = clazz.getDeclaredMethods();
        for (Method declaredMethod : declaredMethods) {
            System.out.println(declaredMethod.getName());
            if ("run".equals(declaredMethod.getName())) {
                // 设置允许访问
                declaredMethod.setAccessible(true);
                declaredMethod.invoke(car);
            }
        }
    }

}

class Car {

    private String name;

    private int age;

    private String color;

    public Car() {
    }

    public Car(String name, int age, String color) {
        this.name = name;
        this.age = age;
        this.color = color;
    }

    private void run() {
        System.out.println("私有方法 run");
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    @Override
    public String toString() {
        return "Car{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", color='" + color + '\'' +
                '}';
    }
}


