package com.basic.project.spring.ioc.bean;

public interface ApplicationContext {

    Object getBean(Class clazz);

}
