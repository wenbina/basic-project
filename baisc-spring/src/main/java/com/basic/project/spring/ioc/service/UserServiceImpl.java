package com.basic.project.spring.ioc.service;

import com.basic.project.spring.ioc.annotation.Bean;

@Bean
public class UserServiceImpl implements UserService {

    @Override
    public void add() {
        System.out.println("add");
    }
}
