package com.basic.project.spring.ioc.bean;

import com.basic.project.spring.ioc.annotation.Bean;
import com.basic.project.spring.ioc.annotation.Di;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Field;
import java.net.URL;
import java.net.URLDecoder;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class ApplicationContextImpl implements ApplicationContext {

    // 存储bean容器
    private Map<Class, Object> beanFactory = new ConcurrentHashMap<>();
    private static String ROOT_PATH;

    /**
     * 创建map存放bean对象
     */
    @Override
    public Object getBean(Class clazz) {
        return beanFactory.get(clazz);
    }

    // 创建有参数构造
    public ApplicationContextImpl(String basePackagePath) {
        // bean 加载
        getFile(basePackagePath);
        // 属性注入
        loadDi();
    }

    // 获取文件
    private void getFile(String basePackagePath) {
        try {
            // 把.换成\：注：mac需要将\\\\换成/
            String packagePath = basePackagePath.replaceAll("\\.", "\\\\");
            // 获取包绝对路径
            Enumeration<URL> urls = Thread.currentThread().getContextClassLoader().getResources(packagePath);
            while (urls.hasMoreElements()) {
                URL url = urls.nextElement();
                String filePath = URLDecoder.decode(url.getFile(), "utf-8");
                // 获取根路径
                ROOT_PATH = filePath.substring(0, filePath.length() - packagePath.length());
                loadBean(new File(filePath));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // 包扫描，装载bean：传递包路径，设置包扫描规则，当前包以及子包，哪个类有@Bean注解，把这个类通过反射实例化；
    private void loadBean(File file) throws Exception {
        // 判断当前是否为文件夹
        if (!file.isDirectory()) {
            return;
        }
        // 获取文件夹中所有内容
        File[] childrenFiles = file.listFiles();
        // 判断文件夹是否为空
        if (childrenFiles == null) {
            return;
        }
        // 遍历文件夹所有内容
        for (File childFile : childrenFiles) {
            // 遍历得到每个file对象，继续判断是否为文件，递归
            if (childFile.isDirectory()) {
                // 递归
                loadBean(childFile);
            } else {
                // mac使用ROOT_PATH.length()
                String pathWithClass = childFile.getAbsolutePath().substring(ROOT_PATH.length() - 1);
                // 判断文件类型是否为.class
                if (pathWithClass.contains(".class")) {
                    // 是.class文件，把路径\替换成. 把.class去掉
                    String allName = pathWithClass.replaceAll("\\\\", ".").replace(".class", "");
                    // 判断是否为接口
                    Class<?> clazz = Class.forName(allName);
                    if (!clazz.isInterface()) {
                        // 判断类上是否有@bean注解，进行实例化
                        Bean annotation = clazz.getAnnotation(Bean.class);
                        if (annotation != null) {
                            Object instance = clazz.getConstructor().newInstance();
                            // 把对象放入beanFactory
                            // 判断当前类是否有接口，有接口让接口class作为map的key
                            if (clazz.getInterfaces().length > 0) {
                                beanFactory.put(clazz.getInterfaces()[0], instance);
                            } else {
                                beanFactory.put(clazz, instance);
                            }
                        }
                    }
                }
            }
        }
    }

    // 属性注入
    private void loadDi() {
        // 遍历bean集合
        for (Map.Entry<Class, Object> entry : beanFactory.entrySet()) {
            // 获取bean集合中的对象，获取对象属性
            Object obj = entry.getValue();
            Class<?> clazz = obj.getClass();
            // 遍历对象属性，判断是否有@Di注解
            Field[] declaredFields = clazz.getDeclaredFields();
            for (Field field : declaredFields) {
                Di annotation = field.getAnnotation(Di.class);
                if (annotation != null) {
                    field.setAccessible(true);
                    try {
                        field.set(obj, beanFactory.get(field.getType()));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
