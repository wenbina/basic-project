package com.basic.starter.mybatisplus.autoconfiguration;

import com.baomidou.mybatisplus.extension.plugins.MybatisPlusInterceptor;
import com.basic.starter.mybatisplus.config.MybatisPlusMetaObjectHandler;
import com.basic.starter.mybatisplus.plugins.BasicPaginationInnerInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/28 14:04
 * mybatis plus统一配置
 */
@Configuration
public class MyBatisPlusAutoConfiguration {

    /**
     * 分页插件, 对于单一数据库类型来说,都建议配置该值,避免每次分页都去抓取数据库类型
     */
    @Bean
    public MybatisPlusInterceptor mybatisPlusInterceptor() {
        MybatisPlusInterceptor interceptor = new MybatisPlusInterceptor();
        interceptor.addInnerInterceptor(new BasicPaginationInnerInterceptor());
        return interceptor;
    }

    /**
     * 审计字段自动填充
     */
    @Bean
    public MybatisPlusMetaObjectHandler mybatisPlusMetaObjectHandler() {
        return new MybatisPlusMetaObjectHandler();
    }
}
