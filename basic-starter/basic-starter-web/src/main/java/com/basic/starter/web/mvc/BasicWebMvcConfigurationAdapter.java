package com.basic.starter.web.mvc;

import cn.hutool.core.collection.CollectionUtil;
import com.basic.starter.core.login.TokenStore;
import com.basic.starter.web.annotation.request.RequestParameterMethodArgumentResolver;
import com.basic.starter.web.annotation.request.RequestParameterOneMethodArgumentResolver;
import com.basic.starter.web.i18n.BasicLocalResolver;
import com.basic.starter.web.interceptor.LoginInterceptor;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.ObjectProvider;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.boot.autoconfigure.http.HttpMessageConverters;
import org.springframework.boot.autoconfigure.validation.ValidatorAdapter;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.util.ClassUtils;
import org.springframework.validation.Validator;
import org.springframework.web.method.support.HandlerMethodArgumentResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.*;
import org.springframework.web.servlet.i18n.LocaleChangeInterceptor;

import java.util.List;
import java.util.Locale;
import java.util.function.Consumer;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 10:32
 * mvc自定义配置
 */
@Configuration
public class BasicWebMvcConfigurationAdapter extends WebMvcConfigurationSupport {

    // 国际化参数
    private final static String LANGUAGE_PARAM = "language";

    private final ObjectProvider<HttpMessageConverters> messageConvertersProvider;
    private final ObjectMapper objectMapper;
    private final TokenStore tokenStore;
    private final ConfigurableBeanFactory beanFactory;
    private final List<WebMvcConfigurer> webMvcConfigurers;

    public BasicWebMvcConfigurationAdapter(ObjectProvider<HttpMessageConverters> messageConvertersProvider,
                                           ObjectMapper objectMapper,
                                           TokenStore tokenStore,
                                           ConfigurableBeanFactory beanFactory,
                                           List<WebMvcConfigurer> webMvcConfigurers) {
        this.messageConvertersProvider = messageConvertersProvider;
        this.objectMapper = objectMapper;
        this.tokenStore = tokenStore;
        this.beanFactory = beanFactory;
        this.webMvcConfigurers = webMvcConfigurers;
    }

    private void doCustomsWebMvcConfig(Consumer<WebMvcConfigurer> consumer) {
        if (CollectionUtil.isNotEmpty(webMvcConfigurers)) {
            for (WebMvcConfigurer webMvcConfigurer : webMvcConfigurers) {
                consumer.accept(webMvcConfigurer);
            }
        }
    }

    /**
     * 全局跨域配置
     */
    @Override
    protected void addCorsMappings(CorsRegistry registry) {
        doCustomsWebMvcConfig(webMvcConfigurer -> webMvcConfigurer.addCorsMappings(registry));
        registry.addMapping("/**")
                .allowCredentials(true)
                .allowedHeaders("*")
                .allowedOrigins("*")
                .allowedMethods("*");
    }

    /**
     * 增加资源映射，处理swagger的静态资源映射
     */
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        doCustomsWebMvcConfig(webMvcConfigurer -> webMvcConfigurer.addResourceHandlers(registry));
        registry.addResourceHandler("*.txt").addResourceLocations("classpath:/");
        registry.addResourceHandler("*.html").addResourceLocations("classpath:/templates/");
    }

    /**
     * 国际化语言解析，主要开启在子线程传递国际化语言
     */
    @Bean
    public LocaleResolver localeResolver() {
        return new BasicLocalResolver(Locale.SIMPLIFIED_CHINESE, LANGUAGE_PARAM);
    }

    /**
     * 配置参数校验validator，主要获取国际化转换的参数校验
     */
    @Bean
    @Override
    public Validator mvcValidator(){
        if (!ClassUtils.isPresent("javax.validation.Validator", getClass().getClassLoader())) {
            return super.mvcValidator();
        }
        return ValidatorAdapter.get(getApplicationContext(), getValidator());
    }

    /**
     * 配置自定义拦截器
     */
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        doCustomsWebMvcConfig(webMvcConfigurer -> webMvcConfigurer.addInterceptors(registry));
        // 设置国际化拦截器，通过获取请求参数中指定参数获取国际化语言
        LocaleChangeInterceptor localeChangeInterceptor = new LocaleChangeInterceptor();
        // 设置拦截参数名
        localeChangeInterceptor.setParamName(LANGUAGE_PARAM);
        registry.addInterceptor(localeChangeInterceptor);

        // 设置登录拦截器
        registry.addInterceptor(new LoginInterceptor(tokenStore));

    }


    /**
     * 配置自定义的参数解析器
     */
    @Override
    public void addArgumentResolvers(List<HandlerMethodArgumentResolver> argumentResolvers) {
        doCustomsWebMvcConfig(webMvcConfigurer -> {
            webMvcConfigurer.addArgumentResolvers(argumentResolvers);
        });
        final RequestParameterMethodArgumentResolver requestParameterResolver = new RequestParameterMethodArgumentResolver(this.getMessageConverters());
        final RequestParameterOneMethodArgumentResolver requestParamOneMethodArgumentResolver = new RequestParameterOneMethodArgumentResolver(beanFactory, false, objectMapper);
        argumentResolvers.add(requestParameterResolver);
        argumentResolvers.add(requestParamOneMethodArgumentResolver);
    }
}
