package com.basic.starter.web.annotation.response;

import java.lang.annotation.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:06
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
public @interface ResponseExceptionCatchs {
    /**
     * 异常捕获类型
     */
    ResponseExceptionCatch[] value();
}
