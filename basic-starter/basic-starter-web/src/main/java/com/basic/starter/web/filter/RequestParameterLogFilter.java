package com.basic.starter.web.filter;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.stream.Collectors;

/**
 * 请求参数打印过滤器
 */
public class RequestParameterLogFilter extends OncePerRequestFilter {

    private static final Logger logger = LoggerFactory.getLogger(RequestParameterLogFilter.class);

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        if (logger.isDebugEnabled()) {
            String requestParameterStr = request.getParameterMap().entrySet().stream()
                    .map(entry -> String.format("%s = %s", entry.getKey(), String.join(",", entry.getValue())))
                    .collect(Collectors.joining(", ", "[", "]"));
            logger.debug("接收到请求 - URI: {}, parameters: {}", request.getRequestURI(), requestParameterStr);
        }
        filterChain.doFilter(request, response);
    }

}
