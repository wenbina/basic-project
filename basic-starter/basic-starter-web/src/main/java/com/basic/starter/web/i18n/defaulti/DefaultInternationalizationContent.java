package com.basic.starter.web.i18n.defaulti;

import org.springframework.util.AntPathMatcher;

import java.util.Collections;
import java.util.Optional;
import java.util.Set;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 11:33
 */
public class DefaultInternationalizationContent {

    private static final AntPathMatcher MATCHER = new AntPathMatcher();
    private static Set<String> defaultInternationalUrls;

    /**
     * 判断当前URI返回结果是否需要国际化
     */
    public static boolean needDefaultInternational(String uri) {
        return Optional.ofNullable(defaultInternationalUrls).orElse(Collections.emptySet())
                .stream()
                .anyMatch(needUri -> MATCHER.match(needUri, uri));
    }

    public static void setDefaultInternationalUrls(Set<String> defaultInternationalUrls) {
        DefaultInternationalizationContent.defaultInternationalUrls = defaultInternationalUrls;
    }
}
