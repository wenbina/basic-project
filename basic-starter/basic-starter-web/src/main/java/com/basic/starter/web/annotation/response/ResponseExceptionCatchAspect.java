package com.basic.starter.web.annotation.response;

import com.basic.starter.core.base.response.ApiResponse;
import com.basic.starter.core.code.Code;
import com.basic.starter.core.exception.BasicException;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.Optional;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:06
 * @Description: 响应异常捕获转换
 */
@Aspect
public class ResponseExceptionCatchAspect {

    /**
     * 配置切入点
     */
    @Pointcut("@annotation(com.basic.starter.web.annotation.response.ResponseExceptionCatchs) || @annotation(com.basic.starter.web.annotation.response.ResponseExceptionCatch)")
    public void pointcut() {
        // 该方法无方法体,主要为了让同类中其他方法使用此切入点
    }

    @Around("pointcut()")
    public Object around(ProceedingJoinPoint joinPoint) throws Throwable {
        try {
            return joinPoint.proceed(joinPoint.getArgs());
        } catch (Throwable throwable) {
            Annotation[] annotations = ((MethodSignature) joinPoint.getSignature()).getMethod().getAnnotations();
            // step 单独ResponseExceptionCatch处理
            Optional<Annotation> responseExceptionCatchOptional = Arrays.stream(annotations).filter(annotation -> annotation instanceof ResponseExceptionCatch).findFirst();
            if (responseExceptionCatchOptional.isPresent()) {
                Code code = ResponseExceptionCatchHandler.handleResponseExceptionCatch((ResponseExceptionCatch) responseExceptionCatchOptional.get(), throwable);
                return doErrorResponse(code, throwable);
            }

            // step 单独ResponseExceptionCatchs处理
            Optional<Annotation> responseExceptionCatchsOptional = Arrays.stream(annotations).filter(annotation -> annotation instanceof ResponseExceptionCatchs).findFirst();
            if (responseExceptionCatchsOptional.isPresent()) {
                ResponseExceptionCatchs responseExceptionCatchs = (ResponseExceptionCatchs)responseExceptionCatchsOptional.get();
                Code code = ResponseExceptionCatchHandler.handleResponseExceptionCatchs(responseExceptionCatchs, throwable);
                return doErrorResponse(code, throwable);
            }
            throw throwable;
        }
    }

    private Object doErrorResponse(Code errorCode, Throwable throwable) {
        int code = errorCode.getCode();
        if (throwable instanceof BasicException && code == -1) {
            code = ((BasicException) throwable).getCode();
        }
        return ApiResponse.error().code(code).message(errorCode.getMessage());
    }

}
