package com.basic.starter.web.exception.handler;

import cn.hutool.json.JSONUtil;
import com.basic.starter.core.base.response.ApiResponse;
import com.basic.starter.web.tool.SpringContextHolder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

import java.util.stream.Collectors;

/**
 * @Author: jiangweifan
 * @Date: 2020/6/8
 * @Description: 对请求响应结果是ApiResponse类型的统一处理(包括异常错误处理的ApiResponse)，主要进行国际化处理
 */
@ControllerAdvice
public class GlobalResponseBodyAdvice implements ResponseBodyAdvice {

    private static final Logger logger = LoggerFactory.getLogger(GlobalResponseBodyAdvice.class);

    @Override
    public boolean supports(MethodParameter returnType, Class converterType) {
        Class<?> returnTypeClass = returnType.getMethod().getReturnType();
        // 此处判断returnTypeClass != Object.class，是为了避免有些内置方法返回Object，导致ClassCastException
        return returnTypeClass != Object.class && returnTypeClass.isAssignableFrom(ApiResponse.class);
    }

    @Override
    public Object beforeBodyWrite(Object body, MethodParameter returnType, MediaType selectedContentType, Class  selectedConverterType, ServerHttpRequest request, ServerHttpResponse response) {
        ApiResponse apiResponse = (ApiResponse) body;
        apiResponse.setMessage(SpringContextHolder.getI18nMessage(apiResponse.getMessage(), apiResponse.getParams()));
        if (logger.isDebugEnabled()) {
            String requestParameterStr = ((ServletServerHttpRequest) request).getServletRequest().getParameterMap().entrySet().stream()
                    .map(entry -> String.format("%s = %s", entry.getKey(), String.join(",", entry.getValue())))
                    .collect(Collectors.joining(", ", "[", "]"));
            logger.debug("响应请求 - URI: {},\n parameters: {}, \n result: {} \n", request.getURI().getPath(), requestParameterStr, JSONUtil.toJsonStr(apiResponse));
        }
        return body;
    }

}
