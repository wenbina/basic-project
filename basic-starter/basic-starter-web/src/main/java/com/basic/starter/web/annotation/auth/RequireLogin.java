package com.basic.starter.web.annotation.auth;

import java.lang.annotation.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:36
 * 自定义登录注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequireLogin {
}
