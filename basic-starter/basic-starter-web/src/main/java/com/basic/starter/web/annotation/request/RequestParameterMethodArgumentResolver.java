package com.basic.starter.web.annotation.request;

import com.basic.starter.core.code.Code;
import com.basic.starter.core.constants.BaseCodeConstants;
import com.basic.starter.core.exception.BasicException;
import com.basic.starter.web.annotation.response.ResponseExceptionCatch;
import com.basic.starter.web.annotation.response.ResponseExceptionCatchHandler;
import com.basic.starter.web.annotation.response.ResponseExceptionCatchs;
import org.springframework.core.Conventions;
import org.springframework.core.MethodParameter;
import org.springframework.core.annotation.AnnotationUtils;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.support.WebDataBinderFactory;
import org.springframework.web.context.request.NativeWebRequest;
import org.springframework.web.method.support.ModelAndViewContainer;
import org.springframework.web.servlet.mvc.method.annotation.AbstractMessageConverterMethodArgumentResolver;

import java.lang.annotation.Annotation;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:06
 * 自定义参数解析器，将请求参数解析到ApiRequest中
 */
public class RequestParameterMethodArgumentResolver extends AbstractMessageConverterMethodArgumentResolver {

    public RequestParameterMethodArgumentResolver(List<HttpMessageConverter<?>> converters) {
        super(converters);
    }

    @Override
    public boolean supportsParameter(MethodParameter parameter) {
        return parameter.hasParameterAnnotation(RequestParameter.class);
    }

    @Override
    public Object resolveArgument(MethodParameter parameter,
                                  ModelAndViewContainer mavContainer,
                                  NativeWebRequest webRequest,
                                  WebDataBinderFactory binderFactory) throws Exception {
        parameter = parameter.nestedIfOptional();
        Object arg = readWithMessageConverters(webRequest, parameter, parameter.getNestedGenericParameterType());
        String name = Conventions.getVariableNameForParameter(parameter);
        if (binderFactory != null) {
            WebDataBinder binder = binderFactory.createBinder(webRequest, arg, name);
            validateIfApplicable(binder, parameter);
            if (binder.getBindingResult().hasErrors() && isBindExceptionRequired(binder, parameter)) {
                Optional<Annotation> responseExceptionCatch = Arrays.stream(parameter.getMethodAnnotations()).filter(annotation -> annotation instanceof ResponseExceptionCatch).findFirst();
                Optional<Annotation> responseExceptionCatchs = Arrays.stream(parameter.getMethodAnnotations()).filter(annotation -> annotation instanceof ResponseExceptionCatchs).findFirst();
                if (responseExceptionCatch.isPresent() || responseExceptionCatchs.isPresent()) {
                    Exception throwable = new BasicException(-1, binder.getBindingResult().getAllErrors().get(0).getDefaultMessage());
                    if (responseExceptionCatchs.isPresent()) {
                        Code code = ResponseExceptionCatchHandler.handleResponseExceptionCatchs((ResponseExceptionCatchs) responseExceptionCatchs.get(), throwable);
                        throw new BasicException(code);
                    }
                    Code code = ResponseExceptionCatchHandler.handleResponseExceptionCatch((ResponseExceptionCatch) responseExceptionCatch.get(), throwable);
                    throw new BasicException(code);
                }
                throw new MethodArgumentNotValidException(parameter, binder.getBindingResult());
            }
            if (mavContainer != null) {
                mavContainer.addAttribute(BindingResult.MODEL_KEY_PREFIX + name, binder.getBindingResult());
            }
        }
        return adaptArgumentIfNecessary(arg, parameter);
    }

    @Override
    protected void validateIfApplicable(WebDataBinder binder, MethodParameter parameter) {
        if (null == binder.getTarget()) {
            throw new BasicException(BaseCodeConstants.APIREQUEST_NO_PARAMETER_TYPE);
        }
        Annotation[] annotations = parameter.getParameterAnnotations();
        for (Annotation ann : annotations) {
            RequestParameter requestParameterAnn = AnnotationUtils.getAnnotation(ann, RequestParameter.class);
            Validated validatedAnn = AnnotationUtils.getAnnotation(ann, Validated.class);
            if (validatedAnn != null || ann.annotationType().getSimpleName().startsWith("Valid")) {
                Object hints = (validatedAnn != null ? validatedAnn.value() : AnnotationUtils.getValue(ann));
                Object[] validationHints = (hints instanceof Object[] ? (Object[]) hints : new Object[] {hints});
                binder.validate(validationHints);
                break;
            }
        }
    }
}
