package com.basic.starter.web.annotation.track;

import java.lang.annotation.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/13 12:24
 * 自定义登录注解
 */
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequireTrack {

}
