package com.basic.starter.web.interceptor;

import com.basic.starter.core.constants.BaseCodeConstants;
import com.basic.starter.core.exception.BasicException;
import com.basic.starter.core.login.TokenStore;
import com.basic.starter.core.login.authentication.Authentication;
import com.basic.starter.web.annotation.auth.RequireLogin;
import com.basic.starter.web.context.authentication.AuthenticationContext;
import com.basic.starter.web.context.BasicOriginContext;
import org.springframework.util.StringUtils;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.handler.HandlerInterceptorAdapter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.lang.reflect.Method;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:22
 * 登录拦截器
 */
public class LoginInterceptor extends HandlerInterceptorAdapter {

    // token参数
    private final static String TOKEN_PARAM = "token";
    // origin参数
    private final static String ORIGIN_PARAM = "origin";
    private final TokenStore tokenStore;

    public LoginInterceptor(TokenStore tokenStore) {
        this.tokenStore = tokenStore;
    }

    /**
     * 前置处理
     */
    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
        if (!(handler instanceof HandlerMethod)) {
            return true;
        }
        final Method method = ((HandlerMethod) handler).getMethod();
        if (!method.isAnnotationPresent(RequireLogin.class)) {
            return true;
        }
        // 登录校验
        String token = request.getParameter(TOKEN_PARAM);
        if (!StringUtils.hasText(token)) {
            throw new BasicException("token 为空");
        }
        Authentication authentication = tokenStore.readAuthenticationByToken(token);
        if (null == authentication) {
            throw new BasicException("token 为空");
        }
        AuthenticationContext.setAuthentication(authentication);
        BasicOriginContext.setOrigin(request.getParameter(ORIGIN_PARAM));
        return true;
    }

    /**
     * 后置处理
     */
    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {
        AuthenticationContext.removeAuthentication();
        BasicOriginContext.removeOrigin();
    }
}
