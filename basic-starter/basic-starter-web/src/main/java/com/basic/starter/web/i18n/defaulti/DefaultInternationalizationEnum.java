package com.basic.starter.web.i18n.defaulti;

import com.basic.starter.web.tool.SpringContextHolder;

import java.util.Locale;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 11:45
 * 国际化正则匹配
 */
public enum DefaultInternationalizationEnum {

    TEST(),;

    private String name;

    private String i18nKey;

    private Function<String, String> enhance;

    private static Function<String, String> defaultEnhance() {
        return msg -> {
            return "\"" + msg + "\"";
        };
    }

    public static String i18nFormat(String msg, Locale locale) {
        for (DefaultInternationalizationEnum internationalizationEnum : DefaultInternationalizationEnum.values()) {
            Pattern pattern = Pattern.compile(internationalizationEnum.getName());
            Matcher matcher = pattern.matcher(msg);
            msg = matcher.replaceAll(internationalizationEnum.getEnhance().apply(SpringContextHolder.getI18nMessage(internationalizationEnum.getI18nKey(), locale)));
        }
        return msg;
    }

    public String getName() {
        return name;
    }

    public String getI18nKey() {
        return i18nKey;
    }

    public Function<String, String> getEnhance() {
        return enhance;
    }
}
