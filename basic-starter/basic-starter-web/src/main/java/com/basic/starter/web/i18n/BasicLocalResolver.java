package com.basic.starter.web.i18n;

import cn.hutool.core.util.StrUtil;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.servlet.LocaleResolver;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Locale;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:07
 * 国际化语言解析，主要用于设置国际化语言在子线程传递
 */
public class BasicLocalResolver implements LocaleResolver {

    private final Locale defaultLocale;

    private final String paramName;

    public BasicLocalResolver(Locale defaultLocale, String paramName) {
        this.defaultLocale = defaultLocale;
        this.paramName = paramName;
    }

    @Override
    public Locale resolveLocale(HttpServletRequest request) {
        String locale = request.getParameter(getParamName());
        return StringUtils.hasText(locale) ? StringUtils.parseLocale(locale) : getDefaultLocale();
    }

    @Override
    public void setLocale(HttpServletRequest request, HttpServletResponse response, Locale locale) {
        // 开启线程传递国际化
        LocaleContextHolder.setLocale(locale, true);
    }

    public Locale getDefaultLocale() {
        return defaultLocale;
    }

    public String getParamName() {
        return paramName;
    }
}
