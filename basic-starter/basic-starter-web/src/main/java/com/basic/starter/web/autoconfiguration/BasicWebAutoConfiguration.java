package com.basic.starter.web.autoconfiguration;


import com.basic.starter.core.base.response.ApiResponse;
import com.basic.starter.web.i18n.BasicI18nValidatorAdapter;
import com.basic.starter.web.i18n.defaulti.DefaultInternationalizationFilter;
import com.basic.starter.web.i18n.defaulti.DefaultInternationalizationListener;
import com.basic.starter.web.mvc.BasicWebMvcConfigurationAdapter;
import com.basic.starter.web.tool.SpringContextHolder;
import com.basic.starter.web.exception.handler.GlobalErrorHandler;
import com.basic.starter.web.exception.handler.GlobalExceptionHandler;
import com.basic.starter.web.exception.handler.GlobalResponseBodyAdvice;
import com.basic.starter.web.filter.RequestParameterLogFilter;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.boot.autoconfigure.condition.ConditionalOnWebApplication;
import org.springframework.boot.autoconfigure.validation.ValidationAutoConfiguration;
import org.springframework.boot.autoconfigure.web.servlet.error.BasicErrorController;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.Lazy;
import org.springframework.core.env.Environment;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Validator;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/30 14:48
 * web自动化配置
 */
@ConditionalOnWebApplication
@AutoConfigureBefore({ValidationAutoConfiguration.class}) // 此处需要在ValidationAutoConfiguration配置之前执行
@Import({
        BasicI18nValidatorAdapter.class,
        BasicWebMvcConfigurationAdapter.class
})
public class BasicWebAutoConfiguration {

    /**
     * 注入ApplicationContext操作工具
     */
    @Bean
    public SpringContextHolder springContextHolder() {
        return new SpringContextHolder();
    }

    /**
     * 自定义全局异常处理
     */
    @Bean
    public GlobalExceptionHandler globalExceptionHandler() {
        return new GlobalExceptionHandler();
    }

    /**
     * 全局错误处理器
     * 这里有一个坑是返回的数据类型不能是 {@link ErrorController}，否则不生效，会是 {@link BasicErrorController}
     * @param errorAttributes
     * @return
     */
    @Bean
    public GlobalErrorHandler globalErrorHandler(ErrorAttributes errorAttributes) {
        return new GlobalErrorHandler(errorAttributes);
    }

    /**
     * 自定义 {@link ApiResponse} 通知处理器，主要对返回信息进行国际化处理
     * 主要拦截处理
     * @see RestController rest接口返回的ApiResponse
     * @see GlobalExceptionHandler 全局异常处理返回的ApiResponse
     * @see GlobalErrorHandler 全局错误处理的返回的ApiResponse
     * @return
     */
    @Bean
    public GlobalResponseBodyAdvice globalResponseBodyAdvice() {
        return new GlobalResponseBodyAdvice();
    }

    /**
     * 重新注入MethodValidationPostProcessor是为了设置exposeProxy为true，这样在使用AopContext获取代理对象时才不会报错
     * @see ValidationAutoConfiguration#methodValidationPostProcessor(Environment, Validator)
     * @param environment
     * @param validator
     * @return
     */
    @Bean
    public static MethodValidationPostProcessor methodValidationPostProcessor(Environment environment,
                                                                              @Lazy Validator validator) {
        MethodValidationPostProcessor processor = new MethodValidationPostProcessor();
        boolean proxyTargetClass = environment.getProperty("spring.aop.proxy-target-class", Boolean.class, true);
        processor.setProxyTargetClass(proxyTargetClass);
        processor.setExposeProxy(true);
        processor.setValidator(validator);
        return processor;
    }

    /**
     * 请求参数打印过滤器
     */
    @Bean
    public RequestParameterLogFilter requestParameterLogFilter() {
        return new RequestParameterLogFilter();
    }

    /**
     * 国际化拦截器
     */
    @Bean
    public DefaultInternationalizationFilter defaultInternationalizationFilter() {
        return new DefaultInternationalizationFilter();
    }

    /**
     * 国际化监听器
     */
    @Bean
    public DefaultInternationalizationListener defaultInternationalizationListener() {
        return new DefaultInternationalizationListener();
    }

}
