package com.basic.starter.web.exception.handler;

import com.basic.starter.core.base.response.ApiResponse;
import com.basic.starter.core.constants.BaseCodeConstants;
import com.basic.starter.core.exception.BasicException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hibernate.validator.internal.engine.path.PathImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindException;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import javax.servlet.http.HttpServletRequest;
import javax.validation.ConstraintViolation;
import javax.validation.ConstraintViolationException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.function.Supplier;

/**
 * 全局异常处理
 */
@RestControllerAdvice
@SuppressWarnings("all")
public class GlobalExceptionHandler {

    private Logger logger = LoggerFactory.getLogger(GlobalExceptionHandler.class);

    @Autowired
    private ObjectMapper objectMapper;

    /**
     * 自定义异常处理
     * @param ex
     * @return
     */
    @ExceptionHandler(BasicException.class)
    public ApiResponse esellException(HttpServletRequest request, BasicException ex) {
        if (ex.isPrintExp()) {
            logger.error("全局异常捕获 - 自定义异常： {}", enhancerLog(request, ex.getMessage()));
        }
        return ApiResponse.error()
                .code(ex.getCode())
                .params(ex.getParams())
                .message(ex.getMessage());
    }


    /**
     * 参数校验异常 - {@link RequestBody} {@link RequestParameter} 参数校验时的异常
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {MethodArgumentNotValidException.class})
    public ApiResponse methodArgumentNotValidExceptionHandler(HttpServletRequest request, MethodArgumentNotValidException ex) {
        // 拼接错误
        BindingResult bindingResult = ex.getBindingResult();
        for (ObjectError objectError : bindingResult.getAllErrors()) {
            ErrorInfo errorInfo = handleDefaultValidMessage(objectError.getDefaultMessage(), null, getValidParamName(ex));
            logger.error("全局异常捕获 - 对象参数绑定校验异常： {}", enhancerLog(request, errorInfo.getMessage()));
            logger.debug("全局异常捕获 - 对象参数绑定校验异常完整信息： {}", exceptionToString(ex));
            return ApiResponse.error().message(errorInfo.getMessage());
        }
        // 包装 CommonResult 结果
        return ApiResponse.error();
    }

    /**
     * 参数校验异常 - MethodValidationInterceptor拦截器参数校验异常，主要是Service层参数校验抛出的异常
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(value = ConstraintViolationException.class)
    public ApiResponse constraintViolationExceptionHandler(HttpServletRequest request, ConstraintViolationException ex) {
        // 拼接错误
        for (ConstraintViolation<?> constraintViolation : ex.getConstraintViolations()) {
            ErrorInfo errorInfo = handleDefaultValidMessage(constraintViolation.getMessage(), null, getValidParamName(ex));
            logger.error("全局异常捕获 - 方法参数校验异常： {}", enhancerLog(request, errorInfo.getMessage()));
            logger.debug("全局异常捕获 - 对象参数绑定校验异常完整信息： {}", exceptionToString(ex));
            return ApiResponse.error().message(errorInfo.getMessage());
        }
        // 包装 CommonResult 结果
        return ApiResponse.error();
    }

    /**
     * 参数校验异常 - 数据绑定的参数校验异常,主要是controller MVC数据绑定时检验抛出的异常
     * @param request
     * @param ex
     * @return
     */
    @ExceptionHandler(value = {BindException.class})
    public ApiResponse bindExceptionHandler(HttpServletRequest request, BindException ex) {
        // 拼接错误
        BindingResult bindingResult = ex.getBindingResult();
        for (ObjectError objectError : bindingResult.getAllErrors()) {
            ErrorInfo errorInfo = handleDefaultValidMessage(objectError.getDefaultMessage(), null, getValidParamName(ex));
            logger.error("全局异常捕获 - 数据绑定参数绑定校验异常： {}", enhancerLog(request, errorInfo.getMessage()));
            logger.debug("全局异常捕获 - 对象参数绑定校验异常完整信息： {}", exceptionToString(ex));
            return ApiResponse.error().message(errorInfo.getMessage());
        }
        // 包装 CommonResult 结果
        return ApiResponse.error();
    }

    @ExceptionHandler(value = {HttpRequestMethodNotSupportedException.class})
    public ApiResponse httpRequestMethodNotSupportedExceptionHandler(HttpServletRequest request, HttpRequestMethodNotSupportedException ex) {
//        logger.error("全局异常捕获 - 请求方式不支持： {} ", enhancerLog(request, "Not Support " + request.getMethod()));
        return ApiResponse.error(BaseCodeConstants.HTTP_REQUEST_METHOD_NOT_SUPPORTED).params(request.getMethod());
    }


    /**
     * 未知异常处理
     * @param ex
     * @return
     */
    @ExceptionHandler(Exception.class)
    public ApiResponse exception(HttpServletRequest request, Exception ex) {
        logger.error("全局异常捕获 - 未知异常： {}", enhancerLog(request, exceptionToString(ex)));
        ApiResponse apiResponse= ApiResponse.error(BaseCodeConstants.UNKNOWN_ERROR);
        apiResponse.setMessage("服务异常，请稍后重试");
        apiResponse.setDebug(exceptionToString(ex));
        return apiResponse;
    }

    private String enhancerLog(HttpServletRequest request, String message) {
        String params = "";
        try {
            params = objectMapper.writeValueAsString(request.getParameterMap());
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        return String.format("%s: %s, \n %s", request.getRequestURI(), params, message);
    }

    /**
     * 异常堆栈转字符串
     * @param e
     * @return
     */
    private static String exceptionToString(Throwable e) {
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            if (e == null) {
                return "无具体异常信息";
            }
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return sw.toString();
        } catch (Exception ex) {
            return "";
        } finally {
            sw.flush();
            pw.flush();
            try {
                sw.close();
            } catch (Exception ex) {}
            try {
                pw.close();
            } catch (Exception ex) {}
        }
    }


    /**
     * 对默认无message的参数校验进行处理
     * @param defaultMessage
     * @param params
     * @param validParamNameSupplier
     * @return
     */
    private ErrorInfo handleDefaultValidMessage(String defaultMessage, Object[] params, Supplier<String> validParamNameSupplier) {
        if (!defaultMessage.contains("{param}")) {
            return new ErrorInfo(defaultMessage, params);
        }
        try {
            String validParamName = validParamNameSupplier.get();
            return new ErrorInfo(defaultMessage.replaceAll("\\{param\\}", validParamName), new Object[]{validParamName});
        } catch (Exception e) {
            return new ErrorInfo(defaultMessage, params);
        }
    }

    /**
     * ConstraintViolationException获取校验错误的参数名
     * @param ex
     * @return
     */
    public Supplier<String> getValidParamName(ConstraintViolationException ex) {
        return () -> {
            for (ConstraintViolation<?> constraintViolation : ex.getConstraintViolations()) {
                PathImpl pathImpl = (PathImpl) constraintViolation.getPropertyPath();
                // 读取参数字段，constraintViolation.getMessage() 读取验证注解中的message值
                return pathImpl.getLeafNode().getName();
            }
            throw new RuntimeException("ConstraintViolationException no exception params");
        };
    }

    /**
     * BindException获取校验错误的参数名
     * @param ex
     * @return
     */
    public Supplier<String> getValidParamName(BindException ex) {
        return () -> ex.getFieldError().getField();
    }

    /**
     * MethodArgumentNotValidException获取校验错误的参数名
     * @param ex
     * @return
     */
    public Supplier<String> getValidParamName(MethodArgumentNotValidException ex) {
        return () -> ex.getBindingResult().getFieldErrors().get(0).getField();
    }

    /**
     * 错误信息及参数
     */
    static class ErrorInfo {
        private String message;
        private Object[] params;

        public ErrorInfo(String message, Object[] params) {
            this.message = message;
            this.params = params;
        }

        public String getMessage() {
            return message;
        }

        public void setMessage(String message) {
            this.message = message;
        }

        public Object[] getParams() {
            return params;
        }

        public void setParams(Object[] params) {
            this.params = params;
        }
    }
}
