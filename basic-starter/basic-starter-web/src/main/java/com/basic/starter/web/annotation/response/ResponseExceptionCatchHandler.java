package com.basic.starter.web.annotation.response;

import com.basic.starter.core.code.Code;
import com.basic.starter.core.constants.BaseCodeConstants;
import com.basic.starter.core.exception.BasicException;
import org.springframework.util.StringUtils;

import java.util.Arrays;
import java.util.Optional;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:06
 */
public class ResponseExceptionCatchHandler {

    public static Code handleResponseExceptionCatch(ResponseExceptionCatch annotation, Throwable throwable)  {
        if (!StringUtils.hasText(annotation.source()) ||
                annotation.source().equalsIgnoreCase(throwable.getMessage())) {
            return new Code(annotation.code(), annotation.target());
        } else if (throwable instanceof BasicException) {
            throw (BasicException)throwable;
        }
        throw new BasicException(BaseCodeConstants.UNKNOWN_ERROR);
    }

    public static Code handleResponseExceptionCatchs(ResponseExceptionCatchs responseExceptionCatchs, Throwable throwable)  {
        Optional<ResponseExceptionCatch> exceptionCatchOptional = Arrays.stream(responseExceptionCatchs.value()).filter(exp -> exp.source().equalsIgnoreCase(throwable.getMessage())).findFirst();
        ResponseExceptionCatch annotation;
        if (exceptionCatchOptional.isPresent()) {
            annotation = exceptionCatchOptional.get();
            if (!StringUtils.hasText(annotation.source()) || annotation.source().equalsIgnoreCase(throwable.getMessage())) {
                return new Code(annotation.code(), annotation.target());
            }
        } else if (throwable instanceof BasicException) {
            throw (BasicException)throwable;
        }
        throw new BasicException(BaseCodeConstants.UNKNOWN_ERROR);

    }
}
