package com.basic.starter.web.i18n.defaulti;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.web.method.HandlerMethod;
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping;

import java.util.Set;
import java.util.stream.Collectors;


/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 11:59
 * 默认国际化监听器
 */
public class DefaultInternationalizationListener implements CommandLineRunner, ApplicationContextAware {

    private final Logger log = LoggerFactory.getLogger(DefaultInternationalizationListener.class);
    private final static String MAPPING_NAME = "requestMappingHandlerMapping";
    private ApplicationContext applicationContext;

    @Override
    public void run(String... args) throws Exception {
        RequestMappingHandlerMapping handlerMapping = (RequestMappingHandlerMapping) applicationContext.getBean(MAPPING_NAME);
        Set<String> uris = handlerMapping.getHandlerMethods().entrySet().stream().filter(entry -> {
            HandlerMethod handlerMethod = entry.getValue();
            return handlerMethod.getMethod().getAnnotation(DefaultInternationalization.class) != null;
        }).flatMap(entry -> entry.getKey().getPatternsCondition().getPatterns().stream()).collect(Collectors.toSet());
        DefaultInternationalizationContent.setDefaultInternationalUrls(uris);
        log.warn("默认i18n转换的uri收集完成:\n {}\n", uris);
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        this.applicationContext = applicationContext;
    }
}
