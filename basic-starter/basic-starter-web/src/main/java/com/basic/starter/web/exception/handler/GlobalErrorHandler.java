package com.basic.starter.web.exception.handler;

import com.basic.starter.core.base.response.ApiResponse;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.web.servlet.error.ErrorAttributes;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.context.request.ServletWebRequest;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Map;

/**
 * 全局错误处理
 */
@Controller
public class GlobalErrorHandler implements ErrorController {
    private static final String ERROR_PATH="/error";
    private static final ObjectMapper objectMapper = new ObjectMapper();
    private ErrorAttributes errorAttributes;

    private Logger logger = LoggerFactory.getLogger(GlobalErrorHandler.class);

    @Override
    public String getErrorPath() {
        return ERROR_PATH;
    }

    public GlobalErrorHandler(ErrorAttributes errorAttributes) {
        this.errorAttributes=errorAttributes;
    }

    /**
     * web页面错误处理
     */
    @RequestMapping(value=ERROR_PATH,produces="text/html")
    public String errorPageHandler(HttpServletRequest request, HttpServletResponse response) throws JsonProcessingException {
        ServletWebRequest requestAttributes =  new ServletWebRequest(request);
        Map<String, Object> attr = this.errorAttributes.getErrorAttributes(requestAttributes, false);
        ApiResponse result = ApiResponse.error().message((String) attr.get("message"));
        logger.error(request.getRequestURI() + " - " + attr.get("message"));
        return objectMapper.writeValueAsString(result);
    }

    /**
     * 除web页面外的错误处理，比如json/xml等
     */
    @RequestMapping(value=ERROR_PATH)
    @ResponseBody
    public ApiResponse errorApiHander(HttpServletRequest request, HttpServletResponse response) {
        ServletWebRequest requestAttributes = new ServletWebRequest(request);
        Map<String, Object> attr=this.errorAttributes.getErrorAttributes(requestAttributes, false);
        logger.error(request.getRequestURI() + " - " + attr.get("message"));
        return ApiResponse.error().message((String) attr.get("message"));
    }
}
