package com.basic.starter.web.tool;

import cn.hutool.core.util.ObjectUtil;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:24
 */
public class ObjectTool extends ObjectUtil {

    /**
     * 驼峰转横杠
     */
    public static String camelToUnderline(String param) {
        if (param == null || "".equals(param.trim())) {
            return "";
        }
        int len = param.length();
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++) {
            char c = param.charAt(i);
            if (Character.isUpperCase(c)) {
                sb.append("-");
                sb.append(Character.toLowerCase(c));
            } else {
                sb.append(c);
            }
        }
        return sb.toString();
    }
}
