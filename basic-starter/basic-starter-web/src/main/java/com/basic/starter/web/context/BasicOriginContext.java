package com.basic.starter.web.context;

import com.basic.starter.core.login.authentication.Authentication;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:31
 */
public class BasicOriginContext {

    private static final ThreadLocal<String> originThreadLocal = new ThreadLocal<>();

    public static String getOrigin() {
        return originThreadLocal.get();
    }

    public static void setOrigin(String origin) {
        originThreadLocal.set(origin);
    }

    public static void removeOrigin() {
        originThreadLocal.remove();
    }
}
