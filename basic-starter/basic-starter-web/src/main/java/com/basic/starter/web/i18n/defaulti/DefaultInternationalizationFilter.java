package com.basic.starter.web.i18n.defaulti;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.util.StringUtils;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.nio.charset.StandardCharsets;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 11:38
 * 默认i18n国际化拦截器
 */
public class DefaultInternationalizationFilter implements Filter {

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse resp = (HttpServletResponse) response;

        String uri = req.getRequestURI().substring(req.getContextPath().length());
        if (!DefaultInternationalizationContent.needDefaultInternational(uri)) {
            chain.doFilter(req, resp);
        } else {
            DefaultInternationalizationResponseWrapper responseWrapper = new DefaultInternationalizationResponseWrapper(resp);
            // 只拦截响应，不处理请求，若需要请求前处理，可以在此处进行处理
            chain.doFilter(req, responseWrapper);
            // 获取响应值
            byte[] content = responseWrapper.getContent();
            if (ObjectUtil.isNotNull(content) && content.length > 0) {
                String res = new String(content, StandardCharsets.UTF_8);
                res = DefaultInternationalizationEnum.i18nFormat(res, StringUtils.parseLocale(req.getParameter("language")));
                // 响应客户端
                if (res.length() > 0) {
                    ServletOutputStream outputStream = resp.getOutputStream();
                    resp.setContentType("application/json;charset=UTF-8");
                    outputStream.write(res.getBytes());
                    outputStream.flush();
                    outputStream.close();
                    resp.flushBuffer();
                }
            }
        }
    }
}
