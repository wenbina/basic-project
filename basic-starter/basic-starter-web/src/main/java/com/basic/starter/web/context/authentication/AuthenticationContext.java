package com.basic.starter.web.context.authentication;

import com.basic.starter.core.login.authentication.Authentication;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:27
 */
public class AuthenticationContext {

    private static final ThreadLocal<Authentication> authThreadLocal = new ThreadLocal<>();

    public static Authentication getAuthentication() {
        return authThreadLocal.get();
    }

    public static void setAuthentication(Authentication authentication) {
        authThreadLocal.set(authentication);
    }

    public static void removeAuthentication() {
        authThreadLocal.remove();
    }
}
