package com.basic.starter.web.i18n.defaulti;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 11:31
 * 默认i18n转换注解
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD, ElementType.TYPE})
public @interface DefaultInternationalization {
}
