package com.basic.starter.web.annotation.request;

import java.lang.annotation.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:03
 * 自定义参数
 */
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface RequestParameter {

    /**
     * 是否需要参数校验，默认需要参数校验
     * @return
     */
    boolean required() default true;
}
