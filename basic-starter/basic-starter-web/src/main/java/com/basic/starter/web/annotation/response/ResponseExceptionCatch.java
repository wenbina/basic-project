package com.basic.starter.web.annotation.response;

import java.lang.annotation.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:06
 */
@Retention(RetentionPolicy.RUNTIME)
@Target({ElementType.METHOD})
@Documented
@Repeatable(ResponseExceptionCatchs.class)
public @interface ResponseExceptionCatch {
    /**
     * 捕获的错误信息
     * @return
     */
    String source() default "";

    /**
     * 返回的错误信息
     * @return
     */
    String target();

    /**
     * 错误返回码
     */
    int code() default -1;

}
