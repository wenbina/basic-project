package com.basic.starter.core.constants;

/**
 * 基础国际化语言key常量
 */
public interface BaseI18nKeyConstants {

    String PAYLOAD_NOT_NULL_KEY = "PAYLOAD_NOT_NULL";
    String SUCCESS_KEY = "SUCCESS";
    String FAIL_KEY = "FAIL";
    String HTTP_REQUEST_METHOD_NOT_SUPPORTED_KEY = "HTTP_REQUEST_METHOD_NOT_SUPPORTED";
    String UNKNOWN_ERROR_KEY = "UNKNOWN_ERROR";

    String APIREQUEST_NO_PARAMETER_TYPE_KEY = "APIREQUEST_NO_PARAMETER_TYPE";

}
