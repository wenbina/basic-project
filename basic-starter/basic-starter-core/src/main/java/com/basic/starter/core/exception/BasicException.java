package com.basic.starter.core.exception;

import com.basic.starter.core.code.Code;
import com.basic.starter.core.constants.BaseCodeConstants;

/**
 * 自定义异常
 */
public class BasicException extends RuntimeException {

    /**
     * 响应状态码
     */
    private Integer code;
    /**
     * 是否需要打印异常信息
     */
    private boolean isPrintExp = true;
    /**
     * 响应消息参数
     */
    private Object[] params;

    public BasicException(Code code) {
        super(code.getMessage());
        this.code = code.getCode();
    }

    public BasicException(Code code, boolean isPrintExp) {
        super(code.getMessage());
        this.code = code.getCode();
        this.isPrintExp = isPrintExp;
    }

    public BasicException(Code code, Object... params) {
        super(code.getMessage());
        this.code = code.getCode();
        this.params = params;
    }

    public BasicException(Integer code, String message) {
        super(message);
        this.code = code;
    }

    public BasicException(Integer code, String message, Object... params) {
        super(message);
        this.code = code;
        this.params = params;
    }

    public BasicException(String message) {
        super(message);
        this.code = BaseCodeConstants.FAIL.getCode();
    }

    public BasicException(String message, Object... params) {
        super(message);
        this.code = BaseCodeConstants.FAIL.getCode();
        this.params = params;
    }

    public BasicException(Throwable e) {
        super(e);
    }

    public Integer getCode() {
        return code;
    }

    public Object[] getParams() {
        return params;
    }

    public boolean isPrintExp() {
        return isPrintExp;
    }
}
