package com.basic.starter.core.login;

import com.basic.starter.core.login.authentication.Authentication;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 10:37
 */
public interface TokenStore {

    /**
     * 存储token
     */
    void saveToken(Authentication authentication);

    /**
     * 移除token
     */
    void removeToken(String token);

    /**
     * 通过token解析Authentication
     */
    Authentication readAuthenticationByToken(String token);
}
