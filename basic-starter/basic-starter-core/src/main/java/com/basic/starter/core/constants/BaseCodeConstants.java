package com.basic.starter.core.constants;

import com.basic.starter.core.code.Code;

import static com.basic.starter.core.constants.BaseI18nKeyConstants.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/30 14:21
 */
public interface BaseCodeConstants {

    Code SUCCESS = new Code(0, SUCCESS_KEY);
    Code FAIL = new Code(-1, FAIL_KEY);
    Code HTTP_REQUEST_METHOD_NOT_SUPPORTED = new Code(-200004, HTTP_REQUEST_METHOD_NOT_SUPPORTED_KEY);
    Code UNKNOWN_ERROR = new Code(-9999999, UNKNOWN_ERROR_KEY);
    Code APIREQUEST_NO_PARAMETER_TYPE = new Code(-200003, APIREQUEST_NO_PARAMETER_TYPE_KEY);
}
