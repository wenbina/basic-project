package com.basic.starter.core.base.request;

/**
 * 通用api请求对象
 */
public class ApiRequest<T> extends ApiBaseRequest {

    private T payload;

    public ApiRequest() {
    }

    public ApiRequest(T payload) {
        this.payload = payload;
    }

    public T getPayload() {
        return payload;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }
}
