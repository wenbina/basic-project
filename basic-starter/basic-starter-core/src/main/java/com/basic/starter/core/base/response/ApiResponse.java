package com.basic.starter.core.base.response;

import com.basic.starter.core.code.Code;
import com.basic.starter.core.constants.BaseCodeConstants;
import com.fasterxml.jackson.annotation.JsonIgnore;

import java.io.Serializable;

public class ApiResponse <T> implements Serializable {

    /**
     * 响应状态码
     */
    private Integer code;
    /**
     * 响应信息
     */
    private String message;
    /**
     * debug
     */
    private String debug;

    /**
     * 响应信息参数
     */

    @JsonIgnore
    private Object[] params;
    /**
     * 响应数据
     */
    private T payload;


    public ApiResponse() {
    }

    /**
     * 统一返回操作成功提示
     */
    public static ApiResponse ok() {
        ApiResponse result = new ApiResponse<>();
        result.setCode(BaseCodeConstants.SUCCESS.getCode());
        result.setMessage(BaseCodeConstants.SUCCESS.getMessage());
        return result;
    }

    /**
     * 返回操作成功提示及响应数据
     */
    public static <T> ApiResponse ok(T payload) {
        ApiResponse result = new ApiResponse<>();
        result.setCode(BaseCodeConstants.SUCCESS.getCode());
        result.setMessage(BaseCodeConstants.SUCCESS.getMessage());
        result.setPayload(payload);
        return result;
    }

    /**
     * 统一返回操作失败提示
     */
    public static ApiResponse error() {
        ApiResponse result = new ApiResponse<>();
        result.setCode(BaseCodeConstants.FAIL.getCode());
        result.setMessage(BaseCodeConstants.FAIL.getMessage());
        return result;
    }

    /**
     * 自定义响应失败的提示
     */
    public static ApiResponse error(Code code) {
        ApiResponse result = new ApiResponse<>();
        result.setCode(code.getCode());
        result.setMessage(code.getMessage());
        return result;
    }

    /**
     * 自定义响应数据
     */
    public static <T> ApiResponse error(Code code, T payload) {
        ApiResponse result = new ApiResponse<>();
        result.setCode(code.getCode());
        result.setMessage(code.getMessage());
        result.setPayload(payload);
        return result;
    }

    /**
     * 自定义状态码
     */
    public ApiResponse code(Integer code) {
        this.setCode(code);
        return this;
    }

    /**
     * 自定义状态码描述信息
     */
    public ApiResponse message(String message) {
        this.setMessage(message);
        return this;
    }

    /**
     * 自定义响应数据
     */
    public ApiResponse payload(T payload) {
        this.setPayload(payload);
        return this;
    }

    /**
     * 自定义debug数据
     */
    public ApiResponse debug(String debug) {
        this.setDebug(debug);
        return this;
    }

    /**
     * 自定义响应信息参数
     */
    public ApiResponse params(Object... params) {
        this.setParams(params);
        return this;
    }

    public void setCode(Integer code) {
        this.code = code;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public void setPayload(T payload) {
        this.payload = payload;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }

    public T getPayload() {
        return payload;
    }

    public Object[] getParams() {
        return params;
    }

    public void setParams(Object[] params) {
        this.params = params;
    }

    public String getDebug() {
        return debug;
    }

    public void setDebug(String debug) {
        this.debug = debug;
    }
}
