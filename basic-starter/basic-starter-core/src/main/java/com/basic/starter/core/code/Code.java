package com.basic.starter.core.code;

import java.io.Serializable;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/30 14:19
 */
public class Code implements Serializable {

    private Integer code;

    private String message;

    public Code(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Integer getCode() {
        return code;
    }

    public String getMessage() {
        return message;
    }
}
