package com.basic.starter.core.base.request;

/**
 * 请求基本参数
 */
public class ApiBaseRequest {

    // token
    private String token;

    // 语言
    private String language;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }
}
