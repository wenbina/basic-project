package com.basic.starter.cache.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/28 15:33
 */
@Configuration
@ConfigurationProperties(prefix = "basic.caffeine.cache")
public class CaffeineCacheProperties {

    // 缓存名称
    private String name;

    // 缓存容量
    private Integer capacity = 1000;

    // 缓存最大大小
    private Integer maximumSize = 1500;

    // 缓存失效时间（秒）
    private Integer timeout = 7200;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getCapacity() {
        return capacity;
    }

    public void setCapacity(Integer capacity) {
        this.capacity = capacity;
    }

    public Integer getMaximumSize() {
        return maximumSize;
    }

    public void setMaximumSize(Integer maximumSize) {
        this.maximumSize = maximumSize;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }
}
