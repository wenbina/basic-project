package com.basic.starter.cache.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import java.time.Duration;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/28 15:10
 */
@Configuration
@ConfigurationProperties(prefix = "basic.redis")
public class RedisProperties {

    // Redis服务器地址
    private String host = "localhost";
    // Redis服务器连接端口
    private Integer port = 6379;
    // Redis数据库索引（默认为0）
    private Integer database = 0;
    // Redis服务器连接密码（默认为空）
    private String password;
    // 连接超时时间（毫秒）
    private Integer timeout = 2000;
    // 缓存key统一前缀
    private String keySerializerPrefix;
    // Spring Cache配置
    private Cache cache = new Cache();
    // 连接池设置
    private Pool pool = new Pool();

    public static class Cache {
        //缓存过期时间，仅针对cache注解有效 手动配置时该单位为毫秒
        private Duration timeToLive = Duration.ofSeconds(60);

        public Duration getTimeToLive() {
            return timeToLive;
        }

        public void setTimeToLive(Duration timeToLive) {
            this.timeToLive = timeToLive;
        }
    }

    public static class Pool {
        // 连接池最大连接数（使用负值表示没有限制）
        private Integer maxTotal = 300;
        // 连接池最大阻塞等待时间（使用负值表示没有限制）
        private Integer maxWait = -1;
        // 连接池中的最大空闲连接
        private Integer maxIdle = 10;
        // 连接池中的最小空闲连接
        private Integer minIdle = 0;
        // 连接池关闭超时时间（毫秒）
        private Integer shutdownTimeout = 2000;

        public Integer getMaxTotal() {
            return maxTotal;
        }

        public void setMaxTotal(Integer maxTotal) {
            this.maxTotal = maxTotal;
        }

        public Integer getMaxWait() {
            return maxWait;
        }

        public void setMaxWait(Integer maxWait) {
            this.maxWait = maxWait;
        }

        public Integer getMaxIdle() {
            return maxIdle;
        }

        public void setMaxIdle(Integer maxIdle) {
            this.maxIdle = maxIdle;
        }

        public Integer getMinIdle() {
            return minIdle;
        }

        public void setMinIdle(Integer minIdle) {
            this.minIdle = minIdle;
        }

        public Integer getShutdownTimeout() {
            return shutdownTimeout;
        }

        public void setShutdownTimeout(Integer shutdownTimeout) {
            this.shutdownTimeout = shutdownTimeout;
        }
    }

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }

    public Integer getDatabase() {
        return database;
    }

    public void setDatabase(Integer database) {
        this.database = database;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getTimeout() {
        return timeout;
    }

    public void setTimeout(Integer timeout) {
        this.timeout = timeout;
    }

    public String getKeySerializerPrefix() {
        return keySerializerPrefix;
    }

    public void setKeySerializerPrefix(String keySerializerPrefix) {
        this.keySerializerPrefix = keySerializerPrefix;
    }

    public Pool getPool() {
        return pool;
    }

    public void setPool(Pool pool) {
        this.pool = pool;
    }

    public Cache getCache() {
        return cache;
    }

    public void setCache(Cache cache) {
        this.cache = cache;
    }
}
