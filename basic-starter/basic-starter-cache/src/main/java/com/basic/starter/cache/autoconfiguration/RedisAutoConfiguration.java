package com.basic.starter.cache.autoconfiguration;

import com.alibaba.fastjson.parser.ParserConfig;
import com.basic.starter.cache.properties.RedisProperties;
import com.basic.starter.cache.serializer.FastJsonRedisSerializer;
import com.basic.starter.cache.tools.RedisClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.StringUtils;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/28 14:57
 */
@EnableConfigurationProperties({RedisProperties.class})
public class RedisAutoConfiguration {

    /**
     * 使用fastjson来序列化redis的缓存值
     */
    @Bean
    @ConditionalOnMissingBean
    public FastJsonRedisSerializer fastJsonRedisSerializer() {
        //将缓存实体对象加入白名单
        ParserConfig.getGlobalInstance().setAutoTypeSupport(true);
        return new FastJsonRedisSerializer<>(Object.class);
    }

    /**
     * 定制redis缓存操作对象
     */
    @Bean(name = "redisTemplate")
    @Primary
    public RedisTemplate<String, Object> redisTemplate(RedisConnectionFactory connectionFactory) {
        //创建Redis缓存操作助手RedisTemplate对象
        RedisTemplate<String, Object> template = new RedisTemplate<>();
        template.setConnectionFactory(connectionFactory);
        //以下代码为将RedisTemplate的Value序列化方式由JdkSerializationRedisSerializer更换为Jackson2JsonRedisSerializer
        //指明序列化方式
        template.setValueSerializer(fastJsonRedisSerializer());
        //是否启用事务
        //template.setEnableTransactionSupport(true);
        template.afterPropertiesSet();
        return template;
    }

    /**
     * redis操作工具类
     */
    @Bean
    @Primary
    public RedisClient redisClient(@Qualifier("redisTemplate") RedisTemplate redisTemplate) {
        return new RedisClient(redisTemplate);
    }
}
