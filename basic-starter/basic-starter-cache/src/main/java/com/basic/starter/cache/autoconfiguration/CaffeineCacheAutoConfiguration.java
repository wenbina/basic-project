package com.basic.starter.cache.autoconfiguration;

import com.basic.starter.cache.properties.CaffeineCacheProperties;
import com.basic.starter.cache.tools.CaffeineCacheClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.CachePut;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/28 15:28
 */
@EnableConfigurationProperties({CaffeineCacheProperties.class})
@EnableCaching
public class CaffeineCacheAutoConfiguration {

    @Autowired
    private CaffeineCacheProperties caffeineCacheProperties;

    @Bean(name = "caffeineCache")
    @Primary
    public SimpleCacheManager caffeineCache() {
        Cache<Object, Object> cache = Caffeine.newBuilder()
                .initialCapacity(caffeineCacheProperties.getCapacity())
                .maximumSize(caffeineCacheProperties.getMaximumSize())
                .expireAfterWrite(caffeineCacheProperties.getTimeout(), TimeUnit.SECONDS)
                .build();
        SimpleCacheManager simpleCacheManager = new SimpleCacheManager();
        CaffeineCache caffeineCache = new CaffeineCache(caffeineCacheProperties.getName(), cache);
        simpleCacheManager.setCaches(Collections.singletonList(caffeineCache));
        return simpleCacheManager;
    }

    @Bean
    @Primary
    public CaffeineCacheClient caffeineCacheClient(@Qualifier("caffeineCache") SimpleCacheManager caffeineCache) {
        return new CaffeineCacheClient(caffeineCache);
    }


}
