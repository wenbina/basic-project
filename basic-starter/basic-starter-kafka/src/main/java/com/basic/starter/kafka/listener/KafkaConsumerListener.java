package com.basic.starter.kafka.listener;

import cn.hutool.core.collection.CollectionUtil;
import cn.hutool.core.util.ObjectUtil;
import com.basic.starter.kafka.consumer.IMessageQueueConsumerService;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.listener.MessageListener;
import org.springframework.stereotype.Component;

import java.util.List;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/21 16:03
 * 自定义kafka监听类
 */
public class KafkaConsumerListener implements MessageListener<String, String> {


    /**
     * 消息消费
     */
    @Override
    public void onMessage(ConsumerRecord<String, String> record) {
        // 消息消费
        System.out.println("消息消费：" + record.value());
    }
}
