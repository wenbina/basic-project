package com.basic.starter.kafka.producer;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.kafka.core.KafkaTemplate;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/21 10:58
 * kafka生产者
 */
public class ProducerClient {

    Logger logger = LoggerFactory.getLogger(ProducerClient.class);

    private static KafkaTemplate<String, Object> kafkaTemplate;

    public ProducerClient(KafkaTemplate<String, Object> kafkaTemplate) {
        ProducerClient.kafkaTemplate = kafkaTemplate;
    }

    /**
     * 默认发送消息
     */
    public static void defaultSend(String topic, Object message) {
        kafkaTemplate.send(topic, message);
    }



}
