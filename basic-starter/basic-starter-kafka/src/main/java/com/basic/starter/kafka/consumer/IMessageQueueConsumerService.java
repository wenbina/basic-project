package com.basic.starter.kafka.consumer;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/21 16:11
 */
public interface IMessageQueueConsumerService {

    void consume(String data);

    String topic();

}
