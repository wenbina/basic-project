package com.basic.starter.kafka.autoconfiguration;

import com.basic.starter.kafka.consumer.KafkaConsumerListener;
import com.basic.starter.kafka.producer.ProducerClient;
import com.basic.starter.kafka.properties.KafkaProperties;
import com.sun.xml.internal.messaging.saaj.packaging.mime.util.LineInputStream;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.kafka.annotation.EnableKafka;
import org.springframework.kafka.core.*;

import javax.annotation.PostConstruct;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/21 15:28
 * 自定义kafka配置类
 */
@EnableKafka
@EnableConfigurationProperties(KafkaProperties.class)

public class KafkaAutoconfiguration {

    Logger log = LoggerFactory.getLogger(KafkaAutoconfiguration.class);
    @Autowired
    private KafkaProperties kafkaProperties;
    /*@Autowired
    private List<KafkaConsumerListener> kafkaConsumerListeners;*/


    public Map<String, Object> producerConfig() {
        Map<String, Object> props = new HashMap<>();
        props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        props.put(ProducerConfig.RETRIES_CONFIG, kafkaProperties.getProducer().getRetries());
        props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, kafkaProperties.getProducer().getKeySerializer());
        props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, kafkaProperties.getProducer().getValueSerializer());
        return props;
    }

    /**
     * 生产者工厂
     */
    public ProducerFactory<String, Object> producerFactory() {
        return new DefaultKafkaProducerFactory<>(producerConfig());
    }

    public Map<String, Object> consumerConfig() {
        Map<String, Object> props = new HashMap<>();
        props.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaProperties.getBootstrapServers());
        props.put(ConsumerConfig.GROUP_ID_CONFIG, kafkaProperties.getConsumer().getGroupId());
        props.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, kafkaProperties.getConsumer().getKeyDeserializer());
        props.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, kafkaProperties.getConsumer().getValueDeserializer());
        return props;
    }

    @PostConstruct
    public void init() {
        initKafkaConsumer();
    }

    /**
     * 初始化消费者
     */
    public void initKafkaConsumer() {
        log.info("kafka 消费者开始初始化...");
        /*for (KafkaConsumerListener kafkaConsumerListener : kafkaConsumerListeners) {

        }*/

    }

    /**
     * 消费者工厂
     */
    public ConsumerFactory<String, Object> consumerFactory() {
        return new DefaultKafkaConsumerFactory<>(consumerConfig());
    }


    @Bean
    public KafkaTemplate<String, Object> kafkaTemplate() {
        log.info("==================kafka初始化生产者配置信息====================");
        return new KafkaTemplate<>(producerFactory());
    }

    @Bean
    public ProducerClient producerClient(@Qualifier("kafkaTemplate") KafkaTemplate kafkaTemplate) {
        return new ProducerClient(kafkaTemplate);
    }





}
