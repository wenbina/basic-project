package com.basic.starter.kafka.consumer;

/**
 * @author wen
 * @version 1.0
 * @data 2023/8/1 16:01
 */
public interface KafkaConsumerListener {

    void consume(String msg);

}
