package com.basic.starter.rocketmq.autoconfigure;

import com.basic.starter.rocketmq.annotation.MqConsumer;
import com.basic.starter.rocketmq.consumer.RocketMqConsumerListener;
import com.basic.starter.rocketmq.producer.RocketProducerClient;
import com.basic.starter.rocketmq.properties.RocketMqProperties;
import org.apache.rocketmq.client.consumer.DefaultMQPushConsumer;
import org.apache.rocketmq.client.consumer.listener.ConsumeConcurrentlyStatus;
import org.apache.rocketmq.client.consumer.listener.MessageListenerConcurrently;
import org.apache.rocketmq.client.exception.MQClientException;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.common.message.MessageExt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.condition.ConditionalOnMissingBean;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.nio.charset.StandardCharsets;
import java.util.List;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/19 9:32
 * rocketMQ自定义配置类
 */
@EnableConfigurationProperties(RocketMqProperties.class)
@Component
public class RocketMqAutoconfiguration {

    private static Logger log = LoggerFactory.getLogger(RocketMqAutoconfiguration.class);

    @Autowired
    private RocketMqProperties rocketMqProperties;
    @Autowired
    private List<RocketMqConsumerListener> mqConsumerListeners;

    @PostConstruct
    public void init() {
        initMqConsumer();
    }

    /**
     * 初始化消费者
     */
    public void initMqConsumer() {
        log.info("RocketMQ 消费者开始初始化...");
        for (RocketMqConsumerListener mqConsumerListener : mqConsumerListeners) {
            try {
                Class<?> clazz = mqConsumerListener.getClass();
                MqConsumer mqConsumer = clazz.getAnnotation(MqConsumer.class);
                DefaultMQPushConsumer pushConsumer = new DefaultMQPushConsumer();
                pushConsumer.setNamesrvAddr(rocketMqProperties.getNameServer());
                pushConsumer.setConsumeFromWhere(mqConsumer.consumeFromWhere());
                pushConsumer.subscribe(mqConsumer.topic(),mqConsumer.tag());
                pushConsumer.setConsumerGroup(mqConsumer.consumerGroup());
                pushConsumer.registerMessageListener((MessageListenerConcurrently) (msgs, context) -> {
                    for (MessageExt msg : msgs) {
                        mqConsumerListener.consume(new String(msg.getBody(), StandardCharsets.UTF_8));
                    }
                    return ConsumeConcurrentlyStatus.CONSUME_SUCCESS;
                });
                pushConsumer.start();
            } catch (Exception e) {
                log.error("消息消费失败，原因：{}", e.getMessage());
            }
        }
    }

    /**
     * 设置默认生产者
     */
    @ConditionalOnMissingBean(RocketProducerClient.class)
    @Bean
    public RocketProducerClient initDefaultProducer() {
        log.info("RocketMQ 生产者开始初始化...");
        DefaultMQProducer defaultMQProducer = new DefaultMQProducer();
        defaultMQProducer.setProducerGroup(rocketMqProperties.getProducer().getGroup());
        defaultMQProducer.setNamesrvAddr(rocketMqProperties.getNameServer());
        try {
            defaultMQProducer.start();
        } catch (MQClientException e) {
            log.error("MQ生产者初始化失败，原因：{}", e.getMessage());
        }
        return new RocketProducerClient(defaultMQProducer);
    }


}
