package com.basic.starter.rocketmq.consumer;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/31 14:21
 */
public interface RocketMqConsumerListener {

    void consume(String msg);

}
