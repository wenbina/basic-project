package com.basic.starter.rocketmq.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/19 9:26
 */
@Configuration
@ConfigurationProperties(prefix = "rocketmq")
public class RocketMqProperties {

    // 服务器地址
    private String nameServer;

    // 生产者
    private Producer producer;

    // 生产者
    public static class Producer {

        // 生产者组名称
        private String group;

        // 消息发送超时时间
        private Integer sendMessageTimeout;

        // 消息发送失败重试次数
        private Integer retryTimesWhenSendFailed;

        public String getGroup() {
            return group;
        }

        public void setGroup(String group) {
            this.group = group;
        }

        public Integer getSendMessageTimeout() {
            return sendMessageTimeout;
        }

        public void setSendMessageTimeout(Integer sendMessageTimeout) {
            this.sendMessageTimeout = sendMessageTimeout;
        }

        public Integer getRetryTimesWhenSendFailed() {
            return retryTimesWhenSendFailed;
        }

        public void setRetryTimesWhenSendFailed(Integer retryTimesWhenSendFailed) {
            this.retryTimesWhenSendFailed = retryTimesWhenSendFailed;
        }
    }

    public String getNameServer() {
        return nameServer;
    }

    public void setNameServer(String nameServer) {
        this.nameServer = nameServer;
    }

    public Producer getProducer() {
        return producer;
    }

    public void setProducer(Producer producer) {
        this.producer = producer;
    }
}
