package com.basic.starter.rocketmq.producer;

import com.alibaba.fastjson.JSON;
import org.apache.rocketmq.client.producer.DefaultMQProducer;
import org.apache.rocketmq.client.producer.selector.SelectMessageQueueByHash;
import org.apache.rocketmq.common.message.Message;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/19 9:15
 * 消息生产者
 */
public class RocketProducerClient {

    private static Logger log = LoggerFactory.getLogger(RocketProducerClient.class);

    private static DefaultMQProducer producer;

    public RocketProducerClient(DefaultMQProducer producer) {
        RocketProducerClient.producer = producer;
    }

    /**
     * 发送普通消息
     */
    public static <T> void send(String topic, String tag, T dto) {
        try {
            // 转换dto为String类型
            String body = JSON.toJSONString(dto);
            // 构建message
            Message message = new Message(topic, tag, body.getBytes("utf-8"));
            // 发送消息
            producer.send(message);
        } catch (Exception e) {
            log.error("消息发送失败，错误信息：{}", e);
            throw new RuntimeException(e.getMessage());
        }
    }

    /**
     * 发送延时消息
     */
    public static <T> void sendDelayMsg(String topic, String tag, T dto, Integer delayLevel) {
        try {
            // 转换dto为String类型
            String body = JSON.toJSONString(dto);
            // 构建message
            Message message = new Message(topic, tag, body.getBytes("utf-8"));
            // messageDelayLevel=1s 5s 10s 30s 1m 2m 3m 4m 5m 6m 7m 8m 9m 10m 20m 30m 1h 2h
            message.setDelayTimeLevel(delayLevel);
            // 发送消息
            producer.send(message);
        } catch (Exception e) {
            log.error("延时消息发送失败，错误信息：{}", e);
            throw new RuntimeException(e.getMessage());
        }
    }

}
