package com.basic.starter.rocketmq.annotation;

import org.apache.rocketmq.common.consumer.ConsumeFromWhere;
import org.springframework.stereotype.Component;

import java.lang.annotation.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/31 14:19
 */
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
@Documented
@Component
public @interface MqConsumer {

    String consumerGroup();

    ConsumeFromWhere consumeFromWhere() default ConsumeFromWhere.CONSUME_FROM_LAST_OFFSET;

    String topic();

    String tag();
}
