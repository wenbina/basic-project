package com.basic.starter.elasticsearch.properties;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author wen
 * @date 2023/8/30
 */
@Configuration
@ConfigurationProperties(prefix = "basic.elasticsearch")
public class ElasticsearchProperties {


    // 地址
    private String host;

    // 端口
    private Integer port;

    public String getHost() {
        return host;
    }

    public void setHost(String host) {
        this.host = host;
    }

    public Integer getPort() {
        return port;
    }

    public void setPort(Integer port) {
        this.port = port;
    }
}
