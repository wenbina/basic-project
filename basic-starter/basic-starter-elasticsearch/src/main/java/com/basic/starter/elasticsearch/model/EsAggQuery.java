package com.basic.starter.elasticsearch.model;

import java.util.List;

/**
 * @author wen
 * @date 2023/8/31
 */
public class EsAggQuery {

    // 索引名称
    private String indexName;

    // 聚合字段
    private List<String> aggFields;

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public List<String> getAggFields() {
        return aggFields;
    }

    public void setAggFields(List<String> aggFields) {
        this.aggFields = aggFields;
    }
}
