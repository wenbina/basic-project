package com.basic.starter.elasticsearch.model;

import org.elasticsearch.search.sort.SortOrder;

import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @date 2023/8/30
 * elasticsearch分页查询参数
 */
public class EsPageQuery {

    // 索引
    private String indexName;

    // 当前页
    private Integer page;

    // 页面大小
    private Integer size;

    // 排序字段
    private Map<String, SortOrder> sortMap;

    // 包含字段
    private List<String> includeFields;

    // 过滤字段
    private List<String> excludeFields;

    // must
    private List<String> mustFields;

    // should
    private List<String> shouldFields;

    // range
    private Map<String, Map<String, Integer>> rangeFieldMap;


    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indexName) {
        this.indexName = indexName;
    }

    public Integer getPage() {
        return page;
    }

    public void setPage(Integer page) {
        this.page = page;
    }

    public Integer getSize() {
        return size;
    }

    public void setSize(Integer size) {
        this.size = size;
    }

    public Map<String, SortOrder> getSortMap() {
        return sortMap;
    }

    public void setSortMap(Map<String, SortOrder> sortMap) {
        this.sortMap = sortMap;
    }

    public List<String> getIncludeFields() {
        return includeFields;
    }

    public void setIncludeFields(List<String> includeFields) {
        this.includeFields = includeFields;
    }

    public List<String> getExcludeFields() {
        return excludeFields;
    }

    public void setExcludeFields(List<String> excludeFields) {
        this.excludeFields = excludeFields;
    }

    public List<String> getMustFields() {
        return mustFields;
    }

    public void setMustFields(List<String> mustFields) {
        this.mustFields = mustFields;
    }

    public List<String> getShouldFields() {
        return shouldFields;
    }

    public void setShouldFields(List<String> shouldFields) {
        this.shouldFields = shouldFields;
    }

    public Map<String, Map<String, Integer>> getRangeFieldMap() {
        return rangeFieldMap;
    }

    public void setRangeFieldMap(Map<String, Map<String, Integer>> rangeFieldMap) {
        this.rangeFieldMap = rangeFieldMap;
    }
}
