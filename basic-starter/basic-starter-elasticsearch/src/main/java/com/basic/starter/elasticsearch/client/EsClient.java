package com.basic.starter.elasticsearch.client;

import cn.hutool.core.collection.CollectionUtil;
import com.alibaba.fastjson.JSON;
import com.basic.starter.elasticsearch.model.EsAggQuery;
import com.basic.starter.elasticsearch.model.EsPageQuery;
import com.fasterxml.jackson.databind.util.JSONWrappedObject;
import org.elasticsearch.action.admin.indices.delete.DeleteIndexRequest;
import org.elasticsearch.action.bulk.BulkRequest;
import org.elasticsearch.action.bulk.BulkResponse;
import org.elasticsearch.action.delete.DeleteRequest;
import org.elasticsearch.action.delete.DeleteResponse;
import org.elasticsearch.action.get.GetRequest;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.action.search.SearchRequest;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.action.support.master.AcknowledgedResponse;
import org.elasticsearch.action.update.UpdateRequest;
import org.elasticsearch.action.update.UpdateResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestHighLevelClient;
import org.elasticsearch.client.indices.CreateIndexRequest;
import org.elasticsearch.client.indices.CreateIndexResponse;
import org.elasticsearch.client.indices.GetIndexRequest;
import org.elasticsearch.client.indices.GetIndexResponse;
import org.elasticsearch.common.xcontent.XContentType;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;
import org.elasticsearch.search.SearchHits;
import org.elasticsearch.search.aggregations.AggregationBuilder;
import org.elasticsearch.search.aggregations.AggregationBuilders;
import org.elasticsearch.search.builder.SearchSourceBuilder;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @date 2023/8/30
 * es操作工具类
 */
public class EsClient {

    private final static Logger log = LoggerFactory.getLogger(EsClient.class);

    private static RestHighLevelClient client;

    public EsClient(RestHighLevelClient restHighLevelClient) {
        EsClient.client = restHighLevelClient;
    }

    // ============================= 索引 ===================================
    /**
     * 创建索引
     */
    public static boolean createIndex(String indexName, String dsl) {
        // 创建request对象
        CreateIndexRequest request = new CreateIndexRequest(indexName);
        // 准备请求参数：DSL语句
        request.source(dsl, XContentType.JSON);
        boolean ack;
        try {
            // 发送请求
            CreateIndexResponse response = client.indices().create(request, RequestOptions.DEFAULT);
            // 获取响应状态
            ack = response.isAcknowledged();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ack;
    }

    /**
     * 删除索引
     */
    public static boolean deleteIndex(String indexName) {
        DeleteIndexRequest request = new DeleteIndexRequest(indexName);
        boolean ack;
        try {
            // 获取响应
            AcknowledgedResponse response = client.indices().delete(request, RequestOptions.DEFAULT);
            // 获取响应状态
            ack = response.isAcknowledged();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return ack;
    }

    /**
     * 索引查询
     */
    public static void queryIndex(String indexName) {
        GetIndexRequest request = new GetIndexRequest(indexName);
        try {
            // 获取响应
            GetIndexResponse response = client.indices().get(request, RequestOptions.DEFAULT);
            log.debug("响应数据：{}", response.getMappings());
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 判断索引是否存在
     */
    public static boolean indexExist(String indexName) {
        // 创建request对象
        GetIndexRequest request = new GetIndexRequest(indexName);
        // 发起请求
        try {
            return client.indices().exists(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    // ==================================== 文档 ========================================

    /**
     * 新增文档
     */
    public static void addDoc(String indexName, Long docId, String dataJson) {
        IndexRequest request = new IndexRequest();
        request.index(indexName).id(String.valueOf(docId));
        // 转换json数据
        request.source(dataJson, XContentType.JSON);
        try {
            // 插入数据
            IndexResponse response = client.index(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 批量新增文档
     */
    public static void addBatchDoc(String indexName, Map<Long, String> dataJsonMap) {
        if (CollectionUtil.isEmpty(dataJsonMap)) {
            return;
        }
        // 创建新增请求
        BulkRequest request = new BulkRequest();
        for (Map.Entry<Long, String> dataJson : dataJsonMap.entrySet()) {
            request.add(new IndexRequest().index(indexName).id(String.valueOf(dataJson.getKey())).source(dataJson.getValue(), XContentType.JSON));
        }
        try {
            // 插入数据
            BulkResponse bulk = client.bulk(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 修改文档
     */
    public static void updateDoc(String indexName, Long docId, String dataJson) {
        UpdateRequest request = new UpdateRequest();
        request.index(indexName).id(String.valueOf(docId));
        // 转换json数据
        request.doc().source(dataJson, XContentType.JSON);
        try {
            // 修改数据
            UpdateResponse response = client.update(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 查询文档
     */
    public static String queryDoc(String indexName, Long docId) {
        GetRequest request = new GetRequest();
        request.index(indexName).id(String.valueOf(docId));
        String res;
        try {
            // 获取数据
            GetResponse response = client.get(request, RequestOptions.DEFAULT);
            res = response.getSourceAsString();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return res;
    }

    /**
     * 全量查询
     */
    public static List<String> queryAllDoc(String indexName) {
        SearchRequest request = new SearchRequest();
        // 查询索引中全部的数据
        request.indices(indexName).source(new SearchSourceBuilder().query(QueryBuilders.matchAllQuery()));
        List<String> res = new ArrayList<>();
        try {
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
            SearchHits hits = response.getHits();
            for (SearchHit hit : hits) {
                res.add(hit.getSourceAsString());
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
        return res;
    }

    /**
     * 分页查询
     */
    public static void queryDocPage(EsPageQuery esPageQuery) {
        SearchRequest request = new SearchRequest();
        request.indices(esPageQuery.getIndexName());
        SearchSourceBuilder builder = new SearchSourceBuilder().query(QueryBuilders.matchAllQuery());
        builder.from((esPageQuery.getPage() - 1) * esPageQuery.getSize());
        builder.size(esPageQuery.getSize());
        // 排序查询
        if (CollectionUtil.isNotEmpty(esPageQuery.getSortMap())) {
            for (Map.Entry<String, SortOrder> sortField : esPageQuery.getSortMap().entrySet()) {
                builder.sort(sortField.getKey(), sortField.getValue());
            }
        }
        // 过滤字段
        if (CollectionUtil.isNotEmpty(esPageQuery.getIncludeFields()) || CollectionUtil.isNotEmpty(esPageQuery.getExcludeFields())) {
            String[] include = new String[]{};
            String[] exclude = new String[]{};
            List<String> includeFields = esPageQuery.getIncludeFields();
            List<String> excludeFields = esPageQuery.getExcludeFields();
            if (CollectionUtil.isNotEmpty(includeFields)) {
                for (int i = 0; i < includeFields.size(); i++) {
                    include[i] = includeFields.get(i);
                }
            }
            if (CollectionUtil.isNotEmpty(excludeFields)) {
                for (int i = 0; i < excludeFields.size(); i++) {
                    exclude[i] = excludeFields.get(i);
                }
            }
            builder.fetchSource(include, exclude);
        }
        request.source(builder);
        try {
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 组合查询
     */
    public static void composeQuery() {

    }

    /**
     * 模糊查询、高亮查询
     */
    public static void highLightOrVagueQuery() {

    }

    /**
     * 聚合查询
     */
    public static void aggQuery(EsAggQuery esAggQuery) {
        // 创建查询请求
        SearchRequest request = new SearchRequest();
        request.indices(esAggQuery.getIndexName());

        // 分组查询
        List<String> aggFields = esAggQuery.getAggFields();
        if (CollectionUtil.isEmpty(aggFields)) {
            return;
        }
        SearchSourceBuilder builder = new SearchSourceBuilder();
        for (String aggField : aggFields) {
            // 聚合名称
            String aggName = aggField + "Group";
            AggregationBuilder aggBuilder = AggregationBuilders.terms(aggName).field(aggField);
            builder.aggregation(aggBuilder);
        }
        request.source(builder);
        try {
            SearchResponse response = client.search(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }


    }

    /**
     * 删除文档
     */
    public static void deleteDoc(String indexName, Long docId) {
        DeleteRequest request = new DeleteRequest();
        request.index(indexName).id(String.valueOf(docId));
        try {
            // 删除数据
            DeleteResponse response = client.delete(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * 批量删除文档
     */
    public static void deleteBatchDoc(Map<String, List<Long>> docDataMap) {
        // 判断文档数据是否为空
        if (CollectionUtil.isEmpty(docDataMap)) {
            return;
        }
        // 创建请求
        BulkRequest request = new BulkRequest();
        for (Map.Entry<String, List<Long>> docMap : docDataMap.entrySet()) {
            List<Long> docIds = docMap.getValue();
            for (Long docId : docIds) {
                request.add(new DeleteRequest().index(docMap.getKey()).id(String.valueOf(docId)));
            }
        }
        try {
            // 批量删除
            client.bulk(request, RequestOptions.DEFAULT);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
