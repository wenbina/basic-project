package com.basic.starter.elasticsearch.autoconfiguration;

import com.basic.starter.elasticsearch.client.EsClient;
import com.basic.starter.elasticsearch.properties.ElasticsearchProperties;
import org.apache.http.HttpHost;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;


/**
 * @author wen
 * @date 2023/8/30
 * 自定义Elasticsearch配置类
 */
@EnableConfigurationProperties(ElasticsearchProperties.class)
public class ElasticsearchConfiguration {

    Logger log = LoggerFactory.getLogger(ElasticsearchConfiguration.class);

    @Autowired
    private ElasticsearchProperties elasticsearchProperties;

    /**
     * es操作工具类
     */
    @Bean
    @Primary
    public EsClient esClient() {
        RestHighLevelClient client = new RestHighLevelClient(RestClient.builder(
                new HttpHost(elasticsearchProperties.getHost(), elasticsearchProperties.getPort(), "http")
        ));
        log.info("es客户端初始化成功...");
        return new EsClient(client);
    }


}
