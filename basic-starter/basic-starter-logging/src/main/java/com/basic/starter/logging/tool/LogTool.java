package com.basic.starter.logging.tool;

import com.basic.starter.core.exception.BasicException;
import org.slf4j.LoggerFactory;
import org.slf4j.helpers.MessageFormatter;
import org.slf4j.spi.LocationAwareLogger;
import sun.misc.JavaLangAccess;
import sun.misc.SharedSecrets;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.Arrays;
import java.util.List;

/**
 * 统一日志打印工具
 */
public class LogTool {
    // 上层栈深度
    private static final int CALLER_STACK_DEPTH = 2;
    // 空数组
    private static final Object[] EMPTY_ARRAY = new Object[] {};
    // 全类名
    private static final String FQCN = LogTool.class.getName();

    // 获取栈中类信息
    private static LocationAwareLogger getLocationAwareLogger(final int stackDepth) {
        // 通过堆栈信息获取调用当前方法的类名和方法名
        JavaLangAccess access = SharedSecrets.getJavaLangAccess();
        Throwable throwable = new Throwable();
        StackTraceElement frame = access.getStackTraceElement(throwable, stackDepth);
        return (LocationAwareLogger) LoggerFactory.getLogger(frame.getClassName());
    }

    /**
     * 封装trace级别日志 - 日志通知
     */
    public static void trace(List<LogNotify> logNotifies, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.TRACE_INT, msg, EMPTY_ARRAY, null);
        notify(logNotifies, msg);
    }


    /**
     * 封装trace级别日志
     */
    public static void trace(String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.TRACE_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装trace级别日志
     */
    public static void traceTopic(String topic, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(topic + msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.TRACE_INT, msg, EMPTY_ARRAY, null);
    }


    /**
     * 封装debug级别日志
     */
    public static void debug(Class<?> clazz, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        ((LocationAwareLogger) LoggerFactory.getLogger(clazz.getName())).log(null, FQCN, LocationAwareLogger.DEBUG_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装debug级别日志
     */
    public static void debug(String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.DEBUG_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装info级别日志 - 日志通知
     */
    public static void debug(List<LogNotify> logNotifies, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.DEBUG_INT, msg, EMPTY_ARRAY, null);
        notify(logNotifies, msg);
    }

    /**
     * 封装debug级别日志
     */
    public static void debugTopic(String topic, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(topic + msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.DEBUG_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装info级别日志
     */
    public static void info(String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.INFO_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装info级别日志
     */
    public static void infoTopic(String topic, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(topic + msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.INFO_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装info级别日志 - 日志通知
     */
    public static void info(List<LogNotify> logNotifies, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.INFO_INT, msg, EMPTY_ARRAY, null);
        notify(logNotifies, msg);
    }


    /**
     * 封装warn级别日志
     */
    public static void warn(String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.WARN_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装warn级别日志
     */
    public static void warnTopic(String topic, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(topic + msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.WARN_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装warn级别日志 - 日志通知
     */
    public static void warn(List<LogNotify> logNotifies, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.WARN_INT, msg, EMPTY_ARRAY, null);
        notify(logNotifies, msg);
    }

    /**
     * 封装error级别日志
     */
    public static void error(String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装error级别日志
     */
    public static void errorTopic(String topic, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(topic + msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, EMPTY_ARRAY, null);
    }

    /**
     * 封装error级别日志 - 日志通知
     */
    public static void error(List<LogNotify> logNotifies,String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, EMPTY_ARRAY, null);
        notify(logNotifies, msg);
    }

    /**
     * 封装error级别日志 - 日志通知
     */
    public static void errorTopic(List<LogNotify> logNotifies,String topic, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(topic + msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, EMPTY_ARRAY, null);
        notify(logNotifies, msg);
    }

    /**
     * 封装error级别日志 - 日志通知
     */
    public static void errorTopic(LogNotify logNotify, String topic, String msg, Object... arguments) {
        msg = MessageFormatter.arrayFormat(topic + msg, arguments).getMessage();
        getLocationAwareLogger(CALLER_STACK_DEPTH).log(null, FQCN, LocationAwareLogger.ERROR_INT, msg, EMPTY_ARRAY, null);
        notify(Arrays.asList(logNotify), msg);
    }

    /**
     * 统一进行日志通知
     */
    private static void notify(List<LogNotify> logNotifies, String message) {
        if (null != logNotifies && logNotifies.size() > 0) {
            logNotifies.parallelStream().forEach(logNotify -> {
                logNotify.logNotify(message);
            });
        }
    }

    /**
     * 判断当前日志级别是否是Debug级别
     */
    public static boolean isDebugEnabled() {
        return getLocationAwareLogger(CALLER_STACK_DEPTH).isDebugEnabled();
    }

    /**
     * 判断当前日志级别是否是Debug级别
     */
    public static boolean isDebugEnabled(Class<?> clazz) {
        return ((LocationAwareLogger) LoggerFactory.getLogger(clazz.getName())).isDebugEnabled();
    }

    /**
     * 判断当前日志级别是否是Info级别
     */
    public static boolean isInfoEnabled() {
        return getLocationAwareLogger(CALLER_STACK_DEPTH).isInfoEnabled();
    }

    /**
     * 判断当前日志级别是否是Warn级别
     */
    public static boolean isWarnEnabled() {
        return getLocationAwareLogger(CALLER_STACK_DEPTH).isWarnEnabled();
    }

    /**
     * 判断当前日志级别是否是Trace级别
     */
    public static boolean isTraceEnabled() {
        return getLocationAwareLogger(CALLER_STACK_DEPTH).isTraceEnabled();
    }


    /**
     * 异常堆栈转字符串
     */
    public static String exceptionToString(Throwable e) {
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            if (e == null) {
                return "无具体异常信息";
            }
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return sw.toString();
        } catch (Exception ex) {
            error("异常堆栈转字符串异常: {}", ex);
            return "";
        } finally {
            sw.flush();
            pw.flush();
            try {
                sw.close();
            } catch (Exception ex) {}
            try {
                pw.close();
            } catch (Exception ex) {}
        }
    }

    /**
     * 异常堆栈转字符串
     */
    public static String exceptionToEsellString(Throwable e) {
        if (e instanceof BasicException) {
            return e.getMessage();
        }
        StringWriter sw = null;
        PrintWriter pw = null;
        try {
            if (e == null) {
                return "无具体异常信息";
            }
            sw = new StringWriter();
            pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            return sw.toString();
        } catch (Exception ex) {
            error("异常堆栈转字符串异常: {}", ex);
            return "";
        } finally {
            sw.flush();
            pw.flush();
            try {
                sw.close();
            } catch (Exception ex) {}
            try {
                pw.close();
            } catch (Exception ex) {}
        }
    }
}
