package com.basic.starter.logging.constants;

public interface LogConstants {

    String MP_MAPPER_PATH = "com.basic.**.mapper.**Mapper.**";

    String MP_CLASS_NAME = "mpclass";

    String MP_METHOD_NAME = "mpmethod";

    String MP_LINE_NAME = "mpline";
}
