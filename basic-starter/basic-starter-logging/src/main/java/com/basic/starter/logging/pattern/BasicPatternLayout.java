package com.basic.starter.logging.pattern;

import ch.qos.logback.classic.PatternLayout;
import com.basic.starter.logging.constants.LogConstants;
import com.basic.starter.logging.mp.convert.BasicMPSqlClassConverter;
import com.basic.starter.logging.mp.convert.BasicMPSqlLineConverter;
import com.basic.starter.logging.mp.convert.BasicMPSqlMethodConverter;

/**
 * 自定义日志输出的格式
 * 自定义日志输出的pattern，定制自己的标签和解析方法
 */
public class BasicPatternLayout extends PatternLayout {

    static {
        defaultConverterMap.put(LogConstants.MP_CLASS_NAME, BasicMPSqlClassConverter.class.getName());
        defaultConverterMap.put(LogConstants.MP_METHOD_NAME, BasicMPSqlMethodConverter.class.getName());
        defaultConverterMap.put(LogConstants.MP_LINE_NAME, BasicMPSqlLineConverter.class.getName());
    }

}
