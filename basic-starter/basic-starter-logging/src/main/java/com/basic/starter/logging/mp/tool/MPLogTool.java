package com.basic.starter.logging.mp.tool;

import com.basic.starter.logging.constants.LogConstants;
import org.springframework.util.AntPathMatcher;

/**
 * MP日志工具类
 */
public class MPLogTool {

    private static final AntPathMatcher pathMatcher = new AntPathMatcher(".");

    private static String MAPPER_PATH = LogConstants.MP_MAPPER_PATH;

    public static boolean isMPMapping(String name) {
        return pathMatcher.match(MAPPER_PATH, name);
    }

}
