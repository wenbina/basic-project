package com.basic.starter.logging.mp.convert;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.pattern.MethodOfCallerConverter;
import ch.qos.logback.classic.spi.CallerData;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.basic.starter.logging.mp.tool.MPLogTool;
import org.springframework.util.StringUtils;

/**
 * 自定义MP日志打印转换器 - MP对应Method
 */
public class BasicMPSqlMethodConverter extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {
        String loggerName = event.getLoggerName();
        try {
            if (MPLogTool.isMPMapping(loggerName)) {
                String methodName = loggerName.substring(loggerName.lastIndexOf(".") + 1);
                if (StringUtils.hasText(methodName)) {
                    return methodName;
                }
            } else {
                StackTraceElement[] cda = event.getCallerData();
                if (cda != null && cda.length > 0) {
                    return cda[0].getMethodName();
                } else {
                    return CallerData.NA;
                }
            }
        } catch (Exception e) {}
        return CallerData.NA;
    }
}
