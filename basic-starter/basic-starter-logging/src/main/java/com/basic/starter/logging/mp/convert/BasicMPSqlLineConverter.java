package com.basic.starter.logging.mp.convert;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.pattern.ClassicConverter;
import ch.qos.logback.classic.pattern.LineOfCallerConverter;
import ch.qos.logback.classic.spi.CallerData;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.basic.starter.logging.mp.tool.MPLogTool;

/**
 * 自定义MP日志打印转换器 - MP对应Line
 */
public class BasicMPSqlLineConverter extends ClassicConverter {

    @Override
    public String convert(ILoggingEvent event) {
        String loggerName = event.getLoggerName();
        try {
            if (MPLogTool.isMPMapping(loggerName)) {
               return CallerData.NA;
            } else {
                StackTraceElement[] cda = event.getCallerData();
                if (cda != null && cda.length > 0) {
                    return Integer.toString(cda[0].getLineNumber());
                } else {
                    return CallerData.NA;
                }
            }
        } catch (Exception e) {}
        return CallerData.NA;
    }
}
