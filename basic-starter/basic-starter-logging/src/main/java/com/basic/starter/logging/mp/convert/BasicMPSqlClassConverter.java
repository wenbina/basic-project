package com.basic.starter.logging.mp.convert;

import ch.qos.logback.classic.PatternLayout;
import ch.qos.logback.classic.pattern.*;
import ch.qos.logback.classic.spi.CallerData;
import ch.qos.logback.classic.spi.ILoggingEvent;
import com.basic.starter.logging.mp.tool.MPLogTool;
import org.springframework.util.StringUtils;

/**
 * 自定义MP日志打印转换器 - MP对应Class
 */
public class BasicMPSqlClassConverter extends ClassicConverter {

    Abbreviator abbreviator = null;

    @Override
    public void start() {
        String optStr = getFirstOption();
        if (optStr != null) {
            try {
                int targetLen = Integer.parseInt(optStr);
                if (targetLen == 0) {
                    abbreviator = new ClassNameOnlyAbbreviator();
                } else if (targetLen > 0) {
                    abbreviator = new TargetLengthBasedClassNameAbbreviator(targetLen);
                }
            } catch (NumberFormatException nfe) {
                // FIXME: better error reporting
            }
        }
    }

    @Override
    public String convert(ILoggingEvent event) {
        String loggerName = event.getLoggerName();
        try {
            if (MPLogTool.isMPMapping(loggerName)) {
                String className = loggerName.substring(0, loggerName.lastIndexOf("."));
                className = className.substring(className.lastIndexOf(".") + 1);
                if (StringUtils.hasText(className)) {
                    return className;
                }
            } else {
                String fqn = getFullyQualifiedName(event);

                if (abbreviator != null) {
                   fqn = abbreviator.abbreviate(fqn);
                }
                return fqn;
            }
        } catch (Exception e) {
        }
        return CallerData.NA;
    }

    protected String getFullyQualifiedName(ILoggingEvent event) {
        return event.getLoggerName();
    }
}
