package com.basic.starter.logging.mp.impl;

import org.apache.ibatis.logging.Log;
import org.slf4j.Logger;

/**
 * 自定义MP的sql日志输出
 */
public class BasicMPjLoggerImpl implements Log {

    private final Logger log;

    public BasicMPjLoggerImpl(Logger logger) {
        log = logger;
    }

    @Override
    public boolean isDebugEnabled() {
        return log.isDebugEnabled();
    }

    @Override
    public boolean isTraceEnabled() {
        return log.isTraceEnabled();
    }

    @Override
    public void error(String s, Throwable e) {
        log.error(s, e);
    }

    @Override
    public void error(String s) {
        log.error(s);
    }

    @Override
    public void debug(String s) {
        log.debug(s);
    }

    @Override
    public void trace(String s) {
        log.trace(s);
    }

    @Override
    public void warn(String s) {
        log.warn(s);
    }
}
