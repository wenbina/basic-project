package com.basic.starter.logging.tool;

/**
 * 日志通知接口
 */
@FunctionalInterface
public interface LogNotify {

    void logNotify(String msg);

}
