package com.basic.project.algorithm.two.pointers;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/3 14:14
 * 双指针
 */
public class TwoPointers {

    public static void main(String[] args) {
        TwoPointers twoPointers = new TwoPointers();
        int[] nums = new int[] {0,0,1,1,1,2,2,3,3,4};
        System.out.println(twoPointers.removeDuplicates(nums));
    }

    /**
     * 删除有序数组中的重复项
     * 给你一个 升序排列的数组nums ，请你原地删除重复出现的元素，使每个元素只出现一次 ，
     * 返回删除后数组的新长度。元素的相对顺序应该保持一致 。然后返回nums中唯一元素的个数
     *
     * 输入：nums = [1,1,2]
     * 输出：2, nums = [1,2,_]
     * 输入：nums = [0,0,1,1,1,2,2,3,3,4]
     * 输出：5, nums = [0,1,2,3,4]
     */
    public int removeDuplicates(int[] nums) {
        // 空值判断
        if (null == nums || nums.length ==0) return -1;
        // 定义前后指针
        int pre = 0, n = nums.length;
        // 遍历数组
        for (int i = 0; i < n; i++) {
            if (nums[i] != nums[pre]) {
                // 两个值不一样，前移指针
                nums[++pre] = nums[i];
            }
        }
        return pre + 1;
    }


}
