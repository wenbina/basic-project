package com.basic.project.algorithm.heap;

import java.util.PriorityQueue;
import java.util.Queue;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/6 17:53
 *
 *
 * 什么是top k
 * 简单来说就是在一组数据里找到频率出现最高的前k个数
 * 经典top k问题：最大（小）k个数，前k个高频元素，第k个最大（小）元素
 *
 *
 */
public class TopK {

    /**
     * top K：基于堆查找数组中最大的k个元素
     */
    public Queue<Integer> topKHeap(int[] nums, int k) {
        Queue<Integer> heap = new PriorityQueue<>();
        // 将数组的前k个元素入堆
        for (int i = 0; i < k; i++) {
            heap.add(nums[i]);
        }
        // 从第k+1个元素开始，维持堆的长度为k
        for (int i = k; i < nums.length; i++) {
            // 若当前元素大于堆顶元素，则将堆顶元素出堆，当前元素入堆
            if (!heap.isEmpty() && nums[i] > heap.peek()) {
                heap.poll();
                heap.add(nums[i]);
            }
        }
        return heap;
    }

    public static void main(String[] args) {
        TopK topK = new TopK();
        int[] nums = new int[] {1, 7, 6, 3, 2};
        int k = 3;
        System.out.println(topK.topKHeap(nums, k));
    }
}
