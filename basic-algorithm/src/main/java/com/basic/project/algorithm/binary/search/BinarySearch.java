package com.basic.project.algorithm.binary.search;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/3 14:13
 * 二分查找
 */
public class BinarySearch {

    //=====================================================题型===============================================================
    /**
     * 二分查找（双闭区间）
     */
    public int binarySearch(int[] nums, int target) {
        // 初始化双闭区间[0,n-1]，即i，j分别指向数组首元素、尾元素
        int i = 0, j = nums.length - 1;
        // 循环，当搜索区间为空时跳出（i>j时为空）
        while (i <= j) {
            int mid = i + (j-i) / 2; // 计算中间值
            if (nums[mid] < target) {
                // 说明target在右区间[mid+1,j]
                i = mid + 1;
            } else if (nums[mid] > target) {
                // 说明target在左区间[i,mid-1]
                j = mid - 1;
            } else {
                // 找到目标元素，返回索引
                return mid;
            }
        }
        // 未找到目标元素
        return -1;
    }

    /**
     * 二分查找（左闭右开）
     */
    public int binarySearchLCRO(int[] nums, int target) {
        // 初始化左闭右开区间， 即i，j分别指向数组首元素、尾元素+1
        int i = 0, j = nums.length;
        // 循环，当搜索区间为空时跳出（i == j为空）
        while (i < j) {
            int mid = i + (j-i) / 2; // 计算中间值
            if (nums[mid] < target) {
                // 说明target在右区间[mid+1,j]
                i = mid + 1;
            } else if (nums[mid] > target) {
                // 说明target在左区间[i,mid-1]
                j = mid - 1;
            } else {
                // 找到目标元素，返回索引
                return mid;
            }
        }
        // 未找到目标元素
        return -1;
    }

    /**
     * 二分查找最左边元素
     */
    public int binarySearchLeft(int[] nums, int target) {
        // 初始化双闭区间
        int i = 0, j = nums.length - 1;
        // 循环，当搜索区间为空时跳出（i > j为空）
        while (i <= j) {
            // 计算中间值
            int mid = i + (j-i) / 2;
            if (nums[mid] < target) {
                // target 在区间[mid+1, j]之间
                i = mid + 1;
            } else if (nums[mid] > target) {
                // target 在区间[i, mid-1]之间
                j = mid - 1;
            } else {
                // target与中间值相等需要找左边元素，首个小于target的元素在区间[i, mid-1]之间
                j = mid - 1;
            }
        }
        if (i == nums.length || nums[i] != target) {
            // 未找到
            return -1;
        }
        return i;
    }

    // 二分查找最右边元素
    public int binarySearchRight(int[] nums, int target) {
        // 初始化双区间
        int i = 0, j = nums.length - 1;
        // 循环，当搜索区间为空时跳出（i > j为空）
        while (i <= j) {
            // 计算中间值
            int mid = i + (j-i) / 2;
            if (nums[mid] > target) {
                // target在区间[i, mid-1]之间
                j = mid - 1;
            } else if (nums[mid] < target) {
                // target 在区间[mid+1, j]之间
                i = mid + 1;
            } else {
                // target与中间值相等需要找右边元素，首个大于target的元素在区间[mid+1, j]之间
                i = mid + 1;
            }
        }
        if (j < 0 || nums[j] != target) {
            // 未找到
            return -1;
        }
        return j;
    }


    //===============================================================题目=======================================================


    /**
     * 搜索插入位置
     * 给定一个排序数组和一个目标值，在数组中找到目标值，并返回其索引。如果目标值不存在于数组中，返回它将会被按顺序插入的位置。
     *
     * 输入: nums = [1,3,5,6], target = 5
     * 输出: 2
     * 输入: nums = [1,3,5,6], target = 2
     * 输出: 1
     * 输入: nums = [1,3,5,6], target = 7
     * 输出: 4
     *
     * 该题采用二分查找方式
     */
    public int searchInsert(int[] nums, int target) {
        // 定义双闭区间
        int i = 0, j = nums.length - 1;
        // 循环，当搜索区间为空时跳出（i > j为空）
        while (i <= j) {
            // 计算中间值
            int mid = i + (j-i) / 2;
            if (nums[mid] < target) {
                // target 在区间[mid+1, j]之间
                i = mid + 1;
            } else if (nums[mid] > target) {
                // target 在区间[i, mid -1]之间
                j = mid - 1;
            } else {
                // target 等于中间值
                return mid;
            }
        }
        // 1：若该值小于数组中得最小值，则i始终为0，返回0
        // 2：若该值大于数组中得最大值，则跳出循环得条件为i>j，返回nums.length
        return i;
    }


    public static void main(String[] args) {
        BinarySearch binarySearch = new BinarySearch();

        // 搜索插入位置
        int[] nums1 = new int[] {1,3,5,6};
        int target1 = 7;
        System.out.println(binarySearch.searchInsert(nums1, target1));
    }

}
