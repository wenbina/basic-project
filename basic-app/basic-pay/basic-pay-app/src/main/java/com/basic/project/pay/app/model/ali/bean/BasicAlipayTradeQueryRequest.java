package com.basic.project.pay.app.model.ali.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:06
 * 支付宝支付 - 支付交易查询参数
 * 这里只设置需要的参数，完整所有参数可查看：https://opendocs.alipay.com/apis/api_1/alipay.trade.query
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicAlipayTradeQueryRequest {

    // 支付宝应用id
    @NotEmpty
    private String appId;
    // 业务参数
    @NotNull
    @Valid
    private BasicAlipayTradeWapQueryModel bizModel;


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class BasicAlipayTradeWapQueryModel {
        // 商户网站唯一订单号
        @NotEmpty
        private String outTradeNo;
        // 商户网站唯一订单号
        private String tradeNo;
    }
}
