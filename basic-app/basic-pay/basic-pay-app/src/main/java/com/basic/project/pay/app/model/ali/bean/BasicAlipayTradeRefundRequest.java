package com.basic.project.pay.app.model.ali.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:10
 * 支付宝支付 - 支付交易关闭参数
 * 这里只设置需要的参数，完整所有参数可查看：https://opendocs.alipay.com/apis/api_1/alipay.trade.refund
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicAlipayTradeRefundRequest {

    // 支付宝应用id
    @NotEmpty
    private String appId;
    // 业务参数
    @NotNull
    @Valid
    private BasicAlipayTradeRefundModel bizModel;


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class BasicAlipayTradeRefundModel {
        // 订单支付时传入的商户订单号,不能和 trade_no同时为空。
        @NotEmpty
        private String outTradeNo;
        // 支付宝交易号，和商户订单号不能同时为空
        private String tradeNo;
        // 标识一次退款请求，同一笔交易多次退款需要保证唯一，如需部分退款，则此参数必传。
        private String outRequestNo;
        // 需要退款的金额，该金额不能大于订单金额,单位为元，支持两位小数
        @NotEmpty
        private String refundAmount;
        // 退款的原因说明
        private String refundReason;
    }
}
