package com.basic.project.pay.app.model.ali.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:12
 * 支付宝支付 - 手机网站支付请求参数
 * 这里只设置需要的参数，完整所有参数可查看：https://opendocs.alipay.com/apis/api_1/alipay.trade.wap.pay
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class BasicAlipayTradeWapPayRequest {

    // 支付宝应用id
    @NotEmpty
    private String appId;
    // 支付宝服务器主动通知商户服务器里指定的页面http/https路径。
    private String notifyUrl;
    // 支付成功的跳转url HTTP/HTTPS开头字符串
    private String returnUrl;
    // 业务参数
    @Valid
    @NotNull
    private BasicAlipayTradeWapPayModel bizModel;


    @Data
    @AllArgsConstructor
    @NoArgsConstructor
    @Builder
    public static class BasicAlipayTradeWapPayModel {
        // 商户网站唯一订单号
        @NotEmpty
        private String outTradeNo;
        // 订单总金额，单位为元，精确到小数点后两位，取值范围[0.01,100000000]
        @NotEmpty
        private String totalAmount;
        // 商品标题/交易标题/订单标题/订单关键字等。注意：不可使用特殊字符，如 /，=，& 等。
        @NotEmpty
        private String subject;
        // 对一笔交易的具体描述信息。如果是多种商品，请将商品描述字符串累加传给body。
        @NotEmpty
        private String body;
        /**
         * 该笔订单允许的最晚付款时间，逾期将关闭交易。
         * 取值范围：5m～15d。m-分钟，h-小时，d-天，1c-当天（1c-当天的情况下，无论交易何时创建，都在0点关闭）。
         * 该参数数值不接受小数点， 如 1.5h，可转换为 90m
         */
        @NotEmpty
        private String timeoutExpress;
    }
}
