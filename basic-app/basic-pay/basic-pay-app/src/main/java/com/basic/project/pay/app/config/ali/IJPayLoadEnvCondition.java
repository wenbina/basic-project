package com.basic.project.pay.app.config.ali;

import org.springframework.context.annotation.Condition;
import org.springframework.context.annotation.ConditionContext;
import org.springframework.core.env.Environment;
import org.springframework.core.type.AnnotatedTypeMetadata;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:23
 */
public class IJPayLoadEnvCondition implements Condition {

    private static final String PROPERTIES_KEY = "spring.profiles.active";

    private static final List<String> ENVS = Arrays.asList("workstation", "dev", "prd");

    @Override
    public boolean matches(ConditionContext ctx, AnnotatedTypeMetadata metadata) {
        Environment env = ctx.getEnvironment();
        String properties = env.getProperty(PROPERTIES_KEY);
        return ENVS.contains(properties);
    }
}
