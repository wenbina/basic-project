package com.basic.project.pay.app.config.ali;

import com.alipay.api.AlipayApiException;
import com.basic.project.common.pay.ali.config.BasicAlipayConfig;
import com.basic.project.common.pay.ali.config.BasicAlipayConfigContext;
import com.basic.project.common.pay.ali.properties.BasicAlipayProperties;
import com.basic.project.pay.app.client.ali.BasicAlipayClient;
import com.basic.starter.logging.tool.LogTool;
import com.basic.starter.web.tool.SpringContextHolder;
import com.ijpay.alipay.AliPayApiConfig;
import com.ijpay.alipay.AliPayApiConfigKit;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Conditional;
import org.springframework.context.annotation.Configuration;

import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:31
 */
@Configuration
@Conditional(IJPayLoadEnvCondition.class)
public class IJPayAlipayConfiguration {

    public IJPayAlipayConfiguration() {
        LogTool.warn("IJPAY支付宝支付配置加载。。。");
    }

    @Bean
    public BasicAlipayClient basicAlipayClient() {
        // 初始化IJPAY的支付参数
        BasicAlipayConfigContext.getAllBasicAlipayConfig().forEach(basicAlipayConfig -> {
            if (basicAlipayConfig.getCertModel()) {
                // 证书模式
                try {
                    AliPayApiConfigKit.putApiConfig(AliPayApiConfig.builder()
                            .setAppId(basicAlipayConfig.getAppId())
                            .setServiceUrl(basicAlipayConfig.getServerUrl())
                            .setPrivateKey(basicAlipayConfig.getAliPayPrivateKey())
                            .setCharset("UTF-8")
                            .setCertModel(true)
                            .setSignType("RSA2")
                            .setAliPayCertPath(basicAlipayConfig.getAlipayPublicCertPath())
                            .setAppCertPath(basicAlipayConfig.getAppCertPath())
                            .setAliPayRootCertPath(basicAlipayConfig.getAlipayRootCertPath())
                            .buildByCert());
                } catch (AlipayApiException e) {
                    LogTool.error("支付宝证书加载失败：{}", LogTool.exceptionToString(e));
                }
            } else {
                // step 非证书模式
                AliPayApiConfigKit.putApiConfig(AliPayApiConfig.builder()
                        .setAppId(basicAlipayConfig.getAppId())
                        .setServiceUrl(basicAlipayConfig.getServerUrl())
                        .setPrivateKey(basicAlipayConfig.getAliPayPrivateKey())
                        .setAliPayPublicKey(basicAlipayConfig.getAliPayPublicKey())
                        .setCharset("UTF-8")
                        .setCertModel(true)
                        .setSignType("RSA2")
                        .build());
            }
        });
        return new BasicAlipayClient();
    }

    // 构建证书模式
    private void certModelBuild() {

    }

    // 构建非证书模式
    private void unCertModelBuild() {

    }
}
