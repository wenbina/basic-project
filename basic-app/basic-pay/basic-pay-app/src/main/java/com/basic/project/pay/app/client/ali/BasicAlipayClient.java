package com.basic.project.pay.app.client.ali;

import com.alibaba.fastjson.JSON;
import com.alipay.api.AlipayApiException;
import com.alipay.api.domain.AlipayTradeWapPayModel;
import com.alipay.api.request.AlipayTradeWapPayRequest;
import com.alipay.api.response.AlipayTradeWapPayResponse;
import com.basic.project.pay.api.model.enums.ali.AlipayReturnCodeEnum;
import com.basic.project.pay.app.model.ali.bean.BasicAlipayTradeWapPayRequest;
import com.basic.starter.logging.tool.LogTool;
import com.ijpay.alipay.AliPayApi;
import com.ijpay.alipay.AliPayApiConfig;
import com.ijpay.alipay.AliPayApiConfigKit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:33
 */
public class BasicAlipayClient {

    /**
     * 手机网站支付
     */
    public static AlipayTradeWapPayResponse wapPay(BasicAlipayTradeWapPayRequest payRequest) {
        LogTool.info("支付宝支付 - 手机网站支付请求参数：{}", JSON.toJSONString(payRequest));
        // 参数校验
        alipayTradeWapPayParamsVerify(payRequest);
        // 构建业务参数
        AlipayTradeWapPayRequest alipayTradeWapPayRequest = aliPayTradeWapParamsBuild(payRequest);
        // 发起支付请求
        AlipayTradeWapPayResponse response = null;
        try {
            response = AliPayApi.pageExecute(alipayTradeWapPayRequest);
        } catch (AlipayApiException e) {
            LogTool.info("支付宝支付 - 手机网站支付请求异常：{}", JSON.toJSONString(e.toString()));
            AlipayTradeWapPayResponse alipayTradeWapPayResponse = new AlipayTradeWapPayResponse();
            alipayTradeWapPayResponse.setCode(AlipayReturnCodeEnum.EXCEPTION_FAIL.getCode());
            alipayTradeWapPayResponse.setMsg(LogTool.exceptionToString(e));
            return alipayTradeWapPayResponse;
        }
        LogTool.info("支付宝支付 - 手机网站支付响应结果：{}", JSON.toJSONString(response));
        return response;
    }

    // 支付参数校验
    private static void alipayTradeWapPayParamsVerify(BasicAlipayTradeWapPayRequest payRequest) {

    }

    // 构建支付业务参数
    private static AlipayTradeWapPayRequest aliPayTradeWapParamsBuild(BasicAlipayTradeWapPayRequest payRequest) {
        AlipayTradeWapPayRequest alipayTradeWapPayRequest = new AlipayTradeWapPayRequest();
        BasicAlipayTradeWapPayRequest.BasicAlipayTradeWapPayModel bizModel = payRequest.getBizModel();
        AlipayTradeWapPayModel payModel = new AlipayTradeWapPayModel();
        payModel.setOutTradeNo(bizModel.getOutTradeNo());
        payModel.setTotalAmount(bizModel.getTotalAmount());
        payModel.setSubject(bizModel.getSubject());
        payModel.setBody(bizModel.getBody());
        payModel.setTimeoutExpress(bizModel.getTimeoutExpress());
        payModel.setProductCode("QUICK_WAP_PAY");
        alipayTradeWapPayRequest.setBizModel(payModel);
        alipayTradeWapPayRequest.setReturnUrl(payRequest.getReturnUrl());
        alipayTradeWapPayRequest.setNotifyUrl(payRequest.getNotifyUrl());
        AliPayApiConfigKit.setThreadLocalAppId(payRequest.getAppId());
        return alipayTradeWapPayRequest;
    }
}
