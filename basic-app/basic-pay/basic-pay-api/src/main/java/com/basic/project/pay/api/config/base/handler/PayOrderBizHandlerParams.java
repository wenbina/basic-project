package com.basic.project.pay.api.config.base.handler;

import com.basic.project.pay.api.model.dto.order.BasicPayOrderDto;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotEmpty;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 12:22
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PayOrderBizHandlerParams extends BasicPayOrderDto {

    // 钱包操作
    @NotEmpty
    private String walletOperation;
    // 支付成功主动通知商户服务器里指定的页面http/https路径。
    private String notifyUrl;
    // 支付成功的跳转url HTTP/HTTPS开头字符串
    private String returnUrl;
    // 支付成功的MQ通知标签
    private String mqNotifyTag;
}
