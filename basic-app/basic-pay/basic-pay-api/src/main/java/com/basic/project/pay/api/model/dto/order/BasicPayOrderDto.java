package com.basic.project.pay.api.model.dto.order;

import lombok.Data;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 10:35
 */
@Data
public class BasicPayOrderDto {

    // 业务订单号，商户业务订单号
    @NotEmpty
    private String bizOrderNo;
    // 商品标题/交易标题/订单标题/订单关键字等。 256个字符
    @NotEmpty
    private String subject;
    // 对一笔交易的具体描述信息。如果是多种商品，请将商品描述字符串累加传给body。 1000个字符
    @NotEmpty
    private String body;
    // 业务类型， 商户业务类型
    @NotEmpty
    private String bizType;
    // 订单金额，单位厘
    @NotNull
    private Long orderAmount;
    // 实际支付金额，单位厘
    @NotNull
    private Long payAmount;
    // 额外扩展的参数，用户支付成功回调通知时进行参数传递
    private String notifyData;
    // 所属系统
    @NotEmpty
    private String system;
    // 支付过期时间，单位毫秒 15 * 60 * 1000 < timeoutExpress < 21600 * 60 * 1000
    @NotNull
    private Long timeoutExpress;
}
