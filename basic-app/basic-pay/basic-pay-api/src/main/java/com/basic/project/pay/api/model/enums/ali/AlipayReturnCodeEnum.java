package com.basic.project.pay.api.model.enums.ali;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:44
 * 支付宝通用返回码枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum AlipayReturnCodeEnum {

    // 接口调用成功
    SUCCESS("10000"),
    // 程序异常失败
    EXCEPTION_FAIL("-1"),
    // 服务不可用
    SERVICE_NOT_AVAILABLE("20000"),
    // 授权权限不足
    INSUFFICIENT_AUTHORIZATION("20001"),
    // 缺少必选参数
    MISSING_REQUIRED_PARAMETERS("40001"),
    // 非法的参数
    ILLEGAL_PARAMETER("40002"),
    // 业务处理失败
    BUSINESS_PROCESSING_FAILURE("40004"),
    // 权限不足
    INSUFFICIENT_PERMISSIONS("40006"),
    ;

    private String code;
}
