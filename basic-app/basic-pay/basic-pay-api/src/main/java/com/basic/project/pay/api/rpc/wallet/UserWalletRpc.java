package com.basic.project.pay.api.rpc.wallet;

import javax.validation.constraints.NotEmpty;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 19:01
 */
public interface UserWalletRpc {

    /**
     * 根据userId和system查询对应的企业钱包金额
     */
    Long queryOrSaveEnterpriseWalletMoneyByUserIdAndSystemCode(@NotEmpty Integer userId, @NotEmpty String systemCode);
}
