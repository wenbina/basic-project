package com.basic.project.pay.api.model.enums.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

import java.util.Arrays;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:45
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ThirdPayChannelEnum {

    // 微信 - JSAPI支付
    WXPAY_JSAPI(ThirdPayClientEnum.WECHAT.name()),

    // 支付宝 - 手机网站支付
    ALIPAY_WAP(ThirdPayClientEnum.ALIPAY.name())
    ;

    private String client;


    /**
     * 根据第三方支付方式判断是否是微信支付
     */
    public static boolean isWxPay(String thirdPayChannel) {
        return Arrays.stream(ThirdPayChannelEnum.values())
                .anyMatch(channelEnum -> channelEnum.name().equalsIgnoreCase(thirdPayChannel)
                        && ThirdPayClientEnum.WECHAT.name().equalsIgnoreCase(channelEnum.getClient()));
    }

    /**
     * 根据第三方支付方式判断是否是支付宝支付
     */
    public static boolean isAlipay(String thirdPayChannel) {
        return Arrays.stream(ThirdPayChannelEnum.values())
                .anyMatch(channelEnum -> channelEnum.name().equalsIgnoreCase(thirdPayChannel)
                        && ThirdPayClientEnum.ALIPAY.name().equalsIgnoreCase(channelEnum.getClient()));
    }


    public static String getClientByThirdPayChannel(String thirdPayChannel) {
        return Arrays.stream(ThirdPayChannelEnum.values())
                .filter(channelEnum -> channelEnum.name().equalsIgnoreCase(thirdPayChannel))
                .map(ThirdPayChannelEnum::getClient)
                .findFirst()
                .orElse(null);
    }
}
