package com.basic.project.pay.api.config.base.handler;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 12:21
 * 聚合支付c2b业务处理统一接口
 */
public interface PayAggregateC2bBizHandler {

    /**
     * 聚合支付C2B确认支付后对业务参数进行回调，并返回支付订单构建所需信息
     */
    PayOrderBizHandlerParams handleBizParams(String bizParams);

}
