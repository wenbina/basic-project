package com.basic.project.pay.api.model.enums.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:46
 * 第三方订单取消操作后订单状态枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ThirdPayOrderCancelStatusEnum {

    // 待支付
    TO_BE_PAID(1),
    // 发起支付请求失败
    PAID_REQUEST_FAIL(2),
    // 支付成功
    PAY_SUCCESS(3),
    // 支付取消
    PAY_CANCEL(4),
    // 未知状态
    UNKNOWN(5),
    ;

    // 取消后订单状态
    private Integer status;
}
