package com.basic.project.pay.api.model.enums.wx;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:56
 * 微信支付交易状态枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum WxPayTrdeStatusEnum {

    // 支付成功
    SUCCESS,
    // 转入退款
    REFUND,
    // 未支付
    UN_PAY,
    // 已关闭
    CLOSED,
    // 已撤销（付款码支付）
    REVOKED,
    // 用户支付中（付款码支付）
    USER_PAYING,
    // 支付失败(其他原因，如银行返回失败)
    PAY_ERROR,
    ;
}
