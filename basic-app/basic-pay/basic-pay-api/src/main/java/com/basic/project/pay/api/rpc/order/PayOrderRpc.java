package com.basic.project.pay.api.rpc.order;

import com.basic.project.pay.api.model.dto.order.PayOrderBalancePayRpcDto;
import com.basic.project.pay.api.model.vo.order.PayOrderBalancePayRpcVo;

import javax.validation.constraints.NotNull;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 19:00
 */
public interface PayOrderRpc {

    /**
     * 余额支付支付订单
     */
    PayOrderBalancePayRpcVo balancePayOrder(@NotNull PayOrderBalancePayRpcDto rpcDto);
}
