package com.basic.project.pay.api.model.vo.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 19:02
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PayOrderBalancePayRpcVo {

    // 支付订单号
    private String tradeOrderNo;
    // 业务订单号，商户业务订单号
    private String bizOrderNo;
    // 支付成功时间
    private Date payDate;
}
