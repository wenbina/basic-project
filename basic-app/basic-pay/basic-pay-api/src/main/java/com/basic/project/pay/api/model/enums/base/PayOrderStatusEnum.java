package com.basic.project.pay.api.model.enums.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:45
 * 支付订单的状态枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum PayOrderStatusEnum {

    // 待支付
    TO_BE_PAID(1),
    // 发起支付请求失败
    PAID_REQUEST_FAIL(2),
    // 支付成功
    PAY_SUCCESS(3),
    // 支付取消
    PAY_CANCEL(4),;

    private Integer status;
}
