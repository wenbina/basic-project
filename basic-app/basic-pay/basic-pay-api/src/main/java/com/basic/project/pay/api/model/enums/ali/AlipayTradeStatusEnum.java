package com.basic.project.pay.api.model.enums.ali;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:44
 * 支付宝支付交易状态枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum AlipayTradeStatusEnum {

    // 交易创建，等待买家付款
    WAIT_BUYER_PAY,
    // 未付款交易超时关闭，或支付完成后全额退款
    TRADE_CLOSED,
    // 交易支付成功
    TRADE_SUCCESS,
    // 交易结束，不可退款
    TRADE_FINISHED,
    ;
}
