package com.basic.project.pay.api.model.dto.order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:59
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PayOrderBalancePayRpcDto extends BasicPayOrderDto {

    // 用户id
    @NotNull
    private Integer userId;
}
