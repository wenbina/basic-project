package com.basic.project.pay.api.model.enums.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.springframework.util.StringUtils;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:45
 * 第三方客户端类型枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum ThirdPayClientEnum {

    // 微信
    WECHAT,
    // 支付宝
    ALIPAY
    ;

    public static String getBrowserTypeFromUserAgent(String userAgent) {
        if (StringUtils.hasText(userAgent) && userAgent.contains("AlipayClient")) {
            return ThirdPayClientEnum.ALIPAY.name();
        } else if (StringUtils.hasText(userAgent) && userAgent.contains("MicroMessenger")) {
            return ThirdPayClientEnum.WECHAT.name();
        }
        return null;
    }
}
