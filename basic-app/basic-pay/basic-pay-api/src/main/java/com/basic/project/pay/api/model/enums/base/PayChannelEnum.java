package com.basic.project.pay.api.model.enums.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:45
 * 支付订单的支付渠道
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum PayChannelEnum {

    // 余额支付
    BALANCE,
    // 微信支付
    WXPAY,
    // 支付宝支付
    ALIPAY,
    // 聚合支付 - C2B
    AGGREGATE_PAY_C2B,
    ;
}
