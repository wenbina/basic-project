package com.basic.project.pay.api.rpc.wx;

import com.basic.project.pay.api.model.dto.wx.WxPayJsApiRpcDto;
import com.basic.project.pay.api.model.vo.wx.WxPayJSAPIRpcVo;

import javax.validation.constraints.NotNull;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 19:01
 */
public interface WxPayRpc {

    /**
     * 微信支付 - JSAPI支付参数获取
     */
    WxPayJSAPIRpcVo queryWxJSAPIPay(@NotNull WxPayJsApiRpcDto rpcDto);
}
