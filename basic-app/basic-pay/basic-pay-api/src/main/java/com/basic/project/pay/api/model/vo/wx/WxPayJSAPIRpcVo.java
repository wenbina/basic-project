package com.basic.project.pay.api.model.vo.wx;

import lombok.Data;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 10:54
 */
@Data
public class WxPayJSAPIRpcVo {

    // 应用id
    private String appId;
    // 预支付交易会话标识
    private String prepayId;
    // 时间戳
    private String timeStamp;
    // 随机字符串
    private String nonceStr;
    // 签名类型
    private String signType;
    // 支付签名
    private String paySign;
}
