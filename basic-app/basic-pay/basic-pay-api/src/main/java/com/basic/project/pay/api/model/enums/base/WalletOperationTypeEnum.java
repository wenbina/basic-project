package com.basic.project.pay.api.model.enums.base;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:46
 * 支付订单钱包操作类型枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum WalletOperationTypeEnum {

    // 余额增加
    ADD("+"),
    // 余额减少
    SUB("-"),
    // 不操作余额
    NONE("")
    ;

    private String flag;
}
