package com.basic.project.pay.api.config.base.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.Date;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 12:21
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class PayOrderNotifyBean implements Serializable {

    // 用户id
    private Integer userId;
    // 支付订单号
    private String tradeOrderNo;
    // 支付渠道
    private String payChannel;
    // 第三方支付渠道
    private String thirdPayChannel;
    // 第三方支付客户端
    private String thirdPayClient;
    // 支付成功时间
    private Date payDate;
    // 业务回调数据
    private String notifyData;


}
