package com.basic.project.pay.api.model.enums.wx;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 18:47
 * 微信通用返回码枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum WxPayReturnCodeEnum {

    // 成功
    SUCCESS("SUCCESS"),
    // 失败
    FAIL("FAIL"),
    // 程序异常失败
    EXCEPTION_FAIL("EXCEPTION_FAIL"),;

    private String code;
}
