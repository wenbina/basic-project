package com.basic.project.common.thread.pool.forkjoin;

import com.basic.starter.logging.tool.LogNotify;
import com.basic.starter.logging.tool.LogTool;

import java.util.List;
import java.util.concurrent.*;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 12:02
 */
public class TraceForkJoinPool extends ForkJoinPool {

    private static final int MAX_CAP = 0x7fff;
    private final static String SHUTDOWN_ARG = "shutdown";
    private final static String SHUTDOWN_NOW_ARG = "shutdownNow";
    private final static String TERMINATED_ARG = "terminated";

    // 线程池内置方法执行日志模板
    private final static String THREAD_POOL_METHOD_EXEC = "- TraceForkJoinPool执行{}方法: \n" +
            "- 线程池名称： {} \n" +
            "- 线程池状态信息： {} \n";
    // 线程池任务执行错误日志模板
    private final static String THREAD_TASK_EXEC_ERROR = "- TraceForkJoinPool线程任务执行错误(仅记录首次错误): \n" +
            "- 线程池名称： {} \n" +
            "- 线程名称： {} \n" +
            "- 任务详情： {} \n" +
            "- 线程池状态信息： {} \n" +
            "- 异常信息：\n {} \n" +
            "- 堆栈追踪：\n {} \n";

    // 线程池名称
    private String poolName;

    // 方法执行超时时间
    private Long timeout = 5000L;

    private BlockingQueue<TraceForkJoinTaskInfo> exceptionQueue = new LinkedBlockingQueue<>(1);

    // trace日志通知方式
    private List<LogNotify> traceLogNotifies;
    // info日志通知方式
    private List<LogNotify> infoLogNotifies;
    // warn日志通知方式
    private List<LogNotify> warnLogNotifies;
    // error日志通知方式
    private List<LogNotify> errorLogNotifies;

    public static TraceForkJoinPool defaultTraceForkJoinPool(String poolName) {
        return new TraceForkJoinPool(poolName, Math.min(MAX_CAP, Runtime.getRuntime().availableProcessors() * 2),
                new TraceForkJoinWorkerThreadFactory(),
                null,
                false);
    }

    /**
     * Fork/Join 参数全自定义
     */
    public static TraceForkJoinPool defaultTraceForkJoinPool(String poolName, int parallelism,
                                                              ForkJoinPool.ForkJoinWorkerThreadFactory factory,
                                                              Thread.UncaughtExceptionHandler handler,
                                                              boolean asyncMode) {
        return new TraceForkJoinPool(poolName, parallelism, factory, handler, asyncMode);
    }

    /**
     * 自定义可追踪的线程工厂作为Fork/Join默认线程工厂
     */
    static final class TraceForkJoinWorkerThreadFactory implements ForkJoinPool.ForkJoinWorkerThreadFactory {
        @Override
        public ForkJoinWorkerThread newThread(ForkJoinPool pool) {
            return new TraceForkJoinWorkerThread((TraceForkJoinPool) pool);
        }
    }

    /**
     * Fork/Join 默认构造方法
     */
    private TraceForkJoinPool(String poolName, int parallelism,
                              ForkJoinPool.ForkJoinWorkerThreadFactory factory,
                              Thread.UncaughtExceptionHandler handler,
                              boolean asyncMode) {
        super(parallelism, factory, handler, asyncMode);
        this.poolName = poolName;
    }

    @Override
    public void shutdown() {
        LogTool.warn(warnLogNotifies, THREAD_POOL_METHOD_EXEC, SHUTDOWN_ARG, poolName, this.toString());
        sendNotifications();
        super.shutdown();
    }

    @Override
    public List<Runnable> shutdownNow() {
        LogTool.warn(warnLogNotifies, THREAD_POOL_METHOD_EXEC, SHUTDOWN_NOW_ARG, this.toString());
        sendNotifications();
        return super.shutdownNow();
    }

    @Override
    public <T> ForkJoinTask<T> submit(ForkJoinTask<T> task) {
        return super.submit(task);
    }

    /**
     * 对线程任务执行错误进行通知
     */
    private void sendNotifications() {
        TraceForkJoinTaskInfo traceForkJoinTaskInfo;
        if ((traceForkJoinTaskInfo = exceptionQueue.poll()) != null) {
            LogTool.error(errorLogNotifies, THREAD_TASK_EXEC_ERROR,
                    traceForkJoinTaskInfo.getThreadPoolName(),
                    traceForkJoinTaskInfo.getThreadName(),
                    traceForkJoinTaskInfo.getTaskInfo(),
                    traceForkJoinTaskInfo.getThreadPoolInfo(),
                    LogTool.exceptionToString(traceForkJoinTaskInfo.getError()),
                    LogTool.exceptionToString(traceForkJoinTaskInfo.getStack()));
        }
    }

    /**
     * 线程任务执行信息
     */
    public static class TraceForkJoinTaskInfo {
        // 线程池名称
        private String threadPoolName;
        // 线程名称
        private String threadName;
        // 任务详情
        private String taskInfo;
        // 线程池状态信息
        private String threadPoolInfo;
        // 错误信息
        private Exception error;
        // 堆栈追踪
        private Exception stack;

        public String getThreadPoolName() {
            return threadPoolName;
        }

        public void setThreadPoolName(String threadPoolName) {
            this.threadPoolName = threadPoolName;
        }

        public String getThreadName() {
            return threadName;
        }

        public void setThreadName(String threadName) {
            this.threadName = threadName;
        }

        public String getTaskInfo() {
            return taskInfo;
        }

        public void setTaskInfo(String taskInfo) {
            this.taskInfo = taskInfo;
        }

        public String getThreadPoolInfo() {
            return threadPoolInfo;
        }

        public void setThreadPoolInfo(String threadPoolInfo) {
            this.threadPoolInfo = threadPoolInfo;
        }

        public Exception getError() {
            return error;
        }

        public void setError(Exception error) {
            this.error = error;
        }

        public Exception getStack() {
            return stack;
        }

        public void setStack(Exception stack) {
            this.stack = stack;
        }
    }

    public String getPoolName() {
        return poolName;
    }

    public void setPoolName(String poolName) {
        this.poolName = poolName;
    }

    public Long getTimeout() {
        return timeout;
    }

    public void setTimeout(Long timeout) {
        this.timeout = timeout;
    }

    public BlockingQueue<TraceForkJoinTaskInfo> getExceptionQueue() {
        return exceptionQueue;
    }

    public void setExceptionQueue(BlockingQueue<TraceForkJoinTaskInfo> exceptionQueue) {
        this.exceptionQueue = exceptionQueue;
    }

    public List<LogNotify> getTraceLogNotifies() {
        return traceLogNotifies;
    }

    public void setTraceLogNotifies(List<LogNotify> traceLogNotifies) {
        this.traceLogNotifies = traceLogNotifies;
    }

    public List<LogNotify> getInfoLogNotifies() {
        return infoLogNotifies;
    }

    public void setInfoLogNotifies(List<LogNotify> infoLogNotifies) {
        this.infoLogNotifies = infoLogNotifies;
    }

    public List<LogNotify> getWarnLogNotifies() {
        return warnLogNotifies;
    }

    public void setWarnLogNotifies(List<LogNotify> warnLogNotifies) {
        this.warnLogNotifies = warnLogNotifies;
    }

    public List<LogNotify> getErrorLogNotifies() {
        return errorLogNotifies;
    }

    public void setErrorLogNotifies(List<LogNotify> errorLogNotifies) {
        this.errorLogNotifies = errorLogNotifies;
    }
}
