package com.basic.project.common.thread.client;

import com.basic.project.common.thread.pool.TraceThreadPoolExecutor;
import com.basic.starter.web.tool.SpringContextHolder;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 10:48
 * 跟踪线程池客户端
 */
public class TraceThreadPoolClient {

    private final static Map<String, TraceThreadPoolExecutor> cacheMap = new ConcurrentHashMap<>();

    /**
     * 根据线程池名称获取线程池
     */
    public static TraceThreadPoolExecutor getExecutor(String poolName) {
        // 先从缓存中获取
        TraceThreadPoolExecutor traceThreadPoolExecutor = cacheMap.get(poolName);
        // 缓存获取不到则从容器中获取
        if (null == traceThreadPoolExecutor) {
            traceThreadPoolExecutor = SpringContextHolder.getBean(poolName);
            // 将值放入缓存
            cacheMap.put(poolName, traceThreadPoolExecutor);
        }
        return traceThreadPoolExecutor;
    }
}
