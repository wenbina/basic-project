package com.basic.project.common.tools.http;

import cn.hutool.core.util.ObjectUtil;
import org.apache.http.HttpEntity;
import org.apache.http.StatusLine;
import org.apache.http.client.config.CookieSpecs;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.client.methods.RequestBuilder;
import org.apache.http.config.Registry;
import org.apache.http.config.RegistryBuilder;
import org.apache.http.conn.socket.ConnectionSocketFactory;
import org.apache.http.conn.socket.PlainConnectionSocketFactory;
import org.apache.http.conn.ssl.SSLConnectionSocketFactory;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.impl.conn.PoolingHttpClientConnectionManager;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.net.*;
import java.security.KeyManagementException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.Enumeration;
import java.util.List;
import java.util.Map;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/26 14:04
 */
public class HttpTool {

    private static Logger log = LoggerFactory.getLogger(HttpTool.class);
    private static final String ANYHOST = "0.0.0.0";
    private static final String LOCALHOST = "127.0.0.1";
    private static final Pattern IP_PATTERN = Pattern.compile("\\d{1,3}(\\.\\d{1,3}){3,5}$");
    private final static int TIMEOUT = 15000;
    private final static RequestConfig DEFAULT_REQUEST_CONFIG = config(TIMEOUT);
    private static final PoolingHttpClientConnectionManager connectionManager;
    private static volatile InetAddress LOCAL_ADDRESS = null;


    static {

        SSLConnectionSocketFactory sslConnectionSocketFactory = null;

        try {
            sslConnectionSocketFactory = ignoreSSLConnectionSocketFactory();
        } catch (KeyManagementException | NoSuchAlgorithmException e) {
            sslConnectionSocketFactory = SSLConnectionSocketFactory.getSocketFactory();
        }

        final Registry<ConnectionSocketFactory> socketFactoryRegistry = RegistryBuilder.<ConnectionSocketFactory> create()
                .register("http", PlainConnectionSocketFactory.getSocketFactory()).register("https", sslConnectionSocketFactory).build();

        connectionManager = new PoolingHttpClientConnectionManager(socketFactoryRegistry);
        connectionManager.setMaxTotal(500);
        connectionManager.setDefaultMaxPerRoute(100);
    }


    /**
     * 获取请求IP
     * @param request
     * @return
     */
    public static String getRemoteIp(HttpServletRequest request) {
        String pattern =
                "^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
                        "([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
        String ip = getRemoteIpFromHeader(request);
        if (StringUtils.hasText(ip) && !ip.matches(pattern)) {
            ip = null;
        }
        return ip;
    }


    /**
     * 从请求头获取请求IP
     * @param request
     * @return
     */
    public static String getRemoteIpFromHeader(HttpServletRequest request) {
        // 基于 X-Forwarded-For 获得
        String ip = request.getHeader("X-Forwarded-For");
        if (StringUtils.hasText(ip) && !"unKnown".equalsIgnoreCase(ip)) {
            // 多次反向代理后会有多个ip值，第一个 ip 才是真实 ip
            int index = ip.indexOf(",");
            if (index != -1) {
                return ip.substring(0, index);
            } else {
                return ip;
            }
        }
        // 基于 X-Real-IP 获得
        ip = request.getHeader("X-Real-IP");
        if (!StringUtils.isEmpty(ip) && !"unKnown".equalsIgnoreCase(ip)) {
            return ip;
        }
        // 默认方式
        return request.getRemoteAddr();
    }

    /**
     * get ip address
     *
     * @return String
     */
    public static String getIp() {
        return getLocalAddress().getHostAddress();
    }

    /**
     * Find first valid IP from local network card
     *
     * @return first valid local IP
     */
    public static InetAddress getLocalAddress() {
        if (LOCAL_ADDRESS != null) {
            return LOCAL_ADDRESS;
        }
        InetAddress localAddress = getLocalAddress0();
        LOCAL_ADDRESS = localAddress;
        return localAddress;
    }

    private static InetAddress getLocalAddress0() {
        InetAddress localAddress = null;
        try {
            localAddress = InetAddress.getLocalHost();
            if (localAddress instanceof Inet6Address) {
                Inet6Address address = (Inet6Address) localAddress;
                if (isValidV6Address(address)) {
                    return normalizeV6Address(address);
                }
            } else if (isValidAddress(localAddress)) {
                return localAddress;
            }
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
        }
        try {
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            if (null == interfaces) {
                return localAddress;
            }
            while (interfaces.hasMoreElements()) {
                try {
                    NetworkInterface network = interfaces.nextElement();
                    Enumeration<InetAddress> addresses = network.getInetAddresses();
                    while (addresses.hasMoreElements()) {
                        try {
                            InetAddress address = addresses.nextElement();
                            if (address instanceof Inet6Address) {
                                Inet6Address v6Address = (Inet6Address) address;
                                if (isValidV6Address(v6Address)) {
                                    return normalizeV6Address(v6Address);
                                }
                            } else if (isValidAddress(address)) {
                                return address;
                            }
                        } catch (Throwable e) {
                            log.error(e.getMessage(), e);
                        }
                    }
                } catch (Throwable e) {
                    log.error(e.getMessage(), e);
                }
            }
        } catch (Throwable e) {
            log.error(e.getMessage(), e);
        }
        return localAddress;
    }


    /**
     * normalize the ipv6 Address, convert scope name to scope id.
     * <p>
     * e.g.
     * convert
     * fe80:0:0:0:894:aeec:f37d:23e1%en0
     * to
     * fe80:0:0:0:894:aeec:f37d:23e1%5
     * <p>
     * The %5 after ipv6 address is called scope id.
     * see java doc of {@link Inet6Address} for more details.
     *
     * @param address the input address
     * @return the normalized address, with scope id converted to int
     */
    private static InetAddress normalizeV6Address(Inet6Address address) {
        String addr = address.getHostAddress();
        int i = addr.lastIndexOf('%');
        if (i > 0) {
            try {
                return InetAddress.getByName(addr.substring(0, i) + '%' + address.getScopeId());
            } catch (UnknownHostException e) {
                // ignore
                log.debug("Unknown IPV6 address: ", e);
            }
        }
        return address;
    }

    /**
     * valid Inet6Address, if an ipv6 address is reachable.
     *
     * @param address
     * @return
     */
    private static boolean isValidV6Address(Inet6Address address) {
        boolean preferIpv6 = Boolean.getBoolean("java.net.preferIPv6Addresses");
        if (!preferIpv6) {
            return false;
        }
        try {
            return address.isReachable(100);
        } catch (IOException e) {
            // ignore
        }
        return false;
    }

    /**
     * valid Inet4Address
     *
     * @param address
     * @return
     */
    private static boolean isValidAddress(InetAddress address) {
        if (address == null || address.isLoopbackAddress()) {
            return false;
        }
        String name = address.getHostAddress();
        return (name != null
                && !ANYHOST.equals(name)
                && !LOCALHOST.equals(name)
                && IP_PATTERN.matcher(name).matches());
    }

    /**
     * 通过url下载成zip
     * @param zipName
     * @param urls
     * @param response
     * @throws IOException
     */
    public static void zipDownloadFromUrl(String zipName,
                                          List<ZipDownloadFromUrl> urls,
                                          HttpServletResponse response) throws IOException {
        ZipOutputStream zos = null;
        InputStream fis = null;
        try {
            zipName = URLEncoder.encode(zipName, "UTF-8");//转换中文否则可能会产生乱码
            response.setContentType("application/x-download");// 指明response的返回对象是文件流
            response.setHeader("Access-Control-Expose-Headers","Content-Disposition");
            response.setHeader("Content-Disposition", "attachment;filename=" + zipName);// 设置在下载框默认显示的文件名
            zos = new ZipOutputStream(response.getOutputStream());
            for (ZipDownloadFromUrl downloadFromUrl : urls) {
                String url = downloadFromUrl.getUrl();
                String path = downloadFromUrl.getPath();
                zos.putNextEntry(new ZipEntry(path));
                fis = getInputStreamByGet(url);
                if (ObjectUtil.isNull(fis)) {
                    continue;
                }
                byte[] buffer = new byte[1024];
                int r = 0;
                while ((r = fis.read(buffer)) != -1) {
                    zos.write(buffer, 0, r);
                }
                fis.close();
            }
            zos.flush();
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (null != zos) {
                zos.close();
            }
            if (null != fis) {
                fis.close();
            }
        }
    }

    public static InputStream getInputStreamByGet(String url) {
        try {
            HttpURLConnection conn = (HttpURLConnection) new URL(url).openConnection();
            conn.setReadTimeout(5000);
            conn.setConnectTimeout(5000);
            conn.setRequestMethod("GET");

            if (conn.getResponseCode() == HttpURLConnection.HTTP_OK) {
                InputStream inputStream = conn.getInputStream();
                return inputStream;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static class ZipDownloadFromUrl {
        private String url;
        private String path;

        public ZipDownloadFromUrl(String url, String path) {
            this.url = url;
            this.path = path;
        }

        public String getUrl() {
            return url;
        }

        public String getPath() {
            return path;
        }
    }

    public static String get(final String url)  {

        return get(url, "utf-8");
    }

    public static String get(final String url, final String charset) {

        return get(url, null, charset);
    }

    public static String get(final String url, final Map<String, String> headers, final String charset) {

        return get(url, headers, charset, TIMEOUT);
    }

    public static String get(final String url, final Map<String, String> headers, final String charset, final int timeoutSeconds) {

        final RequestBuilder requestBuilder = RequestBuilder.get().setUri(url).setConfig(config(timeoutSeconds));

        if (headers != null) {
            for (Map.Entry<String, String> entry : headers.entrySet()) {
                requestBuilder.addHeader(entry.getKey(), entry.getValue());
            }
        }

        final HttpUriRequest httpRequest = requestBuilder.build();

        try {
            return EntityUtils.toString(execute(url, httpRequest), charset);
        } catch (IOException e) {
            httpRequest.abort();
        }

        return "";
    }

    private static RequestConfig config(final int timeout) {

        if (DEFAULT_REQUEST_CONFIG != null && timeout == TIMEOUT) {
            return DEFAULT_REQUEST_CONFIG;
        }

        return RequestConfig
                .custom()
                .setSocketTimeout(timeout)
                .setConnectTimeout(timeout)
                .setConnectionRequestTimeout(timeout)
                .setCookieSpec(CookieSpecs.STANDARD)
                .build();
    }

    private static HttpEntity execute(final String url, final HttpUriRequest request) throws  IOException {

        final CloseableHttpClient httpClient = HttpClients.custom().setConnectionManager(connectionManager).build();

        final CloseableHttpResponse httpResponse = httpClient.execute(request);

        final StatusLine httpStatus = httpResponse.getStatusLine();

        if (httpStatus.getStatusCode() != 200) {

            if (httpResponse != null) {
                EntityUtils.consumeQuietly(httpResponse.getEntity());
                httpResponse.close();
            }

        }

        return httpResponse.getEntity();

    }
    public static SSLConnectionSocketFactory ignoreSSLConnectionSocketFactory() throws NoSuchAlgorithmException, KeyManagementException {

        final SSLContext sslContext = SSLContext.getInstance("SSLv3");

        final X509TrustManager x509TrustManager = new X509TrustManager() {

            @Override
            public void checkClientTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {

            }

            @Override
            public void checkServerTrusted(java.security.cert.X509Certificate[] paramArrayOfX509Certificate, String paramString) throws CertificateException {

            }

            @Override
            public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                return null;
            }
        };

        sslContext.init(null, new TrustManager[] { x509TrustManager }, null);

        return new SSLConnectionSocketFactory(sslContext);
    }
}
