package com.basic.project.common.tools.email;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.extra.mail.MailAccount;
import cn.hutool.extra.mail.MailUtil;
import com.basic.project.common.constants.redis.RedisKeyConstant;
import com.basic.starter.cache.tools.RedisClient;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/26 14:09
 */
public class EmailTool {

    private static MailAccount mailAccount;

    public EmailTool(MailAccount mailAccount) {
        EmailTool.mailAccount = mailAccount;
    }

    /**
     * 发送验证码：将验证码放入缓存中
     */
    public static void sendCode(String email,
                                Long timeout,
                                TimeUnit timeUnit) {
        // 生成验证码
        int code = (int) ((Math.random() * 9 + 1) * 100000);
        // 发送验证码到对应邮箱
        MailUtil.send(mailAccount, email, EmailConstant.EMAIL_VERIFICATION_CODE_TITLE,
                String.format(EmailConstant.EMAIL_VERIFICATION_CODE_CONTENT, code), false);
        // 缓存验证码
        RedisClient.set(RedisKeyConstant.EMAIL_VERIFICATION_CODE_KEY, email, code, timeout, timeUnit);
    }

    /**
     * 批量发送邮件信息
     */
    public static void batchSendEmail(List<String> emails,
                                     String title,
                                     String content) {
        MailUtil.send(mailAccount, emails, title, content, false);
    }

    /**
     * 获取验证码
     */
    public static String getCodeByCache(String email) {
        Object o = RedisClient.get(RedisKeyConstant.EMAIL_VERIFICATION_CODE_KEY, email);
        return ObjectUtil.isNull(o) ? null : String.valueOf(o);
    }
}
