package com.basic.project.common.tools.email;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/28 9:47
 */
public interface EmailConstant {

    // 验证码邮件标题
    String EMAIL_VERIFICATION_CODE_TITLE = "验证码";

    // 验证码邮件内容
    String EMAIL_VERIFICATION_CODE_CONTENT = "您的验证码是：%s，5分钟内有效，如非本人操作请忽略。";
}
