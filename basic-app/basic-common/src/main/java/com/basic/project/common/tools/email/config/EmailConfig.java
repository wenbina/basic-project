package com.basic.project.common.tools.email.config;

import cn.hutool.extra.mail.MailAccount;
import com.basic.project.common.tools.email.EmailTool;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/26 14:14
 * 邮箱配置类
 */
@Configuration
public class EmailConfig {

    // 添加邮箱账户信息
    @Bean
    @ConfigurationProperties(prefix = "email.config")
    public MailAccount mailAccount() {
        return new MailAccount();
    }

    @Bean
    public EmailTool emailTool(MailAccount mailAccount) {
        return new EmailTool(mailAccount);
    }
}
