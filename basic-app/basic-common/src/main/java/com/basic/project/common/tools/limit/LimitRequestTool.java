package com.basic.project.common.tools.limit;

import cn.hutool.core.util.ObjectUtil;
import com.basic.starter.cache.tools.RedisClient;

import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/26 15:04
 * 限流工具
 */
public class LimitRequestTool {

    // 固定窗口限流开始时间
    private static final String REGULAR_WINDOWS_LIMIT_BEGIN_TIME = "LIMIT:REGULAR_WINDOWS_LIMIT_BEGIN_TIME";

    // 固定窗口已请求数量
    private static final String REGULAR_WINDOWS_LIMIT_COUNT = "LIMIT:REGULAR_WINDOWS_LIMIT_COUNT";

    /**
     * 固定窗口限流
     * 维护一个计数器，将单位时间段当作窗口，计数器记录这个窗口接收请求的次数
     * 1：当次数少于限流阈值，就允许访问且计数器+1
     * 2：当次数大于限流阈值，就拒绝访问
     * 3：当前时间窗口过去之后，计数器清零
     * 针对并发量高情况下需要加分布式锁
     */
    public static Boolean regulaWindowsLimit(Long duration, Long permitCount) {
        // 获取上次窗口时间
        Object beginTime = RedisClient.get(REGULAR_WINDOWS_LIMIT_BEGIN_TIME);
        if (ObjectUtil.isNull(beginTime)) {
            RedisClient.set(REGULAR_WINDOWS_LIMIT_BEGIN_TIME, System.currentTimeMillis());
            beginTime = System.currentTimeMillis();
        }
        // 获取当前时间
        long now = System.currentTimeMillis();
        if (now - (Long)beginTime < duration) {
            // 在固定窗口时间
            // 获取窗口已请求数量
            Object count = RedisClient.get(REGULAR_WINDOWS_LIMIT_COUNT);
            count = ObjectUtil.isNotNull(count) ? count : 0L;
            if ((Long)count < permitCount) {
                RedisClient.incr(REGULAR_WINDOWS_LIMIT_COUNT, 1);
                return true;
            } else {
                return false;
            }
        }
        // 窗口时间过期，重置计数器和时间戳
        RedisClient.set(REGULAR_WINDOWS_LIMIT_COUNT, 0L);
        RedisClient.set(REGULAR_WINDOWS_LIMIT_BEGIN_TIME, System.currentTimeMillis());
        return true;
    }

    /**
     * 滑动窗口限流
     *
     */
    public static Boolean slideWindowsLimit() {
        return true;
    }
}
