package com.basic.project.common.tools.limit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/26 15:10
 */
public interface RateLimiter {

    /**
     * 判断请求是否能够通过
     */
    Boolean tryAcquire();

}
