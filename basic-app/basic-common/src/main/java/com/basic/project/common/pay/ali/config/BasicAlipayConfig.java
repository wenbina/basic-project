package com.basic.project.common.pay.ali.config;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:35
 * 支付宝配置信息
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class BasicAlipayConfig implements Serializable {

    // 支付宝应用
    private String app;
    // 支付宝支付网关地址
    private String serverUrl;
    // 支付宝应用id
    private String appId;
    // 支付宝应用密钥
    private String aliPayPrivateKey;
    // 支付宝应用密钥公钥（在非证书模式时需要设置）
    private String aliPayPublicKey;
    // 是否开启证书模式
    private Boolean certModel = false;
    // 应用公钥证书路径（在证书模式时需要设置）
    private String appCertPath;
    // 支付宝公钥证书文件路径（在证书模式时需要设置）
    private String alipayPublicCertPath;
    // 支付宝CA根证书文件路径（在证书模式时需要设置）
    private String alipayRootCertPath;
}
