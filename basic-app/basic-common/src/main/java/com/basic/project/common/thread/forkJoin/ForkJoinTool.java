package com.basic.project.common.thread.forkJoin;

import lombok.Data;

import javax.naming.InsufficientResourcesException;
import java.util.Arrays;
import java.util.List;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.ForkJoinTask;
import java.util.concurrent.RecursiveTask;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/28 14:27
 */
public class ForkJoinTool {

    private static final Integer MAX = 200;

    // 最大处理任务数
    private static final Integer MAX_TASK_COUNT = 500;

    public static void forkJoinCalculate() {

    }

    public static void main(String[] args) {
        /*ForkJoinPool pool = new ForkJoinPool(4);
        try {
            ForkJoinTask<Integer> taskFuture = pool.submit(new MyForkJoinTask(1, 1001));
            Integer res = taskFuture.get();
            System.out.println("计算结果为：" + res);
        } catch (Exception e) {
            throw new RuntimeException(e);
        }*/
    }

    @Data
    static class Stu {

        private String name;

        private Integer age;

    }

    static class MyForkJoinTask1<T> extends RecursiveTask<Integer> {

        // 计算数据
        private List<T> datas;

        public MyForkJoinTask1(List<T> datas) {
            this.datas = datas;
        }

        @Override
        protected Integer compute() {
            // 条件成立，说明任务所需要计算的数值够小，可以进行计算
            if (datas.size() < MAX_TASK_COUNT) {

            } else {
                // 任务拆分
                //MyForkJoinTask1 subTask1 = new MyForkJoinTask1()

            }
            return -1;
        }
    }

    static class MyForkJoinTask extends RecursiveTask<Integer> {

        // 子任务开始计算的值
        private Integer startValue;

        // 子任务结束计算的值
        private Integer endValue;

        public MyForkJoinTask(Integer startValue, Integer endValue) {
            this.startValue = startValue;
            this.endValue = endValue;
        }

        @Override
        protected Integer compute() {
            // 条件成立，说明这个任务所需要计算的数值分的足够小了，可以进行计算
            if (endValue - startValue < MAX) {
                System.out.println(Thread.currentThread().getName() + "开始计算的部分：开始值：" + startValue + "；结束值：" + endValue);
                int totalValue = 0;
                for (int i = this.startValue; i <= endValue; i++) {
                    totalValue += i;
                }
                return totalValue;
            } else {
                // 将任务进行拆分
                MyForkJoinTask subTask1 = new MyForkJoinTask(startValue, (startValue + endValue) / 2);
                subTask1.fork();
                MyForkJoinTask subTask2 = new MyForkJoinTask((startValue + endValue) / 2 + 1, endValue);
                subTask2.fork();
                return subTask1.join() + subTask2.join();
            }
        }
    }


}
