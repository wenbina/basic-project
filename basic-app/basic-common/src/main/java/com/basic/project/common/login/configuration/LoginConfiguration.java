package com.basic.project.common.login.configuration;

import com.basic.project.common.login.context.BasicAuthenticationContext;
import com.basic.project.common.login.store.RedisTokenStore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.core.RedisTemplate;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:57
 */
@Configuration
public class LoginConfiguration {

    @Bean
    public RedisTokenStore redisTokenStore(RedisTemplate redisTemplate) {
        return new RedisTokenStore(redisTemplate);
    }

    @Bean
    public BasicAuthenticationContext basicAuthenticationContext(RedisTokenStore redisTokenStore) {
        return new BasicAuthenticationContext(redisTokenStore);
    }
}
