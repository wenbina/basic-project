package com.basic.project.common.thread.client;

import com.basic.project.common.thread.pool.forkjoin.TraceForkJoinPool;

import java.util.concurrent.ForkJoinTask;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 12:00
 */
public class TraceForkJoinPoolClient {

    private final static TraceForkJoinPool TRACE_FORK_JOIN_POOL = TraceForkJoinPool.defaultTraceForkJoinPool("");

    public static <T> ForkJoinTask<T> submit(ForkJoinTask<T> task) {
        return TRACE_FORK_JOIN_POOL.submit(task);
    }


}
