package com.basic.project.common.tools.redPackage;


import java.util.Random;

public class RedPackageUtil {

    /**
     * 拆分红包
     */
    public static Integer[] splitRedPackage(int totalMoney, int redPackageNum) {
        Integer[] redPackageNums = new Integer[redPackageNum];
        // 已经被抢夺的红包金额，已经被拆分塞进子红包的金额
        int useMoney = 0;
        for (int i = 0; i < redPackageNum; i++) {
            if (i == redPackageNum - 1) {
                // 最后一个红包金额
                redPackageNums[i] = totalMoney - useMoney;
            } else {
                // 二倍均值算法，每次拆分后塞进子红包的金额 = 随机区间（0，（剩余红包金额M/剩余人数N）* 2）
                int avgMoney = ((totalMoney - useMoney) / (redPackageNum - i) * 2);
                redPackageNums[i] = 1 + new Random().nextInt(avgMoney);
            }
            useMoney += redPackageNums[i];
        }
        return redPackageNums;
    }

    public static void main(String[] args) {
        Integer[] redPackages = splitRedPackage(100, 5);
        for (Integer redPackage : redPackages) {
            System.out.println(redPackage);
        }
    }
}
