package com.basic.project.common.thread.enums;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 10:33
 * 跟踪线程池的类型枚举
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum TraceThreadPoolEnum {

    // 通用线程池
    COMMON_THREAD_POOL("通用线程池"),

    ;

    String label;


}
