package com.basic.project.common.aspect;

import com.basic.starter.logging.tool.LogTool;
import com.basic.starter.web.annotation.track.RequireTrack;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.stereotype.Component;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/13 14:14
 */
@Aspect
@Component
public class TrackAspect {

    /**
     * 切入所有controller
     */
    @Pointcut("execution(* com.basic.project..app.controller..*(..))")
    public void point() {

    }

    /**
     * 埋点
     */
    @AfterReturning("@annotation(requireTrack) && point()")
    public void track(JoinPoint joinPoint, RequireTrack requireTrack) {
        LogTool.info("埋点上报：{}", joinPoint.getArgs());
    }




}
