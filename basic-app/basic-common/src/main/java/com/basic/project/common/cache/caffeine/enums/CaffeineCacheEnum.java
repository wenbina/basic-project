package com.basic.project.common.cache.caffeine.enums;

import lombok.Getter;

import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/15 11:27
 */
@Getter
public enum CaffeineCacheEnum {

    // token
    TOKEN(1000, 1500L, 120L, TimeUnit.MINUTES),;

    Integer capacity;

    Long maximumSize;

    Long timeout;

    TimeUnit timeUnit;

    CaffeineCacheEnum(Integer capacity, Long maximumSize, Long timeout, TimeUnit timeUnit) {
        this.capacity = capacity;
        this.maximumSize = maximumSize;
        this.timeout = timeout;
        this.timeUnit = timeUnit;
    }
}
