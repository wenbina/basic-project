package com.basic.project.common.login.authentication;

import com.basic.starter.core.login.authentication.Authentication;
import lombok.Builder;
import lombok.Data;

import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/15 9:42
 */
@Data
public class BasicAuthentication extends Authentication {

    // 用户id
    private Integer userId;

    // 用户登录信息
    private Object userInfo;

    public static Builder builder() {
        return new Builder();
    }

    public static class Builder {

        private BasicAuthentication basicAuthentication;

        public Builder() {
            this.basicAuthentication = new BasicAuthentication();
        }

        public Builder userId(Integer userId) {
            basicAuthentication.setUserId(userId);
            return this;
        }

        public Builder userInfo(Object userInfo) {
            basicAuthentication.setUserInfo(userInfo);
            return this;
        }

        public Builder token(String token) {
            basicAuthentication.setToken(token);
            return this;
        }

        public Builder timeout(Long timeout) {
            basicAuthentication.setTimeout(timeout);
            return this;
        }

        public Builder timeUnit(TimeUnit timeUnit) {
            basicAuthentication.setTimeUnit(timeUnit);
            return this;
        }

        public BasicAuthentication build() {
            return basicAuthentication;
        }
    }


}
