package com.basic.project.common.login.bean;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/16 11:39
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AccountBean {

    private Integer userId;

    private String username;

    private String status;

    private Integer roleId;

    private String roleName;
}
