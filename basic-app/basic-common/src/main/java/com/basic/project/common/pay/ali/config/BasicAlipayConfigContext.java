package com.basic.project.common.pay.ali.config;

import cn.hutool.core.util.ObjectUtil;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:35
 * 支付宝配置上下文
 */
@Component
public class BasicAlipayConfigContext {

    private static final Map<String, BasicAlipayConfig> APP_ID_MAP = new ConcurrentHashMap<>();

    private static final Map<String, BasicAlipayConfig> APP_MAP = new ConcurrentHashMap<>();

    /**
     * 添加支付宝支付配置，每个appId只需添加一次，相同appId将被覆盖
     */
    public static BasicAlipayConfig putBasicAlipayConfig(BasicAlipayConfig alipayConfig) {
        APP_MAP.put(alipayConfig.getApp(), alipayConfig);
        return APP_ID_MAP.put(alipayConfig.getAppId(), alipayConfig);
    }

    /**
     * 根据appId获取支付宝支付配置
     */
    public static BasicAlipayConfig getBasicAlipayConfigByAppId(String appId) {
        BasicAlipayConfig cfg = APP_ID_MAP.get(appId);
        if (null == cfg) {
            throw new IllegalStateException("暂无该支付宝应用AppId配置，请先配置");
        }
        return cfg;
    }

    public static BasicAlipayConfig getBasicAlipayConfigByApp(String app) {
        BasicAlipayConfig cfg = APP_MAP.get(app);
        if (null == cfg) {
            throw new IllegalStateException("暂无该支付宝应用配置，请先配置");
        }
        return cfg;
    }

    public static String getBasicAlipayConfigAppIdByApp(String app) {
        BasicAlipayConfig cfg = APP_MAP.get(app);
        if (null == cfg) {
            throw new IllegalStateException("暂无该支付宝应用配置，请先配置");
        }
        return cfg.getAppId();
    }

    public static List<BasicAlipayConfig> getAllBasicAlipayConfig() {
        return new ArrayList<>(APP_ID_MAP.values());
    }


}
