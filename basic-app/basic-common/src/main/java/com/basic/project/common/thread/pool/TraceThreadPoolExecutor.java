package com.basic.project.common.thread.pool;

import com.basic.project.common.thread.base.Preconditions;
import com.basic.starter.logging.tool.LogNotify;
import com.basic.starter.logging.tool.LogTool;

import java.util.List;
import java.util.Locale;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 9:47
 * 自定义可追踪线程池
 */
public class TraceThreadPoolExecutor extends ThreadPoolExecutor {

    private final static String SHUTDOWN_ARG = "shutdown";
    private final static String SHUTDOWN_NOW_ARG = "shutdownNow";
    private final static String TERMINATED_ARG = "terminated";

    // 线程执行异常日志模板
    private final static String THREAD_RUN_EXCEPTION_LOG = "- 追踪线程池异常：\n" +
            "- 线程池名称: {} \n" +
            "- 异常类别：线程执行失败 \n" +
            "- 执行参数：{} \n" +
            "- 异常信息：{} \n" +
            "- 堆栈追踪：{} \n";

    // 线程池执行拒绝策略日志模板
    private final static String THREAD_RUN_REJECT_LOG = "- 追踪线程池异常：\n" +
            "- 线程池名称: {} \n" +
            "- 异常类别：线程池执行拒绝策略 \n" +
            "- 当前线程池执行信息：{} \n";

    // 线程池内置方法执行日志模板
    private final static String THREAD_POOL_METHOD_EXEC = "- 追踪线程池执行{}方法: \n" +
            "- 线程池名称： {} \n" +
            "- 线程池状态信息： {} \n";


    private final static String THREAD_METHOD_EXEC_TIME = "- 追踪线程池线程执行时间: \n" +
            "- 线程池名称： {} \n" +
            "- 执行参数： {} \n" +
            "- 执行时间： {} \n" +
            "- 线程池状态信息：{} \n";

    // 记录线程执行时间
    private static final ConcurrentHashMap<String, Long> execMap = new ConcurrentHashMap<>();

    // 方法执行超时时间，默认5000ms
    private Long timeout = 5000L;

    // 自定义线程池名称
    private String poolName;

    // trace日志通知方式
    private List<LogNotify> traceLogNotifies;

    // info日志通知方式
    private List<LogNotify> infoLogNotifies;

    // warn日志通知方式
    private List<LogNotify> warnLogNotifies;

    // error日志通知方式
    private List<LogNotify> errorLogNotifies;

    /**
     * 初始化默认线程工厂和拒绝策略构造方法
     */
    public TraceThreadPoolExecutor(String poolName,
                                   int coreThreadSize,
                                   int maximumThreadSize,
                                   long keepAliveTime,
                                   TimeUnit unit,
                                   BlockingQueue<Runnable> workQueue) {
        this(poolName, coreThreadSize, maximumThreadSize, keepAliveTime, unit, workQueue,
                new ThreadFactoryBuilder().setNameFormat(poolName + "-%s").build(),
                new DefaultRejectedExecutionHandler(poolName));
    }

    /**
     * 无默认设置的构造方法
     */
    public TraceThreadPoolExecutor(String poolName, int coreThreadSize, int maximumThreadSize, long keepAliveTime, TimeUnit unit,
                                   BlockingQueue<Runnable> workQueue, ThreadFactory threadFactory, RejectedExecutionHandler handler) {
        super(coreThreadSize, maximumThreadSize, keepAliveTime, unit, workQueue, threadFactory, handler);
        this.poolName = poolName;
    }

    /**
     * 指定线程池名称，创建核心线程数10，最大线程数10，队列长度为1024的默认线程池
     */
    public static TraceThreadPoolExecutor defaultTraceThreadPoolExecutor(String poolName) {
        return new TraceThreadPoolExecutor(poolName,
                10,
                10,
                0,
                TimeUnit.SECONDS,
                new LinkedBlockingDeque<>(1024));
    }

    /**
     * 指定线程池名称，创建核心线程数10，最大线程数10，自定义队列长度的默认线程池
     */
    public static TraceThreadPoolExecutor defaultTraceThreadPoolExecutor(String poolName, BlockingQueue<Runnable> workQueue) {
        return new TraceThreadPoolExecutor(poolName,
                10,
                10,
                0,
                TimeUnit.SECONDS,
                workQueue);
    }

    /**
     * 线程池shutdown方法记录日志
     */
    @Override
    public void shutdown() {
        LogTool.error(errorLogNotifies, THREAD_POOL_METHOD_EXEC, SHUTDOWN_ARG, this.poolName, this.toString());
        super.shutdown();
    }

    /**
     * 线程池shutdownNow方法记录日志
     */
    @Override
    public List<Runnable> shutdownNow() {
        LogTool.error(errorLogNotifies, THREAD_POOL_METHOD_EXEC, SHUTDOWN_NOW_ARG, this.poolName, this.toString());
        return super.shutdownNow();
    }

    /**
     * 线程池terminated方法记录日志
     */
    @Override
    protected void terminated() {
        LogTool.error(errorLogNotifies, THREAD_POOL_METHOD_EXEC, TERMINATED_ARG, this.poolName, this.toString());
        super.terminated();
    }

    /**
     * 增加参数记录和日志记录
     */
    @Override
    public void execute(Runnable task) {
        super.execute(wrapRunnable(task, exceptionTrace(), null));
    }

    /**
     * 增强execute方法：增加参数记录和日志记录
     */
    public void execute(Runnable task, Object... params) {
        super.execute(wrapRunnable(task, exceptionTrace(), params));
    }

    /**
     * 增强submit方法：增加参数记录和日志记录
     */
    public Future<?> submit(Runnable task, Object... params) {
        return super.submit(wrapRunnable(task, exceptionTrace(), params));
    }

    /**
     * 增强submit方法：增加参数记录和日志记录
     */
    public <T> Future<T> submit(Callable<T> task, Object... params) {
        return super.submit(wrapCallable(task, exceptionTrace(), params));
    }

    /**
     * 获取堆栈运行信息
     */
    private Exception exceptionTrace() {
        return new Exception();
    }

    /**
     * 增强Runnable，对任务执行异常进行捕获增加日志记录
     */
    private Runnable wrapRunnable(final Runnable task, Exception threadException, Object... params) {
        return () -> {
            try {
                // 计算任务执行时间
                long startTime = System.currentTimeMillis();
                task.run();
                // 拼接参数
                String runParams = getRunParams(params);
                long diff = System.currentTimeMillis() - startTime;
                if (diff >= timeout) {
//                    LogTool.error(errorLogNotifies, THREAD_METHOD_EXEC_TIME, this.poolName, runParams, diff, this.toString());
                } else {
                    if (LogTool.isTraceEnabled()) {
                        LogTool.trace(traceLogNotifies, THREAD_METHOD_EXEC_TIME, this.poolName, runParams, diff, this.toString());
                    }
                }
            } catch (Exception e) {
                // 日志打印
                LogTool.error(errorLogNotifies, THREAD_RUN_EXCEPTION_LOG, poolName, getRunParams(params), LogTool.exceptionToString(e), LogTool.exceptionToString(threadException));
                throw e;
            }
        };
    }

    /**
     * 增强Callable，对任务执行异常进行捕获增加日志记录
     */
    private <T> Callable<T> wrapCallable(final Callable<T> task, Exception threadException, Object... params) {
        return () -> {
            // 拼接参数
            String runParams = getRunParams(params);
            try {
                long startTime = System.currentTimeMillis();
                T call = task.call();
                long diff = System.currentTimeMillis() - startTime;
                if (diff >= timeout) {
//                    LogTool.error(warnLogNotifies, THREAD_METHOD_EXEC_TIME, this.poolName, stringBuilder.toString(), diff, this.toString());
                } else {
                    if (LogTool.isTraceEnabled()) {
                        LogTool.trace(traceLogNotifies, THREAD_METHOD_EXEC_TIME, this.poolName, runParams, diff, this.toString());
                    }
                }
                return call;
            } catch (Exception e) {
                // 日志打印
                LogTool.error(errorLogNotifies, THREAD_RUN_EXCEPTION_LOG, poolName, runParams, LogTool.exceptionToString(e), LogTool.exceptionToString(threadException));
                throw e;
            }
        };
    }

    /**
     * 获取执行参数
     */
    private String getRunParams(Object... params) {
        StringBuilder stringBuilder = new StringBuilder();
        if (null != params) {
            for (Object param : params) {
                if (null != param) {
                    stringBuilder.append("|").append(param).append("|");
                }
            }
        }
        return stringBuilder.toString();
    }

    /**
     * 线程工厂构建器
     */
    public static class ThreadFactoryBuilder {
        // 自定义线程名称头
        private String nameFormat = null;
        // 自定义是否守护线程
        private Boolean daemon = null;
        // 自定义线程优先级
        private Integer priority = null;
        // 自定义线程异常处理
        private Thread.UncaughtExceptionHandler uncaughtExceptionHandler = null;
        // 自定义创建线程的线程工厂，默认使用Executors的defaultThreadFactory
        private ThreadFactory backingThreadFactory = null;

        public ThreadFactoryBuilder() {
        }

        public ThreadFactoryBuilder setNameFormat(String nameFormat) {
            format(nameFormat, 0);
            this.nameFormat = nameFormat;
            return this;
        }

        public ThreadFactoryBuilder setDaemon(boolean daemon) {
            this.daemon = daemon;
            return this;
        }

        public ThreadFactoryBuilder setPriority(int priority) {
            Preconditions.checkArgument(priority >= 1, "Thread priority (%s) must be >= %s", priority, 1);
            Preconditions.checkArgument(priority <= 10, "Thread priority (%s) must be <= %s", priority, 10);
            this.priority = priority;
            return this;
        }

        public ThreadFactoryBuilder setUncaughtExceptionHandler(Thread.UncaughtExceptionHandler uncaughtExceptionHandler) {
            this.uncaughtExceptionHandler = (Thread.UncaughtExceptionHandler)Preconditions.checkNotNull(uncaughtExceptionHandler);
            return this;
        }

        public ThreadFactoryBuilder setThreadFactory(ThreadFactory backingThreadFactory) {
            this.backingThreadFactory = Preconditions.checkNotNull(backingThreadFactory);
            return this;
        }

        public ThreadFactory build() {
            return build(this);
        }

        private static ThreadFactory build(ThreadFactoryBuilder builder) {
            final String nameFormat = builder.nameFormat;
            final Boolean daemon = builder.daemon;
            final Integer priority = builder.priority;
            final Thread.UncaughtExceptionHandler uncaughtExceptionHandler = builder.uncaughtExceptionHandler;
            final ThreadFactory backingThreadFactory = builder.backingThreadFactory != null ? builder.backingThreadFactory : Executors.defaultThreadFactory();
            final AtomicLong count = nameFormat != null ? new AtomicLong(0L) : null;
            return new ThreadFactory() {
                @Override
                public Thread newThread(Runnable runnable) {
                    Thread thread = backingThreadFactory.newThread(runnable);
                    if (nameFormat != null) {
                        thread.setName(ThreadFactoryBuilder.format(nameFormat, count.getAndIncrement()));
                    }
                    if (daemon != null) {
                        thread.setDaemon(daemon);
                    }
                    if (priority != null) {
                        thread.setPriority(priority);
                    }
                    if (uncaughtExceptionHandler != null) {
                        thread.setUncaughtExceptionHandler(uncaughtExceptionHandler);
                    }
                    return thread;
                }
            };
        }
        private static String format(String format, Object... args) {
            return String.format(Locale.ROOT, format, args);
        }
    }

    /**
     * 默认的拒绝策略
     */
    static class DefaultRejectedExecutionHandler implements RejectedExecutionHandler {

        private String poolName;

        private List<LogNotify> errorLogNotifies;

        public DefaultRejectedExecutionHandler(String poolName) {
            this.poolName = poolName;
        }

        public void setErrorLogNotifies(List<LogNotify> errorLogNotifies) {
            this.errorLogNotifies = errorLogNotifies;
        }

        /**
         * 拒绝策略
         */
        @Override
        public void rejectedExecution(Runnable r, ThreadPoolExecutor executor) {
            // 日志打印
            LogTool.error(errorLogNotifies, THREAD_RUN_REJECT_LOG, poolName, executor.toString());
        }
    }
}
