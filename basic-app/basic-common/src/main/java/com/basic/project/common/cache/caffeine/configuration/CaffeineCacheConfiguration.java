package com.basic.project.common.cache.caffeine.configuration;

import com.basic.project.common.cache.caffeine.enums.CaffeineCacheEnum;
import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.cache.CacheManager;
import org.springframework.cache.caffeine.CaffeineCache;
import org.springframework.cache.support.SimpleCacheManager;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/15 15:35
 */
@Configuration
public class CaffeineCacheConfiguration {

    private static final SimpleCacheManager CACHE_MANAGER = new SimpleCacheManager();
    @Bean
    public CacheManager caffeineCacheManager() {
        List<CaffeineCache> caches = new ArrayList<>();
        for (CaffeineCacheEnum value : CaffeineCacheEnum.values()) {
            Cache<Object, Object> cache = Caffeine.newBuilder()
                    .initialCapacity(value.getCapacity())
                    .maximumSize(value.getMaximumSize())
                    //写入后失效时间
                    .expireAfterWrite(value.getTimeout(), value.getTimeUnit())
                    .build();
            caches.add(new CaffeineCache(value.name(), cache));
        }
        CACHE_MANAGER.setCaches(caches);
        return CACHE_MANAGER;
    }
}
