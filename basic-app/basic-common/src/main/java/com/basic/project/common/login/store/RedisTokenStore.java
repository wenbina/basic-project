package com.basic.project.common.login.store;

import cn.hutool.core.util.ObjectUtil;
import cn.hutool.core.util.StrUtil;
import com.basic.project.common.login.authentication.BasicAuthentication;
import com.basic.starter.core.exception.BasicException;
import com.basic.starter.core.login.TokenStore;
import com.basic.starter.core.login.authentication.Authentication;
import org.springframework.data.redis.core.RedisTemplate;

import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:55
 */
@SuppressWarnings("all")
public class RedisTokenStore implements TokenStore {

    private static final String LOGIN_KEY_FORMAT = "session:SESSION#%s";
    private static final String USER_BEAN_KEY = "USER-BEAN";
    private static final String USER_BEAN_ID_KEY = "USER-BEAN-ID";
    private static final Long DEFAULT_TIMEOUT = 30L;
    private static final TimeUnit DEFAULT_TIME_UNIT = TimeUnit.MINUTES;

    private final RedisTemplate redisTemplate;

    public RedisTokenStore(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    @Override
    public void saveToken(Authentication authentication) {
        String token = authentication.getToken();
        if (StrUtil.isEmpty(token) || !(authentication instanceof BasicAuthentication)) {
            throw new BasicException("用户登陆信息异常");
        }
        BasicAuthentication basicAuth = (BasicAuthentication) authentication;
        if (ObjectUtil.isNull(basicAuth) || ObjectUtil.isNull(basicAuth.getUserId()) || ObjectUtil.isNull(basicAuth.getUserInfo())) {
            throw new BasicException("用户登陆信息异常");
        }
        String key = String.format(LOGIN_KEY_FORMAT, token);
        long timeout = ObjectUtil.isNotNull(basicAuth.getTimeout()) ? basicAuth.getTimeout() :  DEFAULT_TIMEOUT;
        TimeUnit timeUnit = ObjectUtil.isNotNull(basicAuth.getTimeUnit()) ? basicAuth.getTimeUnit() :  DEFAULT_TIME_UNIT;
        // 缓存用户id
        redisTemplate.opsForHash().put(key, USER_BEAN_ID_KEY, basicAuth.getUserId());
        // 缓存用户信息
        redisTemplate.opsForHash().put(key, USER_BEAN_KEY, basicAuth.getUserInfo());
        // 设置过期时间
        redisTemplate.expire(key, timeout, timeUnit);
    }

    @Override
    public void removeToken(String token) {
        redisTemplate.delete(String.format(LOGIN_KEY_FORMAT, token));
    }

    @Override
    public Authentication readAuthenticationByToken(String token) {
        String key = String.format(LOGIN_KEY_FORMAT, token);
        Object userId = redisTemplate.opsForHash().get(key, USER_BEAN_ID_KEY);
        Object userInfo = redisTemplate.opsForHash().get(key, USER_BEAN_KEY);
        return ObjectUtil.isNull(userId) || ObjectUtil.isNull(userInfo) ? null :
                BasicAuthentication.builder()
                        .token(token)
                        .userId((Integer) userId)
                        .userInfo(userInfo)
                        .build();
    }
}
