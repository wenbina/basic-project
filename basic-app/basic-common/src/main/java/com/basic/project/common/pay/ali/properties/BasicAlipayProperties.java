package com.basic.project.common.pay.ali.properties;

import com.basic.project.common.pay.ali.config.BasicAlipayConfig;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.Map;
import java.util.TreeMap;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 12:08
 */
@ConfigurationProperties(prefix = "basic.alipay")
@Data
public class BasicAlipayProperties {

    /**
     * 支付宝应用map
     */
    private Map<String, BasicAlipayConfig> config = new TreeMap<>();
}
