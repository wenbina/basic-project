package com.basic.project.common.thread.configuration;

import com.basic.project.common.thread.pool.TraceThreadPoolExecutor;
import com.basic.project.common.thread.enums.TraceThreadPoolEnum;
import com.basic.project.common.thread.notify.TraceThreadPoolLogNotify;
import com.basic.starter.logging.tool.LogTool;
import org.springframework.beans.BeansException;
import org.springframework.beans.factory.config.ConfigurableListableBeanFactory;
import org.springframework.beans.factory.support.BeanDefinitionRegistry;
import org.springframework.beans.factory.support.BeanDefinitionRegistryPostProcessor;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 10:28
 * 注册线程池到容器中
 */
@Component
public class TraceThreadPoolRunner implements BeanDefinitionRegistryPostProcessor, EnvironmentAware {

    @Override
    public void postProcessBeanFactory(ConfigurableListableBeanFactory beanFactory) throws BeansException {
        LogTool.info("=====================================================线程池初始化开始==========================================================");
        // 通用线程池
        TraceThreadPoolExecutor commonTraceThreadPoolExecutor =
                new TraceThreadPoolExecutor(TraceThreadPoolEnum.COMMON_THREAD_POOL.getLabel(),
                        20,
                        20,
                        0,
                        TimeUnit.SECONDS,
                        new LinkedBlockingQueue<>(1024));
        beanFactory.registerSingleton(TraceThreadPoolEnum.COMMON_THREAD_POOL.name(), commonTraceThreadPoolExecutor);
        LogTool.info("========================================线程池初始化-通用线程池创建成功==========================================================");
    }

    @Override
    public void postProcessBeanDefinitionRegistry(BeanDefinitionRegistry registry) throws BeansException {

    }

    @Override
    public void setEnvironment(Environment environment) {
        TraceThreadPoolLogNotify.TITLE = String.format(TraceThreadPoolLogNotify.TITLE, environment.getActiveProfiles()[0]);
    }
}
