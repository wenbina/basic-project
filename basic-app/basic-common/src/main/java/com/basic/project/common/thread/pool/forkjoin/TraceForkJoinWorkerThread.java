package com.basic.project.common.thread.pool.forkjoin;

import com.basic.starter.logging.tool.LogNotify;
import com.basic.starter.logging.tool.LogTool;

import java.util.List;
import java.util.concurrent.ForkJoinWorkerThread;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 12:02
 */
public class TraceForkJoinWorkerThread extends ForkJoinWorkerThread {

    private final static String THREAD_POOL_START_THREAD = "- TraceForkJoinPool启动线程: \n" +
            "- 线程池名称： {} \n" +
            "- 线程名称： {} \n" +
            "- 线程池状态信息： {} \n";

    private final static String THREAD_POOL_STOP_THREAD = "- TraceForkJoinPool线程关闭: \n" +
            "- 线程池名称： {} \n" +
            "- 线程名称： {} \n" +
            "- 执行任务数： {} \n" +
            "- 线程池状态信息： {} \n";

    /**
     * 记录线程中任务数
     */
    private ThreadLocal<Integer> taskCount = new ThreadLocal<>();

    /**
     * 记录所属Fork/Join pool的自定义名称
     */
    private String poolName;

    /**
     * Creates a ForkJoinWorkerThread operating in the given pool.
     *
     * @param pool the pool this thread works in
     * @throws NullPointerException if pool is null
     */
    protected TraceForkJoinWorkerThread(TraceForkJoinPool pool) {
        super(pool);
        this.poolName = getTraceForkJoinPool().getPoolName();
        setName(this.poolName + "-" + getName());
    }

    /**
     * 重写{@link ForkJoinWorkerThread#onStart()} 记录执行任务数和日志
     */
    @Override
    protected void onStart() {
        super.onStart();
        taskCount.set(0);
        if (LogTool.isDebugEnabled()) {
            LogTool.debug(warnLogNotify(), THREAD_POOL_START_THREAD, poolName, getName(), this.getPool().toString());
        }
    }

    /**
     * 重写{@link ForkJoinWorkerThread#onTermination(Throwable)} 记录日志
     * @param exception
     */
    @Override
    protected void onTermination(Throwable exception) {
        if (LogTool.isDebugEnabled()) {
            LogTool.debug(warnLogNotify(), THREAD_POOL_STOP_THREAD, poolName, getName(), taskCount.get(), this.getPool().toString());
        }
        super.onTermination(exception);
    }

    /**
     * 为当前线程增加执行任务数
     */
    protected final void addTask() {
        taskCount.set(taskCount.get() + 1);
    }

    /**
     * 获取当前线程执行任务数
     */
    protected final Integer getTaskCount() {
        return taskCount.get();
    }

    /**
     * 获取当前线程所属的TraceForkJoinPool
     * @return
     */
    public TraceForkJoinPool getTraceForkJoinPool() {
        return (TraceForkJoinPool) this.getPool();
    }

    /**
     * 当前线程属于TraceForkJoinPool，获取日志通知
     * @return
     */
    public List<LogNotify> traceLogNotify() {
        return ((TraceForkJoinPool) this.getPool()).getTraceLogNotifies();
    }
    /**
     * 当前线程属于TraceForkJoinPool，获取日志通知
     * @return
     */
    public List<LogNotify> infoLogNotify() {
        return ((TraceForkJoinPool) this.getPool()).getInfoLogNotifies();
    }
    /**
     * 当前线程属于TraceForkJoinPool，获取日志通知
     * @return
     */
    public List<LogNotify> warnLogNotify() {
        return ((TraceForkJoinPool) this.getPool()).getWarnLogNotifies();
    }
    /**
     * 当前线程属于TraceForkJoinPool，获取日志通知
     * @return
     */
    public List<LogNotify> errorLogNotify() {
        return ((TraceForkJoinPool) this.getPool()).getErrorLogNotifies();
    }
}
