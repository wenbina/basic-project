package com.basic.project.common.login.context;

import cn.hutool.core.util.ObjectUtil;
import com.basic.project.common.login.authentication.BasicAuthentication;
import com.basic.project.common.login.store.RedisTokenStore;
import com.basic.starter.core.constants.BaseCodeConstants;
import com.basic.starter.core.exception.BasicException;
import com.basic.starter.core.login.authentication.Authentication;
import com.basic.starter.web.context.authentication.AuthenticationContext;
import org.springframework.util.StringUtils;

/**
 * @author wen
 * @version 1.0
 * @data 2023/5/31 16:59
 */
public class BasicAuthenticationContext {

    private static RedisTokenStore redisTokenStore;

    public BasicAuthenticationContext(RedisTokenStore redisTokenStore) {
        BasicAuthenticationContext.redisTokenStore = redisTokenStore;
    }

    /**
     * 用户登录
     */
    public static void login(BasicAuthentication basicAuthentication) {
        redisTokenStore.saveToken(basicAuthentication);
    }

    /**
     * 从token中获取登录信息
     */
    public static BasicAuthentication getBasicAuthentication(String token) {
        Authentication authentication = redisTokenStore.readAuthenticationByToken(token);
        return null == authentication ? null : (BasicAuthentication) authentication;
    }

    /**
     * 获取登录用户id
     */
    public static Integer getUserId() {
        return getBasicAuthentication().getUserId();
    }

    /**
     * 获取登录用户信息
     */
    public static Object getUserInfo() {
        return getBasicAuthentication().getUserInfo();
    }

    /**
     * 获取登录用户Authentication
     */
    public static BasicAuthentication getBasicAuthentication() {
        Authentication authentication = AuthenticationContext.getAuthentication();
        if (ObjectUtil.isNull(authentication) || !StringUtils.hasText(authentication.getToken())) {
            throw new BasicException("用户信息不存在");
        }
        return (BasicAuthentication) authentication;
    }


}
