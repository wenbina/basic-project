package com.basic.project.common.constants.redis;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/28 9:40
 */
public interface RedisKeyConstant {

    // 邮件验证码key
    String EMAIL_VERIFICATION_CODE_KEY = "EMAIL_VERIFICATION_CODE";

    // 短信验证码key
    String SMS_VERIFICATION_CODE_KEY = "SMS_VERIFICATION_CODE";


}
