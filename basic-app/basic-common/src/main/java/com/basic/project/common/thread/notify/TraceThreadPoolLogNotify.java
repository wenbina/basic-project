package com.basic.project.common.thread.notify;

import com.basic.starter.logging.tool.LogNotify;
import com.basic.starter.logging.tool.LogTool;
import org.springframework.core.env.Environment;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/9 10:41
 * 线程池执行通知
 */
public class TraceThreadPoolLogNotify implements LogNotify {

    // 环境名称在启动时初始化
    public static String TITLE = "#### 线程池执行异常日志通知【%s】\n";
    private static String[] AT_MOBILES = new String[0];

    @Override
    public void logNotify(String msg) {
        LogTool.error("{}", TITLE, TITLE + msg, AT_MOBILES);
    }
}
