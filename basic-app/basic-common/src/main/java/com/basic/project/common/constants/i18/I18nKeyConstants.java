package com.basic.project.common.constants.i18;

import com.basic.starter.core.constants.BaseI18nKeyConstants;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 14:17
 */
public interface I18nKeyConstants extends BaseI18nKeyConstants {

    String TEST_KEY = "TEST";
}
