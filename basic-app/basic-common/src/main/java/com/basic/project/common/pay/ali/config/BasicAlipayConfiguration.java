package com.basic.project.common.pay.ali.config;

import com.basic.project.common.login.context.BasicAuthenticationContext;
import com.basic.project.common.pay.ali.properties.BasicAlipayProperties;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.context.annotation.Configuration;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/19 11:36
 */
@Configuration
@EnableConfigurationProperties(BasicAlipayProperties.class)
public class BasicAlipayConfiguration {

    @Resource
    private BasicAlipayProperties basicAlipayProperties;

    /**
     * 初始化支付宝相关参数
     */
    @PostConstruct
    public void init() {
        Map<String, BasicAlipayConfig> configMap = basicAlipayProperties.getConfig();
        configMap.forEach((key, value) -> {
            value.setApp(key);
            BasicAlipayConfigContext.putBasicAlipayConfig(value);
        });
    }

}
