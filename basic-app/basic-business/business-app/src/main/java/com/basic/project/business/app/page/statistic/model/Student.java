package com.basic.project.business.app.page.statistic.model;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @author wen
 * @version 1.0
 * @data 2023/8/9 14:20
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Student {

    private Long id;

    private String name;

    private Integer age;

    private String address;
}
