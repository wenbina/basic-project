package com.basic.project.business.app.model.es.entity;

import lombok.Data;
import org.apache.kafka.common.protocol.types.Field;

/**
 * @author wen
 * @date 2023/9/3
 */
@Data
public class Hotel {

    private Long id;

    // 名称
    private String name;

    // 地址
    private String address;

    // 价格
    private Double price;

    // 分数
    private Double score;

    // 品牌
    private String brand;

    // 城市
    private String city;

    // 酒店星级
    private String starName;

    // 商圈
    private String business;

    // 纬度
    private String latitude;

    // 经度
    private String longitude;

    // 酒店图片
    private String pic;
}
