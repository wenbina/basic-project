package com.basic.project.business.app.model.es.enums;

import com.basic.project.business.app.model.es.constants.EsMappingConstant;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Getter;
import org.apache.kafka.common.protocol.types.Field;

import java.util.Arrays;
import java.util.Locale;

/**
 * @author wen
 * @date 2023/9/5
 */
@Getter
@AllArgsConstructor(access = AccessLevel.PRIVATE)
public enum EsMappingRelationEnum {

    HOTEL_MAPPING("hotel", EsMappingConstant.HOTEL_MAPPING),;

    String index;

    String mapping;

    public static String getMappingByIndex(String index) {
        for (EsMappingRelationEnum mappingEnum : EsMappingRelationEnum.values()) {
            if (mappingEnum.index.equals(index)) {
                return mappingEnum.getMapping();
            }
        }
        return null;
    }
}
