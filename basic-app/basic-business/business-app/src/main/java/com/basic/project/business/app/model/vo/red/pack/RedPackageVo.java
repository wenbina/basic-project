package com.basic.project.business.app.model.vo.red.pack;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.protocol.types.Field;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RedPackageVo {

    private Double generationRate;

    private Integer duration;

    private String activityKey;

    private String redPackageKey;
}
