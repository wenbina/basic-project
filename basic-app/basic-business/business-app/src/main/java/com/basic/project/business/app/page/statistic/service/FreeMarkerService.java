package com.basic.project.business.app.page.statistic.service;

import com.basic.project.business.app.page.statistic.model.Student;
import freemarker.template.Configuration;
import freemarker.template.Template;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @author wen
 * @version 1.0
 * @data 2023/8/9 14:11
 */
@Service
public class FreeMarkerService {

    /**
     * 创建静态页面
     */
    public void createHtml() {
        try {
            // 获取配置对象
            Configuration configuration = new Configuration(Configuration.DEFAULT_INCOMPATIBLE_IMPROVEMENTS);
            // 设置字符集
            configuration.setDefaultEncoding("utf-8");
            // 设置加载的模板目录
            configuration.setDirectoryForTemplateLoading(new File("E:/ownspace/basic-project/basic-app/basic-application/src/main/resources/templates"));
            // 获取填充数据
            List<Student> students = new ArrayList<>();
            students.add(Student.builder().id(1L).name("张三").age(18).address("深圳").build());
            students.add(Student.builder().id(2L).name("李四").age(18).address("深圳").build());
            students.add(Student.builder().id(3L).name("王五").age(18).address("深圳").build());
            students.add(Student.builder().id(4L).name("赵六").age(18).address("深圳").build());

            // 加载数据
            Map<String, List> map = new HashMap<>();
            map.put("students", students);
            // 创建输出流对象
            FileWriter writer = new FileWriter("E:/ownspace/basic-project/ftl_html/student.html");
            // 获取加载的模板
            Template template = configuration.getTemplate("student.ftl");
            // 生成html文件
            template.process(map, writer);
            // 关闭流
            writer.close();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}
