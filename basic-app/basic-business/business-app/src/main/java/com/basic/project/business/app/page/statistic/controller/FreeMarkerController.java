package com.basic.project.business.app.page.statistic.controller;

import com.basic.project.business.app.page.statistic.service.FreeMarkerService;
import com.basic.starter.core.base.response.ApiResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author wen
 * @version 1.0
 * @data 2023/8/9 14:07
 */
@RestController
public class FreeMarkerController {

    @Resource
    private FreeMarkerService freeMarkerService;

    @PostMapping("/create/html")
    public ApiResponse createHtml() {
        this.freeMarkerService.createHtml();
        return ApiResponse.ok();
    }
}
