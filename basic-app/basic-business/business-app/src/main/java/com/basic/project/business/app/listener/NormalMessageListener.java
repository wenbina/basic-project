package com.basic.project.business.app.listener;

import com.basic.starter.kafka.consumer.IMessageQueueConsumerService;
import com.basic.starter.logging.tool.LogTool;
import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/21 10:59
 * 消费者
 */
@Service
public class NormalMessageListener implements IMessageQueueConsumerService {


    @Override
    public void consume(String data) {
        LogTool.info("正常消息监听消费消息：{}", data);
    }

    @Override
    public String topic() {
        return "test";
    }

    @KafkaListener(topics = "test")
    public void listener(ConsumerRecord<Integer, String> msg) {
        LogTool.info("消费消息数据为：{}", msg.value());
    }
}
