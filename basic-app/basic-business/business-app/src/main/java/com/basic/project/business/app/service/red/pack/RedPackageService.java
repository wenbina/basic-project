package com.basic.project.business.app.service.red.pack;

import cn.hutool.core.util.IdUtil;
import cn.hutool.core.util.ObjectUtil;
import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.basic.project.business.app.model.dto.red.pack.RedPackageDto;
import com.basic.project.business.app.model.vo.red.pack.RedPackageVo;
import com.basic.project.common.tools.redPackage.RedPackageUtil;
import com.basic.starter.cache.tools.RedisClient;
import com.basic.starter.core.exception.BasicException;
import com.basic.starter.logging.tool.LogTool;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.Map;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

@Service
public class RedPackageService {

    private static final String RED_PACKAGE_KEY = "redPackage:";
    private static final String RED_PACKAGE_CONSUME_KEY = "redPackage:consume:";
    private static final String RED_PACKAGE_ACTIVITY_KEY = "redPackage:activity:";
    private static final String RED_PACKAGE_INFO_KEY = "redPackage:info:";
    private static final String RED_PACKAGE_REDIS_QUEUE_KEY = "redPackage:redis:queue:";

    @Autowired
    private RedisTemplate redisTemplate;

    /**
     * 添加活动
     */
    public String addRedPackage(RedPackageDto dto) {
        // 记录活动信息，生成活动唯一标识，相当于活动的id标识
        dto.setActivityKey("hd_" + IdUtil.simpleUUID());
        // 保存活动
        redisTemplate.opsForSet().add(RED_PACKAGE_ACTIVITY_KEY, dto);
        // 活动开始后才初始化红包雨相关信息，保证所有用户同一时刻抢红包
        LocalDateTime localDateTime = dto.getStartTime().toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime();
        long delayTime = ChronoUnit.SECONDS.between(LocalDateTime.now(), localDateTime);
        // 启动定时任务，正式环境可以改成MQ延迟消息
        ScheduledExecutorService executor = Executors.newSingleThreadScheduledExecutor();
        executor.schedule(() -> {
            // 活动开始，拆红包
            Integer[] splitRedPackages = RedPackageUtil.splitRedPackage(dto.getTotalMoney(), dto.getRedPackageNum());
            LogTool.info("拆红包：{}", JSON.toJSONString(splitRedPackages));
            // 发红包并保存进list结构里且设置过期时间
            String key = IdUtil.simpleUUID();
            redisTemplate.opsForList().leftPushAll(RED_PACKAGE_KEY + key, splitRedPackages);
            // 红包雨的持续时间为过期时间，由于页面有倒计时，需要把倒计时加上
            redisTemplate.expire(RED_PACKAGE_KEY + key, dto.getDuration() + 10000, TimeUnit.MILLISECONDS);
            // 构建前端红包雨活动数据
            RedPackageVo redPackageVo = RedPackageVo.builder()
                    .generationRate(dto.getGenerationRate())
                    .duration(dto.getDuration())
                    .activityKey(dto.getActivityKey())
                    .redPackageKey(key)
                    .build();
            // 保存红包雨活动数据
            redisTemplate.opsForValue().set(RED_PACKAGE_INFO_KEY + redPackageVo.getActivityKey(), redPackageVo);
            // redis广播信息，服务器接收到广播信息后，websocket推送消息给前端用户开启红包雨活动
            redisTemplate.convertAndSend(RED_PACKAGE_REDIS_QUEUE_KEY, JSON.toJSONString(redPackageVo));
            LogTool.info("红包活动广播：{}", JSON.toJSONString(redPackageVo));
        },delayTime,TimeUnit.SECONDS);
        executor.shutdown();
        return dto.getActivityKey();
    }


    /**
     * 发红包
     */
    public String sendRedPackage(Integer totalMoney, Integer redPackageNum) {
        // 拆红包，将总金额拆分成redPackageNum个红包
        Integer[] splitRedPackages = RedPackageUtil.splitRedPackage(totalMoney, redPackageNum);
        LogTool.info("拆红包：{}", JSON.toJSONString(splitRedPackages));
        // 发红包并保存进list结构里且设置过期时间
        String key = IdUtil.simpleUUID();
        redisTemplate.opsForList().leftPushAll(RED_PACKAGE_KEY + key, splitRedPackages);
        redisTemplate.expire(RED_PACKAGE_KEY + key, 1, TimeUnit.DAYS);
        return key;
    }

    /**
     * 抢红包
     */
    public Object robRedPackage(String redPackageKey, Integer userId) {
        // 验证某个用户是否抢过红包
        Object redPackage = redisTemplate.opsForHash().get(RED_PACKAGE_CONSUME_KEY + redPackageKey, userId);
        if (ObjectUtil.isNotNull(redPackage)) {
            LogTool.info("重复抢红包：key为{}，用户：{}", redPackageKey, userId);
            return new BasicException("你已经抢过红包了");
        }
        // 未抢过红包，从红包池中出队一个作为红包
        Object partRedPackage = redisTemplate.opsForList().leftPop(RED_PACKAGE_KEY + redPackageKey);
        if (ObjectUtil.isNull(partRedPackage)) {
            // 抢完了
            LogTool.info("红包已抢完：key为{}", redPackageKey);
            // 抛出异常
            return new BasicException("手慢了，红包已抢完");
        }
        // 记录红包抢取记录
        redisTemplate.opsForHash().put(RED_PACKAGE_CONSUME_KEY + redPackageKey, userId, partRedPackage);
        LogTool.info("用户：{}，抢到了{}的红包", userId, partRedPackage);
        // TODO 后续异步进mysql或者MQ进一步做统计处理，每一年你发出多少红包，抢到多少红包，年度总结
        return partRedPackage;
    }

    /**
     * 领取记录
     */
    public Map<String, Integer> redPackageRecord(String redPackageKey) {
        return redisTemplate.opsForHash().entries(RED_PACKAGE_CONSUME_KEY + redPackageKey);
    }
}
