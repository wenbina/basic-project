package com.basic.project.business.app.model.dto.red.pack;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.apache.kafka.common.protocol.types.Field;

import java.util.Date;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class RedPackageDto {

    private String activityKey;

    private Double generationRate;

    private Date startTime;

    private Integer totalMoney;

    private Integer redPackageNum;

    private Integer duration;


}
