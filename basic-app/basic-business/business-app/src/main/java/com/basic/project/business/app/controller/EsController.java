package com.basic.project.business.app.controller;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import com.basic.project.business.app.model.es.constants.EsMappingConstant;
import com.basic.project.business.app.model.es.dto.EsHotel;
import com.basic.starter.core.base.response.ApiResponse;
import com.basic.starter.elasticsearch.client.EsClient;
import com.basic.starter.web.annotation.request.RequestParameterOne;
import org.apache.kafka.common.protocol.types.Field;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;


/**
 * @author wen
 * @date 2023/8/20
 */
@RestController
public class EsController {

    /**
     * 新增索引
     */
    @PostMapping("/es/add/index")
    public ApiResponse addIndex() {
        EsClient.createIndex("hotel", EsMappingConstant.HOTEL_MAPPING);
        return ApiResponse.ok();
    }

    /**
     * 判断索引是否存在
     */
    @PostMapping("/es/index/exist")
    public ApiResponse indexExist(@RequestParameterOne("index") String index) {
        return ApiResponse.ok(EsClient.indexExist(index));
    }


    /**
     * 新增文档
     */
    @PostMapping("/es/doc/add")
    public void addDoc() {
        String indexName = "hotel";
        Long docId = 1L;
        EsHotel esHotel = EsHotel.builder()
                .id(docId)
                .name("深圳湾万怡酒店")
                .price(100.0)
                .score(4.5)
                .brand("如家")
                .city("深圳")
                .starName("钻石")
                .business("深圳湾科技生态园")
                .location("22.536074,113.957079")
                .build();
        EsClient.addDoc(indexName, docId, JSONObject.toJSONString(esHotel));
    }

    /**
     * 修改文档
     */





}
