package com.basic.project.business.app.service;


import com.basic.starter.logging.tool.LogTool;
import com.basic.starter.rocketmq.annotation.MqConsumer;
import com.basic.starter.rocketmq.consumer.RocketMqConsumerListener;
import org.springframework.stereotype.Service;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/25 16:50
 */
@Service
@MqConsumer(consumerGroup = "test", topic = "test", tag = "test")
public class TestNormalMessageHandler implements RocketMqConsumerListener {

    @Override
    public void consume(String data) {
        LogTool.info("正常消息监听消费消息：{}", data);
    }

}
