package com.basic.project.business.app.model.request;

import lombok.Data;

/**
 * @author wen
 * @version 1.0
 * @data 2023/6/14 15:28
 */
@Data
public class TestRequest {

    private String name;

    private Integer userId;

}
