package com.basic.project.business.app.controller;

import com.alibaba.fastjson.JSON;
import com.basic.project.business.app.model.request.TestRequest;
import com.basic.project.common.constants.i18.I18nKeyConstants;
import com.basic.project.common.login.authentication.BasicAuthentication;
import com.basic.project.common.login.bean.AccountBean;
import com.basic.project.common.login.context.BasicAuthenticationContext;
import com.basic.project.common.thread.client.TraceThreadPoolClient;
import com.basic.project.common.thread.enums.TraceThreadPoolEnum;
import com.basic.project.common.tools.email.EmailTool;
import com.basic.starter.core.base.request.ApiRequest;
import com.basic.starter.core.base.response.ApiResponse;
import com.basic.starter.kafka.producer.ProducerClient;
import com.basic.starter.logging.tool.LogTool;
import com.basic.starter.rocketmq.producer.RocketProducerClient;
import com.basic.starter.web.annotation.auth.RequireLogin;
import com.basic.starter.web.annotation.request.RequestParameter;
import com.basic.starter.web.annotation.track.RequireTrack;
import com.basic.starter.web.tool.SpringContextHolder;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;
import java.util.concurrent.TimeUnit;


/**
 * @author wen
 * @version 1.0
 * @data 2023/5/29 18:19
 */
@RestController
public class TestController {

    @PostMapping("/test")
    @RequireTrack
    public ApiResponse<?> test(@RequestParameter ApiRequest<TestRequest> apiRequest) {
        TestRequest payload = apiRequest.getPayload();
        BasicAuthentication basicAuthentication = new BasicAuthentication();
        String token = StringUtils.replace(UUID.randomUUID().toString().toUpperCase(), "-", "");
        basicAuthentication.setToken(token);
        basicAuthentication.setTimeout(120L);
        basicAuthentication.setTimeUnit(TimeUnit.MINUTES);
        basicAuthentication.setUserId(1);
        basicAuthentication.setUserInfo(AccountBean.builder()
                .userId(1)
                .username("zs")
                .status("Y")
                .roleId(1)
                .roleName("超级管理员").build());
        BasicAuthenticationContext.login(basicAuthentication);
        TraceThreadPoolClient.getExecutor(TraceThreadPoolEnum.COMMON_THREAD_POOL.name()).execute(() -> {
            LogTool.info("自定义线程池执行任务：参数为：{}", JSON.toJSONString(payload));
        });
        return ApiResponse.ok(SpringContextHolder.getI18nMessage(I18nKeyConstants.TEST_KEY));
    }

    @PostMapping("/test/get")
    @RequireLogin
    public ApiResponse<?> testGet() {
        LogTool.info("用户信息为：{}", JSON.toJSONString(BasicAuthenticationContext.getUserInfo()));
        return ApiResponse.ok(BasicAuthenticationContext.getUserInfo());
    }

    @PostMapping("/test/kafka/send")
    public ApiResponse<?> testKafkaSend() {
        ProducerClient.defaultSend("test", "测试");
        return ApiResponse.ok();
    }

    @PostMapping("/test/send/email")
    public ApiResponse<?> testSendEmail() {
        EmailTool.sendCode("2461838916@qq.com", 60L, TimeUnit.SECONDS);
        return ApiResponse.ok();
    }

    @PostMapping("/test/get/code")
    public ApiResponse<?> testGetCode() {
        return ApiResponse.ok(EmailTool.getCodeByCache("2461838916@qq.com"));
    }

    @PostMapping("/test/mq/send")
    public ApiResponse<?> testRocketMqSend() {
        RocketProducerClient.send("test", "test", "test");
        return ApiResponse.ok();
    }

    @PostMapping("/test/mq/send/delay")
    public ApiResponse<?> testRocketMqSendDelay() {
        RocketProducerClient.sendDelayMsg("delay", "delay", "delayMessage", 2);
        return ApiResponse.ok();
    }
}
