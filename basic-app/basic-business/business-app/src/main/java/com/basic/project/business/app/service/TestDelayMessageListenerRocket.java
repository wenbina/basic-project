package com.basic.project.business.app.service;

import com.basic.starter.logging.tool.LogTool;
import com.basic.starter.rocketmq.annotation.MqConsumer;
import com.basic.starter.rocketmq.consumer.RocketMqConsumerListener;
import org.springframework.stereotype.Service;

/**
 * @author wen
 * @version 1.0
 * @data 2023/7/31 15:14
 */
@Service
@MqConsumer(consumerGroup = "test", topic = "delay", tag = "delay")
public class TestDelayMessageListenerRocket implements RocketMqConsumerListener {

    @Override
    public void consume(String msg) {
        LogTool.info("延时消息监听消费消息：{}", msg);
    }
}
